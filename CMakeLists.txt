cmake_minimum_required (VERSION 3.1 FATAL_ERROR)
project (meshtools)

# C standard
set (CMAKE_CXX_STANDARD 11)

# Module path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_SOURCE_DIR}/cmake/modules/")

# Visualization
option (BUILD_MT "Create mt standalone" ON)
option (BUILD_VIEWER "Build the gl viewer" OFF)
option (BUILD_ANDROID "Use BUILD_ANDROID GL headers" OFF)

if(BUILD_VIEWER AND NOT BUILD_ANDROID)
    find_package(GLEW REQUIRED)
endif(BUILD_VIEWER AND NOT BUILD_ANDROID)

if(BUILD_MT AND BUILD_VIEWER)
    find_package(glfw3 REQUIRED)
    find_package(OpenGL REQUIRED)
endif(BUILD_MT AND BUILD_VIEWER)

# Deps
find_package(OpenMesh REQUIRED)

if(NOT BUILD_ANDROID)
find_package (Threads REQUIRED)
find_package(JNI REQUIRED)
endif(NOT BUILD_ANDROID)

# Versions
set(Upstream_VERSION 0.0.1)

# Upstream stuff
set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_INCLUDE_CURRENT_DIR_IN_INTERFACE ON)

# Config file
configure_file (
  "${PROJECT_SOURCE_DIR}/MeshtoolsConfig.h.in"
  "${PROJECT_BINARY_DIR}/MeshtoolsConfig.h"
  )

# Compiler directives
add_definitions(-D_USE_MATH_DEFINES)
add_definitions(-DNOMINMAX)
add_definitions(-DINCLUDE_TEMPLATES)
add_definitions(-DOM_INCLUDE_TEMPLATES)
add_definitions(-D_CRT_SECURE_NO_WARNINGS)
add_definitions(-D_SCL_SECURE_NO_WARNINGS)

######## PACKAGES #########
#  utils
set  (utils_SOURCES
        src/utils/stringutils.cpp
        src/utils/fileutils.cpp
        src/utils/byteutils.cpp)
set  (utils_HEADERS
        src/utils/stringutils.h
        src/utils/fileutils.h
        src/utils/types.h
        src/utils/byteutils.h
        src/utils/threadutils.h
        src/utils/osutils.h)
source_group(Source\ Files\\Utils FILES ${utils_SOURCES})
source_group(Header\ Files\\Utils FILES ${utils_HEADERS})

# laslib
set  (laslib_SOURCES
        src/laslib/lasreader.cpp
        src/laslib/lasreader_las.cpp
        src/laslib/lasreader_bin.cpp
        src/laslib/lasreader_qfit.cpp
        src/laslib/lasreader_shp.cpp
        src/laslib/lasreader_asc.cpp
        src/laslib/lasreader_bil.cpp
        src/laslib/lasreader_dtm.cpp
        src/laslib/lasreader_las.cpp
        src/laslib/lasreader_txt.cpp
        src/laslib/lasreadermerged.cpp
        src/laslib/lasreaderbuffered.cpp
        src/laslib/lasreaderpipeon.cpp
        src/laslib/laswriter.cpp
        src/laslib/laswriter_las.cpp
        src/laslib/laswriter_bin.cpp
        src/laslib/laswriter_qfit.cpp
        src/laslib/laswriter_wrl.cpp
        src/laslib/laswriter_txt.cpp
        src/laslib/laswritercompatible.cpp
        src/laslib/laswaveform13reader.cpp
        src/laslib/laswaveform13writer.cpp
        src/laslib/lasutility.cpp
        src/laslib/lasfilter.cpp
        src/laslib/lastransform.cpp
        src/laslib/fopen_compressed.cpp)
set  (laslib_HEADERS
        src/laslib/lasreader.hpp
        src/laslib/lasreader_las.hpp
        src/laslib/lasreader_bin.hpp
        src/laslib/lasreader_qfit.hpp
        src/laslib/lasreader_shp.hpp
        src/laslib/lasreader_asc.hpp
        src/laslib/lasreader_bil.hpp
        src/laslib/lasreader_dtm.hpp
        src/laslib/lasreader_las.hpp
        src/laslib/lasreader_txt.hpp
        src/laslib/lasreadermerged.hpp
        src/laslib/lasreaderbuffered.hpp
        src/laslib/lasreaderpipeon.hpp
        src/laslib/laswriter.hpp
        src/laslib/laswriter_las.hpp
        src/laslib/laswriter_bin.hpp
        src/laslib/laswriter_qfit.hpp
        src/laslib/laswriter_wrl.hpp
        src/laslib/laswriter_txt.hpp
        src/laslib/laswritercompatible.hpp
        src/laslib/laswaveform13reader.hpp
        src/laslib/laswaveform13writer.hpp
        src/laslib/lasutility.hpp
        src/laslib/lasfilter.hpp
        src/laslib/lastransform.hpp)
source_group(Source\ Files\\LasLib FILES ${laslib_SOURCES})
source_group(Header\ Files\\LasLib FILES ${laslib_HEADERS})


# laszip
set  (laszip_SOURCES
        src/laszip/laszip.cpp
        src/laszip/lasreadpoint.cpp
        src/laszip/lasreaditemcompressed_v1.cpp
        src/laszip/lasreaditemcompressed_v2.cpp
        src/laszip/laswritepoint.cpp
        src/laszip/laswriteitemcompressed_v1.cpp
        src/laszip/laswriteitemcompressed_v2.cpp
        src/laszip/integercompressor.cpp
        src/laszip/arithmeticdecoder.cpp
        src/laszip/arithmeticencoder.cpp
        src/laszip/arithmeticmodel.cpp
        src/laszip/lasindex.cpp
        src/laszip/lasquadtree.cpp
        src/laszip/lasinterval.cpp)
set  (laszip_HEADERS
        src/laszip/laszip.hpp
        src/laszip/lasreadpoint.hpp
        src/laszip/lasreaditemcompressed_v1.hpp
        src/laszip/lasreaditemcompressed_v2.hpp
        src/laszip/laswritepoint.hpp
        src/laszip/laswriteitemcompressed_v1.hpp
        src/laszip/laswriteitemcompressed_v2.hpp
        src/laszip/integercompressor.hpp
        src/laszip/arithmeticdecoder.hpp
        src/laszip/arithmeticencoder.hpp
        src/laszip/arithmeticmodel.hpp
        src/laszip/lasindex.hpp
        src/laszip/lasquadtree.hpp
        src/laszip/lasinterval.hpp)
source_group(Source\ Files\\LasZip FILES ${laszip_SOURCES})
source_group(Header\ Files\\LasZip FILES ${laszip_HEADERS})


# data types
set  (datatypes_SOURCES
        src/datatypes/solid.cpp
        src/datatypes/pointcloud.cpp
        src/datatypes/bbox.cpp
        src/datatypes/ray.cpp
        src/datatypes/octree.cpp
        src/datatypes/fsoctree.cpp
        src/datatypes/voxelgrid.cpp
        src/datatypes/mesh.cpp
		src/datatypes/circle.cpp)
set  (datatypes_HEADERS
        src/datatypes/solid.h
        src/datatypes/pointcloud.h
        src/datatypes/bbox.h
        src/datatypes/octree.h
        src/datatypes/fsoctree.h
        src/datatypes/ray.h
        src/datatypes/voxelgrid.h
        src/datatypes/mesh.h
        src/datatypes/memorypool.h
        src/datatypes/linkedlist.h
        src/datatypes/forwardlinkedlist.h
		src/datatypes/circle.h)
source_group(Source\ Files\\DataTypes FILES ${datatypes_SOURCES})
source_group(Header\ Files\\DataTypes FILES ${datatypes_HEADERS})

# io
set  (io_SOURCES
        src/io/pointcloudreader.cpp
        src/io/pointcloudwriter.cpp
        src/io/plyreader.cpp
        src/io/plywriter.cpp
        src/io/meshreader.cpp
        src/io/meshwriter.cpp
        src/io/lasreader.cpp
        src/io/xyzwriter.cpp
        src/io/lodcreator.cpp)
set  (io_HEADERS
        src/io/pointcloudreader.h
        src/io/pointcloudwriter.h
        src/io/plyreader.h
        src/io/plywriter.h
        src/io/meshreader.h
        src/io/meshwriter.h
        src/io/lasreader.h
        src/io/xyzwriter.h
        src/io/lodcreator.h)
source_group(Source\ Files\\Io FILES ${io_SOURCES})
source_group(Header\ Files\\Io FILES ${io_HEADERS})

# filtering
set  (filtering_SOURCES
        src/filtering/downsampling.cpp
        src/filtering/islands.cpp
        src/filtering/mislands.cpp
        src/filtering/decimate.cpp
        src/filtering/subdivide.cpp
        src/filtering/smooth.cpp
        src/filtering/remesh.cpp
        src/filtering/aremesh.cpp
        src/filtering/repair.cpp
        src/filtering/nonmanifolds.cpp
        src/filtering/holefill.cpp
        src/filtering/farea.cpp
        src/filtering/fedgelength.cpp)
set  (filtering_HEADERS
        src/filtering/downsampling.h
        src/filtering/islands.h
        src/filtering/mislands.h
        src/filtering/decimate.h
        src/filtering/subdivide.h
        src/filtering/smooth.h
        src/filtering/remesh.h
        src/filtering/aremesh.h
        src/filtering/repair.h
        src/filtering/nonmanifolds.h
        src/filtering/holefill.h
        src/filtering/farea.h
        src/filtering/fedgelength.h)
source_group(Source\ Files\\Filtering FILES ${filtering_SOURCES})
source_group(Header\ Files\\Filtering FILES ${filtering_HEADERS})

# processing
set  (processing_SOURCES
        src/processing/transfercolor.cpp
        src/processing/merge.cpp)
set  (processing_HEADERS
        src/processing/transfercolor.h
        src/processing/merge.h)
source_group(Source\ Files\\Processing FILES ${processing_SOURCES})
source_group(Header\ Files\\Processing FILES ${processing_HEADERS})

# math
set  (math_SOURCES
        src/math/matrix.cpp
        src/math/quaternion.cpp)
set  (math_HEADERS
        src/math/matrix.h
        src/math/quaternion.h)
source_group(Source\ Files\\Math FILES ${math_SOURCES})
source_group(Header\ Files\\Math FILES ${math_HEADERS})

# visualization
set  (visualization_HEADERS
        src/visualization/viewer.h
        src/visualization/view.h
        src/visualization/meshview.h
        src/visualization/pointcloudview.h
        src/visualization/fsoctreeview.h
		src/visualization/circleview.h)
source_group(Header\ Files\\Visualization FILES ${visualization_HEADERS})

if(BUILD_VIEWER)
set  (visualization_SOURCES
        src/visualization/viewer.cpp
        src/visualization/view.cpp
        src/visualization/meshview.cpp
        src/visualization/pointcloudview.cpp
        src/visualization/fsoctreeview.cpp
		src/visualization/circleview.cpp)
source_group(Source\ Files\\Visualization FILES ${visualization_SOURCES})
endif(BUILD_VIEWER)

### BINDINGS ###
# jni
set (jni_SOURCES
        src/bindings/jni/jni_pointcloud.cpp
        src/bindings/jni/jni_pointcloudreader.cpp
        src/bindings/jni/jni_islands.cpp
        src/bindings/jni/jni_downsample.cpp
        src/bindings/jni/jni_pointcloudwriter.cpp)
if(BUILD_VIEWER)
    set (jniviewer_SOURCES
        src/bindings/jni/jni_viewer.cpp)
endif(BUILD_VIEWER)
source_group(Source\ Files\\Bindings\\jni FILES ${jni_SOURCES} ${jniviewer_SOURCES})


# cs
set (cs_SOURCES
        src/bindings/cs/cs_pointcloud.cpp
        src/bindings/cs/cs_mesh.cpp
        src/bindings/cs/cs_pointcloudreader.cpp
        src/bindings/cs/cs_pointcloudwriter.cpp
        src/bindings/cs/cs_meshreader.cpp
        src/bindings/cs/cs_merge.cpp
        src/bindings/cs/cs_downsample.cpp
        src/bindings/cs/cs_bbox.cpp
        src/bindings/cs/cs_linkedlist_point.cpp
		src/bindings/cs/cs_circle.cpp)

if(BUILD_VIEWER)
set (csviewer_SOURCES
        src/bindings/cs/cs_viewer.cpp)

set  (visualization_SOURCES
		src/visualization/viewer.cpp
		src/visualization/view.cpp
		src/visualization/meshview.cpp
		src/visualization/pointcloudview.cpp
		src/visualization/fsoctreeview.cpp
		src/visualization/circleview.cpp)
source_group(Source\ Files\\Visualization FILES ${visualization_SOURCES})
endif(BUILD_VIEWER)
source_group(Source\ Files\\Bindings\\cs FILES ${cs_SOURCES} ${csviewer_SOURCES})

set (bindings_SOURCES
        ${jni_SOURCES} ${jniviewer_SOURCES}
        ${cs_SOURCES} ${csviewer_SOURCES}
        )


# meshtools library
set  (project_SOURCES
        ${utils_SOURCES}
        ${laslib_SOURCES}
        ${laszip_SOURCES}
        ${io_SOURCES}
        ${datatypes_SOURCES}
        ${filtering_SOURCES}
        ${math_SOURCES}
        ${visualization_SOURCES}
        ${processing_SOURCES})
set  (project_HEADERS
        ${utils_HEADERS}
        ${laslib_HEADERS}
        ${laszip_HEADERS}
        ${io_HEADERS}
        ${datatypes_HEADERS}
        ${filtering_HEADERS}
        ${math_HEADERS}
        ${visualization_HEADERS}
        ${processing_HEADERS})

add_library(meshtools ${project_SOURCES} ${project_HEADERS})
add_library(meshtools_shared SHARED ${project_SOURCES} ${project_HEADERS} ${bindings_SOURCES})


# Includes
target_include_directories(
    meshtools PUBLIC
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src>"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>"
)
target_include_directories(
    meshtools_shared PUBLIC
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src>"
    "$<BUILD_INTERFACE:${CMAKE_CURRENT_BINARY_DIR}>"
)

# headers and libraries
target_include_directories(meshtools PRIVATE "${PROJECT_BINARY_DIR}")
target_include_directories(meshtools PRIVATE "${OPENMESH_INCLUDE_DIRS}")
target_include_directories(meshtools PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/openflipper>")
target_include_directories(meshtools PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/laslib>")
target_include_directories(meshtools PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/laszip>")

if(NOT BUILD_ANDROID)
target_include_directories(meshtools_shared PRIVATE "${JNI_INCLUDE_DIRS}")
endif(NOT BUILD_ANDROID)
target_include_directories(meshtools_shared PRIVATE "${PROJECT_BINARY_DIR}")
target_include_directories(meshtools_shared PRIVATE "${OPENMESH_INCLUDE_DIRS}")
target_include_directories(meshtools_shared PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/openflipper>")
target_include_directories(meshtools_shared PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/laslib>")
target_include_directories(meshtools_shared PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/laszip>")


if(BUILD_VIEWER AND NOT BUILD_ANDROID)
target_include_directories(meshtools PUBLIC ${OPENGL_INCLUDE_DIR})
target_include_directories(meshtools PUBLIC ${GLEW_INCLUDE_DIRS})
target_link_libraries(meshtools PUBLIC ${GLEW_LIBRARIES})
target_link_libraries(meshtools PUBLIC ${OPENGL_gl_LIBRARY})
target_link_libraries(meshtools PUBLIC ${OPENGL_glu_LIBRARY})

target_include_directories(meshtools_shared PUBLIC ${OPENGL_INCLUDE_DIR})
target_include_directories(meshtools_shared PUBLIC ${GLEW_INCLUDE_DIRS})
target_link_libraries(meshtools_shared PUBLIC ${GLEW_LIBRARIES})
target_link_libraries(meshtools_shared PUBLIC ${OPENGL_gl_LIBRARY})
target_link_libraries(meshtools_shared PUBLIC ${OPENGL_glu_LIBRARY})

target_compile_definitions(meshtools_shared PRIVATE MODULE_API_EXPORTS=1)
endif(BUILD_VIEWER AND NOT BUILD_ANDROID)

target_link_libraries (meshtools PUBLIC ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(meshtools PUBLIC ${OPENMESH_LIBRARIES})
target_link_libraries (meshtools_shared PUBLIC ${CMAKE_THREAD_LIBS_INIT})
target_link_libraries(meshtools_shared PUBLIC ${OPENMESH_LIBRARIES})
if(BUILD_ANDROID)
target_link_libraries(meshtools_shared PUBLIC log)
endif(BUILD_ANDROID)

if(BUILD_ANDROID AND BUILD_VIEWER)
target_link_libraries(meshtools_shared PUBLIC GLESv2)
endif(BUILD_ANDROID AND BUILD_VIEWER)

### MT ###
if( BUILD_MT)
# commands
set (commands_SOURCES
        src/mt/commands/command.cpp
        src/mt/commands/cmd_list.cpp
        src/mt/commands/cmd_stats.cpp
        src/mt/commands/cmd_open.cpp
        src/mt/commands/cmd_mopen.cpp
        src/mt/commands/cmd_save.cpp
        src/mt/commands/cmd_close.cpp
        src/mt/commands/cmd_keep.cpp
        src/mt/commands/cmd_select.cpp
        src/mt/commands/cmd_unselect.cpp
        src/mt/commands/cmd_focus.cpp
        src/mt/commands/cmd_rename.cpp
        src/mt/commands/cmd_octreelevel.cpp
		src/mt/commands/cmd_circle.cpp

        src/mt/commands/batchcommand.cpp
        src/mt/commands/cmd_octree.cpp
        src/mt/commands/cmd_downsample.cpp
        src/mt/commands/cmd_islands.cpp
        src/mt/commands/cmd_decimate.cpp
        src/mt/commands/cmd_subdivide.cpp
        src/mt/commands/cmd_smooth.cpp
        src/mt/commands/cmd_remesh.cpp
        src/mt/commands/cmd_aremesh.cpp
        src/mt/commands/cmd_repair.cpp
        src/mt/commands/cmd_nonmanifolds.cpp
        src/mt/commands/cmd_holefill.cpp
        src/mt/commands/cmd_transfercolor.cpp
        src/mt/commands/cmd_merge.cpp
        src/mt/commands/cmd_delete.cpp
        src/mt/commands/cmd_farea.cpp
        src/mt/commands/cmd_fedgelength.cpp
        src/mt/commands/cmd_makeok.cpp
        src/mt/commands/cmd_createlods.cpp)
set (commands_HEADERS
        src/mt/commands/command.h
        src/mt/commands/cmd_open.h
        src/mt/commands/cmd_mopen.h
        src/mt/commands/cmd_save.h
        src/mt/commands/cmd_close.h
        src/mt/commands/cmd_keep.h
        src/mt/commands/cmd_select.h
        src/mt/commands/cmd_unselect.h
        src/mt/commands/cmd_focus.h
        src/mt/commands/cmd_rename.h
        src/mt/commands/cmd_octreelevel.h
		src/mt/commands/cmd_circle.h

        src/mt/commands/batchcommand.h
        src/mt/commands/cmd_octree.h
        src/mt/commands/cmd_downsample.h
        src/mt/commands/cmd_islands.h
        src/mt/commands/cmd_list.h
        src/mt/commands/cmd_stats.h
        src/mt/commands/cmd_decimate.h
        src/mt/commands/cmd_subdivide.h
        src/mt/commands/cmd_smooth.h
        src/mt/commands/cmd_remesh.h
        src/mt/commands/cmd_aremesh.h
        src/mt/commands/cmd_repair.h
        src/mt/commands/cmd_nonmanifolds.h
        src/mt/commands/cmd_holefill.h
        src/mt/commands/cmd_transfercolor.h
        src/mt/commands/cmd_merge.h
        src/mt/commands/cmd_delete.h
        src/mt/commands/cmd_farea.h
        src/mt/commands/cmd_fedgelength.h
        src/mt/commands/cmd_makeok.h
        src/mt/commands/cmd_createlods.h)
source_group(Source\ Files\\Commands FILES ${commands_SOURCES})
source_group(Header\ Files\\Commands FILES ${commands_HEADERS})

# state
set (state_SOURCES
        src/mt/state/mtstate.cpp
        src/mt/state/objectstate.cpp
        src/mt/state/meshstate.cpp
        src/mt/state/pointcloudstate.cpp
		src/mt/state/circlestate.cpp)
set (state_HEADERS
        src/mt/state/mtstate.h
        src/mt/state/objectstate.h
        src/mt/state/meshstate.h
        src/mt/state/pointcloudstate.h
		src/mt/state/circlestate.h)
source_group(Source\ Files\\State FILES ${state_SOURCES})
source_group(Header\ Files\\State FILES ${state_HEADERS})

# viewer
if(BUILD_VIEWER)
set (viewer_HEADERS
        src/mt/viewer/iviewer.h
        src/mt/viewer/viewerfactory.h

        src/mt/viewer/glfwviewer.h
)
set (viewer_SOURCES
        src/mt/viewer/viewerfactory.cpp
        src/mt/viewer/glfwviewer.cpp
        )

else(BUILD_VIEWER)
set (viewer_HEADERS
        src/mt/viewer/iviewer.h
        src/mt/viewer/viewerfactory.h
)
set (viewer_SOURCES
        src/mt/viewer/viewerfactory.cpp
)
endif(BUILD_VIEWER)
source_group(Source\ Files\\Viewer FILES ${viewer_SOURCES})
source_group(Header\ Files\\Viewer FILES ${viewer_HEADERS})

# mt tool
set  (mt_SOURCES
        ${commands_SOURCES}
        ${commands_HEADERS}
        ${viewer_SOURCES}
        ${viewer_HEADERS}
        ${state_SOURCES}
        ${state_HEADERS}
        src/mt/main.cpp)

add_executable (mt ${mt_SOURCES})
target_include_directories(mt PRIVATE "${PROJECT_BINARY_DIR}")
target_include_directories(mt PRIVATE "${OPENMESH_INCLUDE_DIRS}")
target_include_directories(mt PRIVATE "$<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}/src/openflipper>")
set_target_properties(mt PROPERTIES INSTALL_RPATH_USE_LINK_PATH TRUE)

target_link_libraries (mt meshtools)
if(BUILD_VIEWER)
    target_link_libraries(mt glfw)
    target_link_libraries(mt ${OPENGL_gl_LIBRARY})
    target_link_libraries(mt ${OPENGL_glu_LIBRARY})
endif(BUILD_VIEWER)
endif(BUILD_MT)
###########################

# Targets
if(BUILD_MT)
install(TARGETS meshtools meshtools_shared mt EXPORT meshtoolsTargets
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES DESTINATION include
)
else(BUILD_MT)
install(TARGETS meshtools meshtools_shared EXPORT meshtoolsTargets
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  RUNTIME DESTINATION bin
  INCLUDES DESTINATION include
)
endif(BUILD_MT)


# Headers
install(
  DIRECTORY
    "src/"
    "src/datatypes"
    "src/filtering"
    "src/io"
    "src/utils"
    "src/math"
    "src/visualization"
    "src/processing"
    "src/utils"

    "src/openflipper/ACG"
    "src/openflipper/HoleFilling"
    "src/openflipper/IsotropicRemesher"
    "src/openflipper/MeshRepair"
    "src/openflipper/MeshTools"
    "src/openflipper/Remesher"

    "src/OpenMesh/Core/Geometry"
    "src/OpenMesh/Core/IO"
    "src/OpenMesh/Core/IO/exporter"
    "src/OpenMesh/Core/IO/importer"
    "src/OpenMesh/Core/IO/reader"
    "src/OpenMesh/Core/IO/writer"
    "src/OpenMesh/Core/Mesh"
    "src/OpenMesh/Core/Mesh"
    "src/OpenMesh/Core/Mesh/gen"
    "src/OpenMesh/Core/System"
    "src/OpenMesh/Core/Templates"
    "src/OpenMesh/Core/Utils"
    "src/OpenMesh/Tools/Decimater"
    "src/OpenMesh/Tools/Dualizer"
    "src/OpenMesh/Tools/Kernel_OSG"
    "src/OpenMesh/Tools/Smoother"
    "src/OpenMesh/Tools/Subdivider/Adaptive"
    "src/OpenMesh/Tools/Subdivider/Uniform"
    "src/OpenMesh/Tools/Utils"
    "src/OpenMesh/Tools/VDPM"
  DESTINATION "include"
  FILES_MATCHING
    PATTERN "*.h"
    PATTERN "*.hh"
    PATTERN "*T.cc"
    PATTERN "Algorithms.cc"
)

# Export headers:
include(GenerateExportHeader)
generate_export_header(meshtools)
install(
    FILES
        "${CMAKE_CURRENT_BINARY_DIR}/meshtools_export.h"
        "${CMAKE_CURRENT_BINARY_DIR}/MeshtoolsConfig.h"
    DESTINATION "include"
)

include(CMakePackageConfigHelpers)
write_basic_package_version_file(
  "${CMAKE_CURRENT_BINARY_DIR}/meshtools/meshtoolsConfigVersion.cmake"
  VERSION ${Upstream_VERSION}
  COMPATIBILITY AnyNewerVersion
)

export(EXPORT meshtoolsTargets
  FILE "${CMAKE_CURRENT_BINARY_DIR}/meshtools/meshtoolsTargets.cmake"
)
configure_file(cmake/meshtoolsConfig.cmake
  "${CMAKE_CURRENT_BINARY_DIR}/meshtools/meshtoolsConfig.cmake"
  COPYONLY
)

set(ConfigPackageLocation lib/cmake/meshtools)
install(EXPORT meshtoolsTargets
  FILE meshtoolsTargets.cmake
  DESTINATION ${ConfigPackageLocation}
)
install(
  FILES
    cmake/meshtoolsConfig.cmake
    "${CMAKE_CURRENT_BINARY_DIR}/meshtools/meshtoolsConfigVersion.cmake"
  DESTINATION
    ${ConfigPackageLocation}
  COMPONENT
    Devel
)
