mkdir -p build-android && \
cd build-android && \
cmake -DCMAKE_TOOLCHAIN_FILE=../android-cmake/android.toolchain.cmake -DCMAKE_BUILD_TYPE=Release -DANDROID_ABI="armeabi" -DANDROID_NATIVE_API_LEVEL=android-21 -DBUILD_MT=OFF -DBUILD_VIEWER=ON -DBUILD_ANDROID=ON ../ && \
cmake --build .
