FROM atlassian/default-image:latest
RUN wget http://www.cmake.org/files/v3.6/cmake-3.6.2.tar.gz && tar -xvf cmake-3.6.2.tar.gz
WORKDIR cmake-3.6.2
RUN ./bootstrap && make && make install
WORKDIR ..
RUN git clone https://www.graphics.rwth-aachen.de:9000/OpenMesh/OpenMesh.git
WORKDIR OpenMesh
RUN mkdir build && cd build/ && cmake ../ && make install
