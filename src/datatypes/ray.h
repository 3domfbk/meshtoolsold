#pragma once

#include <datatypes/bbox.h>
#include <OpenMesh/Core/Geometry/VectorT.hh>

using namespace OpenMesh;

namespace mt {

    struct Ray {
        Vec3f origin;
        Vec3f direction;
        Vec3f directionInv;

        /* Build ray */
        Ray(Vec3f origin, Vec3f direction);

        /**
         * Ray - AABB intersection
         **/
        bool intersectAABB(BBox* bbox);
    };
}