#include <datatypes/circle.h>
#include <utils/types.h>
#include <math.h>

#define MAX_POINT_VALUE 100000.f


using namespace mt;

Circle::Circle(float x, float y, float z, float radius, int rgb) {
	this->position[0] = x;
	this->position[1] = y;
	this->position[2] = z;
	this->radius = radius;
	this->rgb = rgb;

	bbox = BBox();

	for (int i = 0, j = 0; i < 360; i++, j += 3) {

		float angle = i * 2.0 * M_PI / 360.0;

		vertices[j] = x + radius * cos(angle);
		vertices[j + 1] = y + radius * sin(angle);
		vertices[j + 2] = z;

		colors[j] = RED(rgb);
		colors[j + 1] = GREEN(rgb);
		colors[j + 2] = BLUE(rgb);

		elements[i] = i;

		bbox.bounds[0] = MIN(bbox.bounds[0], vertices[j]);
		bbox.bounds[1] = MAX(bbox.bounds[1], vertices[j]);
		bbox.bounds[2] = MIN(bbox.bounds[2], vertices[j+1]);
		bbox.bounds[3] = MAX(bbox.bounds[3], vertices[j+1]);
		bbox.bounds[4] = MIN(bbox.bounds[4], vertices[j+2]);
		bbox.bounds[5] = MAX(bbox.bounds[5], vertices[j+2]);
	}

	this->shiftX = this->shiftY = this->shiftZ = 0;
}

void Circle::setShiftX(double x) {
	this->shiftX = x; 
	this->position[0] -= shiftX;
}

void Circle::setShiftY(double y) {
	this->shiftY = y;
	this->position[1] -= shiftY;
}

void Circle::setShiftZ(double z) {
	this->shiftZ = z;
	this->position[2] -= shiftZ;
}