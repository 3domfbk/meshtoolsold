#pragma once
#include <iostream>
#include <vector>

using namespace std;

namespace mt {

    /**
     * A Memory Pool
     */
    template<class T> class MemoryPool {
    public:

        /**
         * Create a pool with specified initial size
         **/
        MemoryPool(int size) {
            this->capacity = size;
            this->nextAvailable = 0;
            this->currentPool = 0;
            pools.push_back(new T[capacity]);
        }

        ~MemoryPool() {
            for (int i = 0; i < pools.size(); i++) {
                delete[] pools[i];
            }
        }

        /**
         * Grab a new element from pool
         **/
        T* alloc() {
            // Full!
            if (nextAvailable >= capacity) {
                nextAvailable = 0;
                currentPool++;
                pools.push_back(new T[capacity]);
            }

            return &pools.at(currentPool)[nextAvailable++];
        }

    private:
        int capacity;
        vector<T*> pools;
        int currentPool;
        int nextAvailable;
    };
}