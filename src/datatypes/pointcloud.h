#pragma once
#include <datatypes/solid.h>
#include <datatypes/linkedlist.h>

#include <unordered_set>

using namespace std;

namespace mt {

    // Fwd
    class Octree;

    // A point of the cloud

    struct Point {
        float x, y, z;
        float nx, ny, nz;
        int rgb;

        Point() {
        }

        Point(float x, float y, float z, int rgb, float nx, float ny, float nz) {
            this->x = x;
            this->y = y;
            this->z = z;
            this->nx = nx;
            this->ny = ny;
            this->nz = nz;
            this->rgb = rgb;
        }
    };

    // Simplify point list cell
    typedef LinkedList<Point>::ListCell PointCell;

    /**
     * A point cloud.
     * Supports xyz coordinates and color information
     */
    class PointCloud : public Solid {
    public:

        /**
         * Creates a new empty point cloud
         */
        PointCloud(int initialCapacity);

        /**
         * Creates a copy of a point cloud
         **/
        PointCloud(PointCloud* copyFrom);

        /**
         * Releases all data associated to this point cloud
         */
        ~PointCloud();

        /**
         * Adds a point to the cloud
         */
        void addPoint(double x, double y, double z, int rgb, double nx, double ny, double nz);

        /**
         * Changes a point of the cloud
         */
        void setPoint(PointCell* p, float x, float y, float z, int rgb);

        /**
        * Get the point at the specified index
        */
		LinkedList<Point>::ListCell* getPoint(int index);

        /**
         * Mark point for deletion
         **/
        void markForDeletion(PointCell* p);

        /**
         * Build the octree of this pointcloud
         **/
        void buildOctreeNP(int leafMaxPoints);

        /**
         * Build the octree of this pointcloud
         **/
        void buildOctreeLS(float leafMaxSize);

        /**
         * Discards the current octree
         **/
        void deleteOctree();

        /**
         * Update bbox, octree and all the stuff when the cloud changed
         **/
        void update();

        /**
         * Copy the properties from another cloud
         **/
        void copyProps(PointCloud* cloud);

        // Shifts
        void setShiftX(double x);
        void setShiftY(double y);
        void setShiftZ(double z);

        double getShiftX() {
            return shiftX;
        }

        double getShiftY() {
            return shiftY;
        }

        double getShiftZ() {
            return shiftZ;
        }

        bool hasNormals() {
            return points.first->data.nx != -2.0f;
        }

        // Statistics
        float getAvgPointDistance();

        // Accessors
        long long getNumPoints();

        LinkedList<Point>* getData();
        Octree* getOctree();

    private:
        // Points data
        LinkedList<Point> points;
        // Points for deletion
        unordered_set<PointCell*> toDelete;

        // Global shift
        double shiftX, shiftY, shiftZ;

        // Octree
        Octree* octree;
    };
}
