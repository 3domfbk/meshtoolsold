#include <datatypes/pointcloud.h>
#include <datatypes/octree.h>
#include <utils/types.h>
#include <float.h>

#define MAX_POINT_VALUE 100000.f

using namespace mt;

PointCloud::PointCloud(int initialCapacity) : points(initialCapacity) {
    this->octree = NULL;
    this->shiftX = this->shiftY = this->shiftZ = 0;
}

PointCloud::PointCloud(PointCloud* copyFrom){
  LinkedList<Point>* points = copyFrom->getData();
  auto point = points->begin();

  for(; point->next!=NULL; point=point->next){
    double x = point->data.x;
    double y = point->data.y;
    double z = point->data.z;
    float nx = point->data.nx;
    float ny = point->data.ny;
    float nz = point->data.nz;
    int rgb = point->data.rgb;

    x += copyFrom->getShiftX();
    y += copyFrom->getShiftY();
    z += copyFrom->getShiftZ();

    addPoint(x,y,z,rgb,nx,ny,nz);
  }

  copyProps(copyFrom);
  update();
}

PointCloud::~PointCloud() {
    delete octree;
}

void PointCloud::addPoint(double x, double y, double z, int rgb, double nx, double ny, double nz) {

    if (x > MAX_POINT_VALUE && getShiftX() == 0) {
        setShiftX(x);
    }
    if (y > MAX_POINT_VALUE && getShiftY() == 0) {
        setShiftY(y);
    }
    if (z > MAX_POINT_VALUE && getShiftZ() == 0) {
        setShiftZ(z);
    }

    points.add(Point(x - getShiftX(), y - getShiftY(), z - getShiftZ(), rgb, nx, ny, nz));
}

void PointCloud::setPoint(PointCell* p, float x, float y, float z, int rgb) {
    p->data.x = x;
    p->data.y = y;
    p->data.z = z;
    p->data.rgb = rgb;
}

LinkedList<Point>::ListCell* PointCloud::getPoint(int index) {
    
	int i = 0;
	auto point = points.begin();

	for (; point != NULL; point = point->next, i++) {
		if (i == index)
			break;
	}
    return point;
}

void PointCloud::markForDeletion(PointCell* p) {
    toDelete.insert(p);
}

void PointCloud::buildOctreeNP(int leafMaxPoints) {
    delete octree;
    octree = new Octree(this, leafMaxPoints, INFINITY);
}

void PointCloud::buildOctreeLS(float leafMaxSize) {
    delete octree;
    octree = new Octree(this, numeric_limits<int32_t>::max(), leafMaxSize);
}

void PointCloud::deleteOctree() {
    delete octree;
    octree = NULL;
}

void PointCloud::update() {
    bbox = BBox();

    // Delete points marked for deletion
    for (auto it = toDelete.begin(); it != toDelete.end(); it++) {
        points.remove((LinkedList<Point>::ListCell*) * it);
    }

    toDelete.clear();

    // Recalculate bbox
    for (auto it = points.begin(); it != NULL; it = it->next) {
        bbox.bounds[0] = MIN(bbox.bounds[0], it->data.x);
        bbox.bounds[1] = MAX(bbox.bounds[1], it->data.x);
        bbox.bounds[2] = MIN(bbox.bounds[2], it->data.y);
        bbox.bounds[3] = MAX(bbox.bounds[3], it->data.y);
        bbox.bounds[4] = MIN(bbox.bounds[4], it->data.z);
        bbox.bounds[5] = MAX(bbox.bounds[5], it->data.z);
    }

    // Recreate octree
    if (octree != NULL) {
        deleteOctree();
    }
}

void PointCloud::copyProps(PointCloud* cloud) {
    shiftX = cloud->getShiftX();
    shiftY = cloud->getShiftY();
    shiftZ = cloud->getShiftZ();
}

float PointCloud::getAvgPointDistance() {
    buildOctreeNP(8);
    return 1.0f / powf(getOctree()->getDensity(), 1.0f / 3);
}

long long PointCloud::getNumPoints() {
    return points.size();
}

LinkedList<Point>* PointCloud::getData() {
    return &points;
}

Octree* PointCloud::getOctree() {
    return octree;
}

void PointCloud::setShiftX(double x) {
    this->shiftX = x;
    for (auto it = points.begin(); it != NULL; it = it->next) {
        it->data.x -= shiftX;
    }
}

void PointCloud::setShiftY(double y) {
    this->shiftY = y;
    for (auto it = points.begin(); it != NULL; it = it->next) {
        it->data.y -= shiftY;
    }
}

void PointCloud::setShiftZ(double z) {
    this->shiftZ = z;
    for (auto it = points.begin(); it != NULL; it = it->next) {
        it->data.z -= shiftZ;
    }
}
