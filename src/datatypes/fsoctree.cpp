/* 
 * File:   fsoctree.cpp
 * Author: lake
 * 
 * Created on January 3, 2017, 12:47 PM
 */

#include "fsoctree.h"
#include <utils/types.h>
#include <string.h>

using namespace mt;
FsOctree::Octant::Octant(){
    memset(children, 0, sizeof (children));
}

FsOctree::Octant::Octant(Vec3f center, float extent, const char* filename, int numPoints, bool leaf){
    memset(children, 0, sizeof (children));

    this->center = center;
    this->extent = extent;
    strcpy(this->filename, filename);
    this->numPoints = numPoints;
    this->leaf = leaf;
}

FsOctree::Octant::~Octant(){
    for (int i = 0; i < 8; i++) {
        delete children[i];
        children[i] = NULL;
    }
}

// Sphere collisions
bool FsOctree::Octant::inside(float x, float y, float z, float rsquared) {
    Vec3f q(fabs(x - center[0]), fabs(y - center[1]), fabs(z - center[2]));
    q -= Vec3f(-extent, -extent, -extent);
    float l = q.length();
    return l * l < rsquared;
}

bool FsOctree::Octant::overlaps(float x, float y, float z, float rsquared) {
    Vec3f q(fabs(x - center[0]), fabs(y - center[1]), fabs(z - center[2]));

    if (MIN(q[0], q[1]) < extent) return true;
    float l = (q - Vec3f(extent, extent, extent)).length();
    return l * l < rsquared;
}

FsOctree::FsOctree(char* dir, BBox* bbox, FsOctree::Octant* root){
    this->bbox = *bbox;
    float x = bbox->getCenterX();
    float y = bbox->getCenterY();
    float z = bbox->getCenterZ();
    Vec3f center(x, y, z);
    float extent = bbox->extent();
    
    char ofname[64];
    sprintf(ofname, "%s/R.ply", dir);
    this->root = root;
}

FsOctree::FsOctree(char* dir, BBox* bbox, int numPoints) {
    this->bbox = *bbox;
    float x = bbox->getCenterX();
    float y = bbox->getCenterY();
    float z = bbox->getCenterZ();
    Vec3f center(x, y, z);
    float extent = bbox->extent();
    
    char ofname[64];
    sprintf(ofname, "%s/R.ply", dir);
    root = new Octant(center, extent, ofname, numPoints, false);
}

FsOctree::~FsOctree() {
    delete root;
}

FsOctree::Octant* FsOctree::getRoot() {
    return root;
}

BBox* FsOctree::getBBox(){
    return &bbox;
}