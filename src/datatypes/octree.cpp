#include <datatypes/octree.h>
#include <utils/types.h>
#include <string.h>
#include <iostream>

#define MAX_SPLIT_LEVEL 20

using namespace std;
using namespace mt;

Octree::Octant::Octant(Vec3f center, float extent, PointCell* startIdx, PointCell* endIdx, int numPoints) {
    memset(children, 0, sizeof (children));

    this->center = center;
    this->extent = extent;
    this->start = startIdx;
    this->end = endIdx;
    this->numPoints = numPoints;
    this->leaf = true;
}

Octree::Octant::~Octant() {
    for (int i = 0; i < 8; i++) {
        delete children[i];
        children[i] = NULL;
    }
}

void Octree::Octant::split(PointCloud* cloud, int maxLeafPoints, float maxLeafSize, int level) {
    // Divide if needed
    if ((numPoints > maxLeafPoints || extent > maxLeafSize) && level < MAX_SPLIT_LEVEL) {
        if (leaf) {
            leaf = false;

            int mk[8] = {0}; // Each child numPoints
            PointCell * sk[8] = {0}; // Each child startIdx
            PointCell * tk[8] = {0}; // Each child endIdx

            // Scan all points of this octant
            PointCell* i = start;
            for (int j = 0; j < numPoints; j++) {
                // Find which child octant the points belongs to
                bool x = i->data.x > center[0];
                bool y = i->data.y > center[1];
                bool z = i->data.z > center[2];

                int cid = (int) (x << 2) | (int) (y << 1) | (int) z;

                if (mk[cid] == 0) sk[cid] = i;
                else tk[cid]->setNext(i);

                tk[cid] = i;
                mk[cid]++;

                // Next point
                i = i->next;
            }

            // Create all children that have at least one point.
            float halfExtent = extent / 2;
            int lastChild = -1;
            bool isFirst = true;
            for (int c = 0; c < 8; c++) {
                // Must have at least one points
                if (mk[c] > 0) {
                    // Calculate center
                    float cx = ((c >> 2) & 1) ? center[0] + halfExtent : center[0] - halfExtent;
                    float cy = ((c >> 1) & 1) ? center[1] + halfExtent : center[1] - halfExtent;
                    float cz = (c & 1) ? center[2] + halfExtent : center[2] - halfExtent;

                    // Recurse
                    children[c] = new Octant(Vec3f(cx, cy, cz), halfExtent, sk[c], tk[c], mk[c]);
                    children[c]->split(cloud, maxLeafPoints, maxLeafSize, level + 1);

                    if (isFirst) {
                        isFirst = false;
                        // Update parent octant start
                        start = children[c]->start;
                    }                        // Update previous octant end's successor
                    else {
                        children[lastChild]->end->setNext(children[c]->start);
                    }

                    // Set end of whole octant
                    end = children[c]->end;
                    // Remember last octant visited
                    lastChild = c;
                }
            }
        }            // Not a leaf, just recur
        else {
            for (int c = 0; c < 8; c++) {
                if (children[c]) children[c]->split(cloud, maxLeafPoints, maxLeafSize, level + 1);
            }
        }
    }
}

bool Octree::Octant::inside(float x, float y, float z, float rsquared) {
    Vec3f q(fabs(x - center[0]), fabs(y - center[1]), fabs(z - center[2]));
    q -= Vec3f(-extent, -extent, -extent);
    float l = q.length();
    return l * l < rsquared;
}

bool Octree::Octant::overlaps(float x, float y, float z, float rsquared) {
    Vec3f q(fabs(x - center[0]), fabs(y - center[1]), fabs(z - center[2]));

    if (MIN(q[0], q[1]) < extent) return true;
    float l = (q - Vec3f(extent, extent, extent)).length();
    return l * l < rsquared;
}

Octree::Octree(PointCloud* cloud, int maxLeafPoints, float maxLeafSize) {
    this->cloud = cloud;

    // Create tree
    float x = cloud->getBBox()->getCenterX();
    float y = cloud->getBBox()->getCenterY();
    float z = cloud->getBBox()->getCenterZ();
    Vec3f center(x, y, z);
    float extent = cloud->getBBox()->extent();
    root = new Octant(center, extent,
            (PointCell*) cloud->getData()->begin(),
            (PointCell*) cloud->getData()->end(),
            (int) cloud->getNumPoints());

    root->split(cloud, maxLeafPoints, maxLeafSize, 0);

    // Adjust point list start and end!
    cloud->getData()->first = root->start;
    cloud->getData()->last = root->end;
    cloud->getData()->last->next = NULL;
}

Octree::~Octree() {
    delete root;
}

int Octree::leafsLS(float maxLeafSize) {
    return leafsLS(root, maxLeafSize);
}

int Octree::leafsLS(Octant* octant, float maxLeafSize) {
    if (octant->extent < maxLeafSize) return octant->numPoints;

    // Sum up children
    int sum = 0;
    for (int i = 0; i < 8; i++) {
        if (octant->children[i]) {
            sum += leafsLS(octant->children[i], maxLeafSize);
        }
    }

    return sum;
}

PointCloud* Octree::getCloud() {
    return cloud;
}

Octree::Octant* Octree::getRoot() {
    return root;
}

float Octree::getVolume() {
    return getVolume(root);
}

float Octree::getVolume(Octree::Octant* octant) {
    if (octant->leaf) return powf(octant->extent, 3);

    float volume = 0;
    for (int i = 0; i < 8; i++) {
        if (octant->children[i]) volume += getVolume(octant->children[i]);
    }

    return volume;
}

float Octree::getDensity() {
    return cloud->getNumPoints() / getVolume();
}

void Octree::dump(Octree::Octant* octant, int indent) {
    for (int i = 0; i < indent; i++) cout << "  ";
    cout << octant->start << "->" << octant->end << " n=(" << octant->numPoints << ") succ=" << octant->end->next << endl;

    for (int i = 0; i < 8; i++) {
        if (octant->children[i]) dump(octant->children[i], indent + 1);
    }
}

PointCell* Octree::nearest(Ray &ray, float &outDistance) {
    vector<Octant*> candidates;

    raySearch(root, ray, candidates);

    // Search for closest point
    PointCell* closest = NULL;
    float distanceSq = INFINITY;
    for (auto it = candidates.begin(); it != candidates.end(); it++) {
        // Search all points
        PointCell* p = (*it)->start;
        for (int i = 0; i < (*it)->numPoints; i++) {
            Vec3d pt(p->data.x, p->data.y, p->data.z);
            Vec3d l = (ray.direction % (pt - ray.origin));

            float d = (float) l.length();

            if (d < distanceSq) {
                closest = p;
                distanceSq = d;
            }

            p = p->next;
        }
    }

    outDistance = sqrt(distanceSq);
    return closest;
}

void Octree::raySearch(Octant* octant, Ray& ray, vector<Octant*> &result) {
    BBox box;
    box.bounds[0] = octant->center[0] - octant->extent;
    box.bounds[1] = octant->center[0] + octant->extent;
    box.bounds[2] = octant->center[1] - octant->extent;
    box.bounds[3] = octant->center[1] + octant->extent;
    box.bounds[4] = octant->center[2] - octant->extent;
    box.bounds[5] = octant->center[2] + octant->extent;
    if (!ray.intersectAABB(&box)) {
        return;
    }

    if (octant->leaf) result.push_back(octant);
    else {
        for (int c = 0; c < 8; c++) {
            if (octant->children[c] != NULL) {
                raySearch(octant->children[c], ray, result);
            }
        }
    }
}

void Octree::nearestNeighbor(float x, float y, float z, float radius, vector<PointCell*>* out) {
    float rsquared = radius * radius;

    // Do radius search starting from root
    radiusSearch(root, x, y, z, rsquared, out);
}

void Octree::radiusSearch(Octant* octant, float x, float y, float z, float rsquared, vector<PointCell*>* out) {
    // If the whole octant is inside the search ball, add all the points
    if (octant->inside(x, y, z, rsquared)) {
        PointCell* i = octant->start;
        for (int j = 0; j < octant->numPoints; j++) {
            out->push_back(i);
            i = i->next;
        }
        return;
    }

    // If leaf, check all points
    if (octant->leaf) {
        PointCell* i = octant->start;
        for (int j = 0; j < octant->numPoints; j++) {
            // Compute distance squared
            float dx = i->data.x - x;
            float dy = i->data.y - y;
            float dz = i->data.z - z;
            float dsquared = dx * dx + dy * dy + dz*dz;
            if (dsquared < rsquared) out->push_back(i);
            i = i->next;
        }
        return;
    }

    // Recursion
    for (int c = 0; c < 8; c++) {
        if (octant->children[c] != NULL && octant->overlaps(x, y, z, rsquared)) {
            radiusSearch(octant->children[c], x, y, z, rsquared, out);
        }
    }
}
