#pragma once
#include <MeshtoolsConfig.h>
#include <datatypes/solid.h>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <ACG/Geometry/bsp/TriangleBSPT.hh>

#include <vector>

using namespace std;
using namespace OpenMesh;

namespace mt {

    /**
     * A mesh.
     */
    class Mesh : public TriMesh_ArrayKernelT<>, public Solid {
    public:

        /**
         * Creates a new empty mesh
         */
        Mesh();

        /**
         * Releases all data associated to this point cloud
         */
        ~Mesh();

        /**
         * Re-calculate the bounding box
         */
        void recalculateBbox();

        /**
         * Update mesh after modification
         */
        void update();

        /**
         * Create the bsp
         **/
        void buildBsp();

        /**
         * Delete the bsp
         **/
        void deleteBsp();

        /**
         * Gets the bsp
         **/
        OpenMeshTriangleBSPT< Mesh >* getBsp();

        /**
         * Gets the area of the surface
         **/
        float getSurfaceArea();

        /**
         * Gets the point density (points/surface)
         **/
        float getPointDensity();

    private:
        // BSP
        OpenMeshTriangleBSPT< Mesh >* bsp;
    };
}