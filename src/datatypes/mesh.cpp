#include <datatypes/mesh.h>
#include <utils/types.h>

using namespace mt;

Mesh::Mesh() {
    bsp = NULL;
}

Mesh::~Mesh() {
    delete bsp;
}

void Mesh::recalculateBbox() {
    bbox = BBox();
    for (TriMesh_ArrayKernelT<>::VertexIter v = vertices_begin();
            v != vertices_end(); v++) {
        bbox.bounds[0] = MIN(bbox.bounds[0], point(*v)[0]);
        bbox.bounds[1] = MAX(bbox.bounds[1], point(*v)[0]);
        bbox.bounds[2] = MIN(bbox.bounds[2], point(*v)[1]);
        bbox.bounds[3] = MAX(bbox.bounds[3], point(*v)[1]);
        bbox.bounds[4] = MIN(bbox.bounds[4], point(*v)[2]);
        bbox.bounds[5] = MAX(bbox.bounds[5], point(*v)[2]);
    }
}

void Mesh::update() {
    // Commit deletions
    garbage_collection();

    // Calculate bbox
    recalculateBbox();

    // Recalculate normals
    update_normals();
    update_face_normals();
    update_halfedge_normals();

    // Update bsp
    if (bsp) buildBsp();
}

void Mesh::buildBsp() {
    delete bsp;

    // create Triangle BSP
    bsp = new OpenMeshTriangleBSPT< Mesh >(*this);

    // build Triangle BSP
    bsp->reserve(n_faces());

    typename Mesh::FIter f_it = faces_begin();
    typename Mesh::FIter f_end = faces_end();

    for (; f_it != f_end; ++f_it)
        bsp->push_back(*f_it);

    bsp->build(10, 100);
}

void Mesh::deleteBsp() {
    delete bsp;
}

OpenMeshTriangleBSPT< Mesh >* Mesh::getBsp() {
    return bsp;
}

float Mesh::getSurfaceArea() {
    // Process all faces
    float area = 0;
    for (Mesh::FaceIter fx = faces_begin(); fx != faces_end(); fx++) {
        area += calc_sector_area(halfedge_handle(*fx));
    }

    return area;
}

float Mesh::getPointDensity() {
    return n_vertices() / getSurfaceArea();
}