#pragma once
#include <datatypes/solid.h>

using namespace std;

namespace mt {

	/**
	* A circle.
	* Supports xyz coordinates and color information
	*/
	class Circle : public Solid {
	public:
		/**
		* Creates a new circle with specified position, radius and color
		*/
		Circle(float x, float y, float z, float radius, int rgb);

		// Shifts
		void setShiftX(double x);
		void setShiftY(double y);
		void setShiftZ(double z);

		double getShiftX() {
			return shiftX;
		}

		double getShiftY() {
			return shiftY;
		}

		double getShiftZ() {
			return shiftZ;
		}

		float vertices[360 * 3];
		float colors[360 * 3];
		short elements[360];

	private:
		
		float position[3];
		float radius;
		int rgb;

		// Global shift
		double shiftX, shiftY, shiftZ;
	};
}