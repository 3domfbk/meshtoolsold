#pragma once
#include <datatypes/pointcloud.h>
#include <datatypes/forwardlinkedlist.h>
#include <datatypes/ray.h>
#include <math/matrix.h>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <vector>

using namespace OpenMesh;
using namespace std;

namespace mt {

    /**
     * A point cloud.
     * Supports xyz coordinates and color information
     */
    class Octree {
    public:
        // An octant of the octree

        typedef struct Octant {
            Octant(Vec3f center, float extent, PointCell* startIdx, PointCell* endIdx, int numPoints);
            ~Octant();

            // Recursive slice
            void split(PointCloud* cloud, int maxLeafPoints, float maxLeafSize, int level);

            // Sphere collisions
            bool inside(float x, float y, float z, float rsquared);
            bool overlaps(float x, float y, float z, float rsquared);

            Octant* children[8];
            Vec3f center;
            float extent;
            PointCell* start;
            PointCell* end;
            int numPoints;
            bool leaf;
        } Octant;

        /**
         * Creates a new octree with max points in a node
         */
        Octree(PointCloud* cloud, int maxLeafPoints, float maxLeafSize);

        /**
         * Releases all data associated to this point cloud
         */
        ~Octree();

        /**
         * Gets the number of nodes smaller than specified leaf size
         **/
        int leafsLS(float maxLeafSize);

        /**
         * Search for the nearest neighbors within the specified radius
         * to the specified points, and puts them in the out list
         */
        void nearestNeighbor(float x, float y, float z, float radius, vector<PointCell*>* out);

        /**
         * Calculates nearest point to specified ray
         **/
        PointCell* nearest(Ray &ray, float &outDistance);

        /**
         * Get backing point cloud
         */
        PointCloud* getCloud();

        /**
         * Gets root node
         **/
        Octant* getRoot();

        /**
         * Calculate actually occupied volume (that is, the total volume of the leafs)
         **/
        float getVolume();

        /**
         * Calculate actual average point density (leaf-size accurate)
         **/
        float getDensity();

        /**
         * Dump the tree structure to stdout
         **/
        void dump(Octant* octant, int indent);

    private:

        // Radius search
        void radiusSearch(Octant* octant, float x, float y, float z, float radius, vector<PointCell*>* out);

        // Ray search
        void raySearch(Octant* octant, Ray& ray, vector<Octant*> &result);

        // Leaf count
        int leafsLS(Octant* octant, float maxLeafSize);

        // Volume
        float getVolume(Octant* octant);

        // Backing point cloud
        PointCloud* cloud;

        // Root node
        Octant* root;
    };
}
