#include <datatypes/voxelgrid.h>
#include <string.h>

using namespace mt;

VoxelGrid::VoxelGrid(PointCloud* cloud, float voxelSide) :
width((int) (cloud->getBBox()->width() / voxelSide + 1)),
height((int) (cloud->getBBox()->height() / voxelSide + 1)),
depth((int) (cloud->getBBox()->depth() / voxelSide + 1)),
points(getNumVoxels()) {
    this->pointCloud = cloud;
    this->voxelSide = voxelSide;
    this->actualVoxelNum = 0;
    cout << "Allocating " << getNumVoxels() << " voxels" << endl;
    this->voxels = new Voxel[getNumVoxels()];
    memset(this->voxels, 0, sizeof (Voxel) * getNumVoxels());

    for (PointCell* it = pointCloud->getData()->begin(); it != NULL; it = it->next) {
        points.add(it);
    }

    // Create voxel grid
    for (VoxelPoint* it = (VoxelPoint*) points.begin(); it != NULL; it = it->next) {
        float x = it->data->data.x;
        float y = it->data->data.y;
        float z = it->data->data.z;
        Voxel* v = getVoxel(x, y, z);

        // Create voxel if needed
        if (v->numPoints == 0) {
            v->start = it;
            actualVoxelNum++;
        } else {
            v->end->next = it;
        }

        v->end = it;
        v->numPoints++;
    }
}

VoxelGrid::~VoxelGrid() {
    delete[] voxels;
}

Voxel* VoxelGrid::getVoxel(float x, float y, float z) {
    int xv = (int) ((x - pointCloud->getBBox()->bounds[0]) / voxelSide);
    int yv = (int) ((y - pointCloud->getBBox()->bounds[2]) / voxelSide);
    int zv = (int) ((z - pointCloud->getBBox()->bounds[4]) / voxelSide);
    int key = id(xv, yv, zv);
    if (key < 0 || key >= getNumVoxels()) return NULL;
    return &voxels[key];
}

PointCloud* VoxelGrid::getPointCloud() {
    return pointCloud;
}

Voxel* VoxelGrid::getVoxels() {
    return voxels;
}

int VoxelGrid::getNumVoxels() {
    return width * height * depth;
}

float VoxelGrid::getVoxelSide() {
    return voxelSide;
}

int VoxelGrid::getActualVoxelNum() {
    return actualVoxelNum;
}

int VoxelGrid::idX(int id) {
    return id % width;
}

int VoxelGrid::idY(int id) {
    return (id / width) % height;
}

int VoxelGrid::idZ(int id) {
    return id / (width * height);
}

int VoxelGrid::id(int x, int y, int z) {
    return x + y * width + z * width*height;
}

// Accessors

int VoxelGrid::getWidth() {
    return width;
}

int VoxelGrid::getHeight() {
    return height;
}

int VoxelGrid::getDepth() {
    return depth;
}
