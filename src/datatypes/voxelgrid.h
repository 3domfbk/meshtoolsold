#pragma once
#include <datatypes/pointcloud.h>
#include <datatypes/forwardlinkedlist.h>
#include <utils/types.h>
#include <unordered_map>
#include <vector>

using namespace std;

namespace mt {

    typedef FwdLinkedList<PointCell*>::ListCell VoxelPoint;

    /**
     * One voxel
     */
    typedef struct Voxel {
        VoxelPoint* start;
        VoxelPoint* end;
        int numPoints;
    } Voxel;

    /**
     * A voxel grid
     */
    class VoxelGrid {
    public:

        /**
         * Creates a new voxel grid with specified voxel side size
         */
        VoxelGrid(PointCloud* cloud, float voxelSide);

        /**
         * Releases all data associated to this structure
         */
        ~VoxelGrid();

        /**
         * Obtains a voxel from 3d location
         */
        Voxel* getVoxel(float x, float y, float z);

        /**
         * Obtains the number of voxels
         */
        int getNumVoxels();

        /**
         * Gets point next to p in list
         **/
        int next(int p);

        /**
         * Obtains the backing point cloud
         */
        PointCloud* getPointCloud();

        /**
         * Get all voxels (for iteration)
         */
        Voxel* getVoxels();

        /**
         * Get voxel side
         */
        float getVoxelSide();

        /**
         * Get the number of actual, non-empty voxels
         */
        int getActualVoxelNum();

        // ID calculation
        int idX(int id);
        int idY(int id);
        int idZ(int id);
        int id(int x, int y, int z);

        // Accessors
        int getWidth();
        int getHeight();
        int getDepth();

    private:
        // Point cloud
        PointCloud* pointCloud;

        // Leaf size
        float voxelSide;

        // Grid size
        int width, height, depth;

        // Voxels
        Voxel* voxels;
        int actualVoxelNum;

        // List
        FwdLinkedList<PointCell*> points;
    };
}