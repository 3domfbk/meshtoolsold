#include <datatypes/bbox.h>
#include <utils/types.h>
#include <float.h>

using namespace mt;

BBox::BBox() {
    bounds[0] = FLT_MAX;
    bounds[1] = -FLT_MAX;
    bounds[2] = FLT_MAX;
    bounds[3] = -FLT_MAX;
    bounds[4] = FLT_MAX;
    bounds[5] = -FLT_MAX;
}

float BBox::getCenterX() {
    return (bounds[0] + bounds[1]) / 2;
}

float BBox::getCenterY() {
    return (bounds[2] + bounds[3]) / 2;
}

float BBox::getCenterZ() {
    return (bounds[4] + bounds[5]) / 2;
}

float BBox::width() {
    return bounds[1] - bounds[0];
}

float BBox::height() {
    return bounds[3] - bounds[2];
}

float BBox::depth() {
    return bounds[5] - bounds[4];
}

float BBox::volume() {
    return (bounds[1] - bounds[0]) * (bounds[3] - bounds[2]) * (bounds[5] - bounds[4]);
}

float BBox::extent() {
    float w = bounds[1] - bounds[0];
    float h = bounds[3] - bounds[2];
    float d = bounds[5] - bounds[4];

    return MAX(w, MAX(h, d)) / 2.0f;
}
