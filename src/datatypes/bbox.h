#pragma once

namespace mt {

    struct BBox {
        float bounds[6];

        float getCenterX();
        float getCenterY();
        float getCenterZ();

        float width();
        float height();
        float depth();

        float volume();
        float extent();

        BBox();
    };
}