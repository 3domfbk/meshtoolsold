#pragma once
#include <MeshtoolsConfig.h>
#include <datatypes/bbox.h>

namespace mt {

    /**
     * A solid object
     */
    class Solid {
    public:

        /**
         * Releases all data associated to this solid
         */
        virtual ~Solid();

        /**
         * Get bbox
         */
        BBox* getBBox();

    protected:
        // Bounding box
        BBox bbox;
    };
}