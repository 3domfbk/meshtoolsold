/* 
 * File:   fsoctree.h
 * Author: lake
 *
 * Created on January 3, 2017, 12:47 PM
 */
#pragma once

#include <datatypes/linkedlist.h>
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include "bbox.h"

using namespace OpenMesh;

namespace mt {
    
    /**
     * An octree where the leaf points are stored 
     * on the file system as individual files
     **/
    class FsOctree {
    public:
        // An octant of the octree
        typedef struct Octant {
            Octant();
            Octant(Vec3f center, float extent, const char* filename, int numPoints, bool leaf);
            ~Octant();
            
            // Sphere collisions
            bool inside(float x, float y, float z, float rsquared);
            bool overlaps(float x, float y, float z, float rsquared);

            Octant* children[8];
            Vec3f center;
            float extent;
            char filename[64];
            int numPoints;
            bool leaf;
        } Octant;
        
        FsOctree(char* dir, BBox* bbox, Octant* root);
        /**
         * Creates a new fsoctree
         */
        FsOctree(char* dir, BBox* bbox, int numPoints);

        /**
         * Releases all data associated to this octree
         */
        ~FsOctree();
        
        /**
         * @return The root
         */
        Octant* getRoot();
        
        /**
         * @return The bounding box
         */
        BBox* getBBox();
        
    private:
        BBox bbox;
        Octant* root;
    };
}
