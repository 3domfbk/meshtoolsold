#include <datatypes/ray.h>
#include <utils/types.h>

using namespace mt;

Ray::Ray(Vec3f origin, Vec3f direction) {
    this->origin = origin;
    this->direction = direction;
    this->directionInv = Vec3f(1, 1, 1) / direction;
}

bool Ray::intersectAABB(BBox* bbox) {
    double tmin = -INFINITY, tmax = INFINITY;

    double tx1 = (bbox->bounds[0] - origin[0]) * directionInv[0];
    double tx2 = (bbox->bounds[1] - origin[0]) * directionInv[0];

    tmin = MIN(tx1, tx2);
    tmax = MAX(tx1, tx2);

    double ty1 = (bbox->bounds[2] - origin[1]) * directionInv[1];
    double ty2 = (bbox->bounds[3] - origin[1]) * directionInv[1];

    tmin = MAX(tmin, MIN(ty1, ty2));
    tmax = MIN(tmax, MAX(ty1, ty2));

    double tz1 = (bbox->bounds[4] - origin[2]) * directionInv[2];
    double tz2 = (bbox->bounds[5] - origin[2]) * directionInv[2];

    tmin = MAX(tmin, MIN(tz1, tz2));
    tmax = MIN(tmax, MAX(tz1, tz2));

    return tmax >= tmin;
}