#pragma once
#include <iostream>
#include <datatypes/memorypool.h>

namespace mt {

    /**
     * A single-link LinkedList (awww yeah)
     */
    template<class T> class FwdLinkedList {
    public:

        struct ListCell {
            T data;
            ListCell* next;

            ListCell() {
                next = NULL;
            }
        };

        /**
         * Create an empty list
         **/
        FwdLinkedList(int poolSize = 1000) : cellPool(poolSize) {
            elements = 0;
            first = NULL;
            last = NULL;
        }

        /**
         * Add an element to the list
         **/
        void add(T element) {
            elements++;

            // First element
            if (last == NULL) {
                first = cellPool.alloc();
                first->data = element;
                last = first;
            }                // General case
            else {
                last->next = cellPool.alloc();
                last->next->data = element;
                last = last->next;
            }
        }

        /**
         * Returns beginning of list
         **/
        ListCell* begin() {
            return first;
        }

        /**
         * Returns end of list
         **/
        ListCell* end() {
            return last;
        }

        /**
         * Returns number of elements
         **/
        long long size() {
            return elements;
        }

    private:
        MemoryPool<ListCell> cellPool;
        ListCell* first;
        ListCell* last;
        long long elements;
    };
}