#pragma once
#include <iostream>
#include <datatypes/memorypool.h>

namespace mt {

    /**
     * A LinkedList (aw yeah)
     */
    template<class T> class LinkedList {
    public:

        struct ListCell {
            T data;
            ListCell* previous;
            ListCell* next;

            ListCell() {
                previous = NULL;
                next = NULL;
            }

            void setNext(ListCell* cell) {
                next = cell;
                if (cell != NULL) {
                    cell->previous = this;
                }
            }
        };

        /**
         * Create an empty list
         **/
        LinkedList(int poolSize = 1000) : cellPool(poolSize) {
            elements = 0;
            first = NULL;
            last = NULL;
        }

        /**
         * Add an element to the list
         **/
        void add(T element) {
            elements++;

            // First element
            if (last == NULL) {
                first = cellPool.alloc();
                first->data = element;
                last = first;
            }// General case
            else {
                ListCell* newCell = cellPool.alloc();
                newCell->data = element;
                last->setNext(newCell);
                last = newCell;
            }
        }

        /**
         * Remove an element of known location
         * The cell will be removed from list
         **/
        void remove(ListCell* cell) {
            if (cell == first) {
                first = cell->next;
            }
            if (cell == last) {
                last = cell->previous;
            }

            if (cell->previous != NULL) {
                cell->previous->setNext(cell->next);
            }
            elements--;
        }

        /**
         * Returns beginning of list
         **/
        ListCell* begin() {
            return first;
        }

        /**
         * Returns end of list
         **/
        ListCell* end() {
            return last;
        }

        /**
         * Returns number of elements
         **/
        long long size() {
            return elements;
        }


        // State (should not access directly)
        MemoryPool<ListCell> cellPool;
        ListCell* first;
        ListCell* last;
        long long elements;
    };
}