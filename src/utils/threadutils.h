#pragma once

#ifdef _WIN32
#include <windows.h>
#define THREAD HANDLE
#define CREATE_THREAD(handle, f, args, idout) handle = CreateThread(NULL, 0, f, args, 0, &idout)
#define THREADFUNC(name) DWORD WINAPI name( LPVOID lpParam )
#define THREADID DWORD
#define MUTEX HANDLE
#define CREATE_MUTEX(handle) handle = CreateMutex(NULL, FALSE, NULL)
#define LOCK(handle) WaitForSingleObject(handle, INFINITE)
#define UNLOCK(handle) ReleaseMutex(handle)
#define RELEASE_MUTEX(handle) CloseHandle(handle)
#else
#include <pthread.h>
#define THREAD pthread_t
#define CREATE_THREAD(handle, f, args, idout) idout = pthread_create(&handle, NULL, f, (void *)args);
#define THREADFUNC(name) void* name( void* threadid )
#define THREADID long
#define MUTEX pthread_mutex_t
#define CREATE_MUTEX(handle) pthread_mutex_init(&handle, NULL)
#define LOCK(handle) pthread_mutex_lock (&handle)
#define UNLOCK(handle) pthread_mutex_unlock (&handle)
#define RELEASE_MUTEX(handle) pthread_mutex_destroy(&handle)
#endif
