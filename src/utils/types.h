#pragma once
#include <unordered_map>
#include <limits>
#include <cstdint>
#include <MeshtoolsConfig.h>

// Power of 2
#define POW2(x) (1LL<<(x))

// Min and maxie moe
#define MIN(a,b) (a<b?a:b)
#define MAX(a,b) (a>b?a:b)

// Some primes
#define P1 74980951
#define P2 15485863
#define P3 85880369

// Some epsilon
#define EPSILON 0.000001

// RGB conversions
#define BGR(r, g, b) ((b<<16) | (g<<8) | r)
#define BLUE(rgb) ((rgb >> 16) & 0xFF)
#define GREEN(rgb) ((rgb >> 8) & 0xFF)
#define RED(rgb) (rgb & 0xFF)

namespace mt {
    // Byte type
    typedef unsigned char byte;
}

#ifdef BUILD_ANDROID
#include <android/log.h>
#define  LOGE(...)  __android_log_print(ANDROID_LOG_ERROR, "MESHTOOLS", __VA_ARGS__)
#define  LOGI(...)  __android_log_print(ANDROID_LOG_INFO, "MESHTOOLS", __VA_ARGS__)
#else
#define  LOGE(...)  fprintf(stderr, __VA_ARGS__)
#define  LOGI(...)  fprintf(stdout, __VA_ARGS__)
#endif

#ifdef _WIN32
#ifdef MODULE_API_EXPORTS
#define MODULE_API __declspec(dllexport)
#else
#define MODULE_API __declspec(dllimport)
#endif
#else
#define MODULE_API
#endif
