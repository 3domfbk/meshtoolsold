#include <utils/stringutils.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

namespace mt {

    bool strStartsWith(const char* string, const char* prefix) {
        return strncmp(string, prefix, strlen(prefix)) == 0;
    }

    bool strEndsWith(const char* string, const char* suffix) {
        return strncmp(string + strlen(string) - strlen(suffix), suffix, strlen(suffix)) == 0;
    }

    bool strEquals(const char* string1, const char* string2) {
        return strcmp(string1, string2) == 0;
    }

    char** strSplit(char* a_str, const char a_delim, int& numTokens) {
        char** result = 0;
        size_t count = 0;
        char* tmp = a_str;
        char* last_comma = 0;
        char delim[2];
        delim[0] = a_delim;
        delim[1] = 0;

        /* Count how many elements will be extracted. */
        while (*tmp) {
            if (a_delim == *tmp) {
                count++;
                last_comma = tmp;
            }
            tmp++;
        }

        /* Add space for trailing token. */
        count += last_comma < (a_str + strlen(a_str) - 1);

        result = (char**) malloc(sizeof (char*) * count);

        if (result) {
            size_t idx = 0;
            char* token = strtok(a_str, delim);

            while (token) {
                assert(idx < count);
                *(result + idx++) = strdup(token);
                token = strtok(0, delim);
            }
        }

        numTokens = (int) count;
        return result;
    }

    char* strSep(char** str, const char* delim) {
        char* start = *str;
        char* p;

        p = (start != NULL) ? strpbrk(start, delim) : NULL;
        while (p != NULL && *(p + 1) == *delim) p++;

        if (p == NULL) {
            *str = NULL;
        } else {
            *p = '\0';
            *str = p + 1;
        }

        return start;
    }
}
