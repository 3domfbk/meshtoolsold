#pragma once

#ifdef _WIN32
#include <direct.h>
#define MKDIR(a) _mkdir(a)
#else
#include <sys/stat.h>
#define MKDIR(a) mkdir(a, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH)
#endif
