#include <utils/byteutils.h>
#include <string.h>

namespace mt {

    float readFloat(byte* buffer, bool lilEndian) {
        // Little endian
        if (lilEndian) {
            return ((float*) buffer)[0];
        }

        // Big endian
        byte tmp[4];
        tmp[0] = buffer[3];
        tmp[1] = buffer[2];
        tmp[2] = buffer[1];
        tmp[3] = buffer[0];
        return ((float*) tmp)[0];
    }
    
    void writeInt(int value, byte* buffer) {
        memcpy(buffer, &value, sizeof (value));
    }

    double readDouble(byte* buffer, bool lilEndian) {
        // Little endian
        if (lilEndian) {
            return ((double*)buffer)[0];
        }

        // Big endian
        byte tmp[4];
        tmp[0] = buffer[7];
        tmp[1] = buffer[6];
        tmp[2] = buffer[5];
        tmp[3] = buffer[4];
        tmp[4] = buffer[3];
        tmp[5] = buffer[2];
        tmp[6] = buffer[1];
        tmp[7] = buffer[0];
        return ((double*)tmp)[0];
    }

    void writeFloat(float value, byte* buffer) {
        memcpy(buffer, &value, sizeof (value));
    }

    void writeDouble(double value, byte* buffer) {
        memcpy(buffer, &value, sizeof (value));
    }
}