#pragma once

namespace mt {
    /**
     * Determines if string starts with given prefix
     */
    bool strStartsWith(const char* string, const char* prefix);

    /**
     * Determines if string ends with given suffix
     */
    bool strEndsWith(const char* string, const char* suffix);

    /**
     * Determines if strings are equal
     */
    bool strEquals(const char* string1, const char* string2);

    /**
     * Splits a string into tokens (allocates memory)
     */
    char** strSplit(char* str, const char delimiter, int &numTokens);

    /**
     * Custom implementation of gnu strsep
     **/
    char* strSep(char** str, const char* delim);
}