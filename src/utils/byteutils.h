#pragma once
#include <utils/types.h>

namespace mt {

    /**
     * Reads a float value from a byte buffer
     */
    float readFloat(byte* buffer, bool lilEndian);

    /**
    * Reads a double value from a byte buffer
    */
    double readDouble(byte* buffer, bool lilEndian);

    /**
     * Writes an int value to a byte buffer
     */
    void writeInt(int value, byte* buffer);
    
    /**
     * Writes a float value to a byte buffer
     */
    void writeFloat(float value, byte* buffer);

    /**
     * Writes a double value to a byte buffer
     */
    void writeDouble(double value, byte* buffer);
}