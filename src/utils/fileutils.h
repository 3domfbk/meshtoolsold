#pragma once
#include <stdio.h>

namespace mt {
    /**
     * Reads one line.
     * The buffer must be large enough
     */
    int readLine(FILE* file, char* buffer);
}