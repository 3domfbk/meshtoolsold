#include <utils/fileutils.h>
#include <string.h>

namespace mt {

    int readLine(FILE* file, char* buffer) {
        // Read 1 character at time until \n
        int i = 0;
        while (true) {
            fread(&buffer[i], 1, 1, file);
            if (buffer[i] == '\n') break;
            if (buffer[i] == '\r') {
                fread(&buffer[i], 1, 1, file);
                if (buffer[i] != '\n') {
                    fseek(file, -1, SEEK_CUR);
                }
                break;
            }

            i++;
        }
        buffer[i] = '\0';

        return i;
    }
}
