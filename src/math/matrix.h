#pragma once
#include <MeshtoolsConfig.h>
#include <OpenMesh/Core/Geometry/VectorT.hh>

using namespace OpenMesh;

namespace mt {

    /**
     * The 4x4 matrix and its mathematical operations
     */
    struct Matrix {
    public:

        Matrix();

        /**
         * Set the identity
         */
        void setIdentity();

        /**
         * Sets perspective transform
         */
        void setPerspective(float fovDegrees, float aspectRatio, float znear, float zfar);

        /**
         * Sets frustum transform
         */
        void setFrustum(float left, float right, float bottom, float top, float znear, float zfar);

        /**
         * Sets ortho transform
         */
        void setOrtho(float left, float right, float bottom, float top, float znear, float zfar);

        /**
         * Sets lookat transform
         */
        void setLookAt(float eyex, float eyey, float eyez, float centerx, float centery, float centerz, float upx, float upy, float upz);

        /**
         * Translates by specified amount
         */
        void translate(float x, float y, float z);
        
        /**
         * Scales by specified amount
         */
        void scale(float x, float y, float z);

        /**
         * Multiply by another matrix
         */
        void multiply(Matrix &right);

        // Get translation vector
        Vec3f getTranslation();

        // Get raw data
        float* getData();

        // Data individual access
        float& operator[](int id);

        // Operators
        Matrix operator*(Matrix &other); // Matrix multiplication
        void operator*=(Matrix &other); // In-place multiplication
        Matrix operator~(); // Inverse
        Vec4f operator*(Vec4f &vector); // Vector transform

    private:
        float data[16];
    };

}