#pragma once
#include <OpenMesh/Core/Geometry/VectorT.hh>
#include <math/matrix.h>

using namespace OpenMesh;

namespace mt {

    /**
     * The quaternion and its mathematical operations
     */
    struct Quaternion {
    public:
        /**
         * Create identity
         */
        Quaternion();

        /**
         * Create from axis rotation
         */
        Quaternion(Vec3f axis, float angle);

        /**
         * Create from rotation vec to vec
         */
        Quaternion(Vec3f v1, Vec3f v2);

        /**
         * Convert to matrix
         */
        void toMatrix(Matrix& out);

        /**
         * Sets to fixed values
         */
        void set(float x, float y, float z, float w);

        /**
         * Set to identity rotation
         */
        void setIdentity();

        /**
         * Normalizes the quaternion
         **/
        void normalize();

        // Multiplication
        Quaternion operator*(Quaternion other);
        void operator*=(Quaternion other);

    private:


        // State components
        float X, Y, Z, W;

    };
}