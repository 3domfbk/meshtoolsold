#include <math/quaternion.h>
#include <math.h>
#include <utils/types.h>

using namespace mt;

Quaternion::Quaternion() {
    setIdentity();
}

Quaternion::Quaternion(Vec3f axis, float angle) {
    const float fHalfAngle = 0.5f * angle;
    const float fSin = sinf(fHalfAngle);
    W = cosf(fHalfAngle);
    X = fSin * axis[0];
    Y = fSin * axis[1];
    Z = fSin * axis[2];
}

// Based on https://svn.code.sf.net/p/irrlicht/code/trunk/include/quaternion.h

Quaternion::Quaternion(Vec3f from, Vec3f to) : Quaternion() {
    Vec3f v0 = from;
    Vec3f v1 = to;
    v0.normalize();
    v1.normalize();

    const float d = v0 | v1;
    if (d >= 1.0f) // If dot == 1, vectors are the same
    {
        return;
    } else if (d <= -1.0f) // exactly opposite
    {
        Vec3f axis(1.0f, 0.f, 0.f);
        axis = axis % v0;
        if (axis.length() == 0) {
            axis = Vec3f(0.f, 1.f, 0.f);
            axis = axis % v0;
        }
        // same as fromAngleAxis(core::PI, axis).normalize();
        set(axis[0], axis[1], axis[2], 0);
        normalize();
    }

    const float s = sqrtf((1 + d) * 2); // optimize inv_sqrt
    const float invs = 1.f / s;
    const Vec3f c = (v0 % v1) * invs;

    set(c[0], c[1], c[2], s * 0.5f);
    normalize();
}

void Quaternion::set(float x, float y, float z, float w) {
    this->X = x;
    this->Y = y;
    this->Z = z;
    this->W = w;
}

void Quaternion::setIdentity() {
    set(0, 0, 0, 1);
}

void Quaternion::normalize() {
    const float n = X * X + Y * Y + Z * Z + W*W;

    if (n == 1) return;

    float rn = 1.0f / sqrtf(n);
    this->X *= rn;
    this->Y *= rn;
    this->Z *= rn;
    this->W *= rn;
}

void Quaternion::toMatrix(Matrix& dest) {
    dest[0] = 1.0f - 2.0f * Y * Y - 2.0f * Z*Z;
    dest[1] = 2.0f * X * Y + 2.0f * Z*W;
    dest[2] = 2.0f * X * Z - 2.0f * Y*W;
    dest[3] = 0.0f;

    dest[4] = 2.0f * X * Y - 2.0f * Z*W;
    dest[5] = 1.0f - 2.0f * X * X - 2.0f * Z*Z;
    dest[6] = 2.0f * Z * Y + 2.0f * X*W;
    dest[7] = 0.0f;

    dest[8] = 2.0f * X * Z + 2.0f * Y*W;
    dest[9] = 2.0f * Z * Y - 2.0f * X*W;
    dest[10] = 1.0f - 2.0f * X * X - 2.0f * Y*Y;
    dest[11] = 0.0f;

    dest[12] = 0;
    dest[13] = 0;
    dest[14] = 0;
    dest[15] = 1.f;
}

Quaternion Quaternion::operator*(Quaternion other) {
    Quaternion tmp;

    tmp.W = (other.W * W) - (other.X * X) - (other.Y * Y) - (other.Z * Z);
    tmp.X = (other.W * X) + (other.X * W) + (other.Y * Z) - (other.Z * Y);
    tmp.Y = (other.W * Y) + (other.Y * W) + (other.Z * X) - (other.X * Z);
    tmp.Z = (other.W * Z) + (other.Z * W) + (other.X * Y) - (other.Y * X);

    return tmp;
}

void Quaternion::operator*=(Quaternion other) {
    *this = *this * other;
}