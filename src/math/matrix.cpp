#include <utils/types.h>
#include <math/matrix.h>
#include <string.h>
#include <math.h>

using namespace OpenMesh;
using namespace mt;

Matrix::Matrix() {
    setIdentity();
}

void Matrix::setIdentity() {
    memset(data, 0, sizeof (data));
    data[0] = 1;
    data[5] = 1;
    data[10] = 1;
    data[15] = 1;
}

void Matrix::setPerspective(float fovDegrees, float aspectRatio, float znear, float zfar) {
    float ymax, xmax;
    ymax = znear * tanf(fovDegrees * (float) M_PI / 360.0f);
    xmax = ymax * aspectRatio;
    setFrustum(-xmax, xmax, -ymax, ymax, znear, zfar);
}

void Matrix::setFrustum(float left, float right, float bottom, float top, float znear, float zfar) {
    float temp, temp2, temp3, temp4;
    temp = 2.0f * znear;
    temp2 = right - left;
    temp3 = top - bottom;
    temp4 = zfar - znear;
    setIdentity();
    data[0] = temp / temp2;
    data[5] = temp / temp3;
    data[8] = (right + left) / temp2;
    data[9] = (top + bottom) / temp3;
    data[10] = (-zfar - znear) / temp4;
    data[11] = -1.0f;
    data[14] = (-temp * zfar) / temp4;
    data[15] = 0;
}

void Matrix::setOrtho(float left, float right, float bottom, float top, float znear, float zfar) {
    setIdentity();
    data[0] = 2.0f / (right - left);
    data[5] = 2.0f / (top - bottom);
    data[10] = -2.0f / (zfar - znear);
    data[12] = -(right + left) / (right - left);
    data[13] = -(top + bottom) / (top - bottom);
    data[14] = -(zfar + znear) / (zfar - znear);
}

void Matrix::setLookAt(float eyex, float eyey, float eyez, float centerx, float centery, float centerz, float upx, float upy, float upz) {
    Vec3f forward(centerx - eyex, centery - eyey, centerz - eyez);
    Vec3f up(upx, upy, upz);

    forward.normalize();

    /* Side = forward x up */
    Vec3f side = forward % up;
    side.normalize();

    /* Recompute up as: up = side x forward */
    up = side % forward;

    setIdentity();
    data[0] = side[0];
    data[1] = side[1];
    data[2] = side[2];
    data[4] = up[0];
    data[5] = up[1];
    data[6] = up[2];
    data[8] = -forward[0];
    data[9] = -forward[1];
    data[10] = -forward[2];
    data[12] = -eyex;
    data[13] = -eyey;
    data[14] = -eyez;
}

void Matrix::translate(float x, float y, float z) {
    data[12] += data[0] * x + data[4] * y + data[8] * z;
    data[13] += data[1] * x + data[5] * y + data[9] * z;
    data[14] += data[2] * x + data[6] * y + data[10] * z;
    data[15] += data[3] * x + data[7] * y + data[11] * z;
}

void Matrix::scale(float x, float y, float z) {
    data[0] *= x;
    data[1] *= x;
    data[2] *= x;
    data[3] *= x;
    data[4] *= y;
    data[5] *= y;
    data[6] *= y;
    data[7] *= y;
    data[8] *= z;
    data[9] *= z;
    data[10] *= z;
    data[11] *= z;
}

void Matrix::multiply(Matrix &other) {
    float* M = data;
    float m1[16];
    memcpy(m1, data, sizeof (m1));
    float* m2 = other.data;

    M[0] = m1[0] * m2[0] + m1[4] * m2[1] + m1[8] * m2[2] + m1[12] * m2[3];
    M[1] = m1[1] * m2[0] + m1[5] * m2[1] + m1[9] * m2[2] + m1[13] * m2[3];
    M[2] = m1[2] * m2[0] + m1[6] * m2[1] + m1[10] * m2[2] + m1[14] * m2[3];
    M[3] = m1[3] * m2[0] + m1[7] * m2[1] + m1[11] * m2[2] + m1[15] * m2[3];

    M[4] = m1[0] * m2[4] + m1[4] * m2[5] + m1[8] * m2[6] + m1[12] * m2[7];
    M[5] = m1[1] * m2[4] + m1[5] * m2[5] + m1[9] * m2[6] + m1[13] * m2[7];
    M[6] = m1[2] * m2[4] + m1[6] * m2[5] + m1[10] * m2[6] + m1[14] * m2[7];
    M[7] = m1[3] * m2[4] + m1[7] * m2[5] + m1[11] * m2[6] + m1[15] * m2[7];

    M[8] = m1[0] * m2[8] + m1[4] * m2[9] + m1[8] * m2[10] + m1[12] * m2[11];
    M[9] = m1[1] * m2[8] + m1[5] * m2[9] + m1[9] * m2[10] + m1[13] * m2[11];
    M[10] = m1[2] * m2[8] + m1[6] * m2[9] + m1[10] * m2[10] + m1[14] * m2[11];
    M[11] = m1[3] * m2[8] + m1[7] * m2[9] + m1[11] * m2[10] + m1[15] * m2[11];

    M[12] = m1[0] * m2[12] + m1[4] * m2[13] + m1[8] * m2[14] + m1[12] * m2[15];
    M[13] = m1[1] * m2[12] + m1[5] * m2[13] + m1[9] * m2[14] + m1[13] * m2[15];
    M[14] = m1[2] * m2[12] + m1[6] * m2[13] + m1[10] * m2[14] + m1[14] * m2[15];
    M[15] = m1[3] * m2[12] + m1[7] * m2[13] + m1[11] * m2[14] + m1[15] * m2[15];
}

float* Matrix::getData() {
    return data;
}

Vec3f Matrix::getTranslation() {
    return Vec3f(data[12], data[13], data[14]);
}

float& Matrix::operator[](int id) {
    return data[id];
}

void Matrix::operator*=(Matrix &other) {
    multiply(other);
}

Matrix Matrix::operator*(Matrix &other) {
    Matrix result = *this;
    result.multiply(other);
    return result;
}

Vec4f Matrix::operator*(Vec4f &vector) {
    Vec4f res;
    res[0] = data[0] * vector[0] + data[4] * vector[1] + data[8] * vector[2] + data[12] * vector[3];
    res[1] = data[1] * vector[1] + data[5] * vector[1] + data[9] * vector[2] + data[13] * vector[3];
    res[2] = data[2] * vector[0] + data[6] * vector[1] + data[10] * vector[2] + data[14] * vector[3];
    res[3] = data[3] * vector[0] + data[7] * vector[1] + data[11] * vector[2] + data[15] * vector[3];
    return res;
}

Matrix Matrix::operator~() {
    Matrix res;
    float det;
    int i;

    res.data[0] = data[5] * data[10] * data[15] -
            data[5] * data[11] * data[14] -
            data[9] * data[6] * data[15] +
            data[9] * data[7] * data[14] +
            data[13] * data[6] * data[11] -
            data[13] * data[7] * data[10];

    res.data[4] = -data[4] * data[10] * data[15] +
            data[4] * data[11] * data[14] +
            data[8] * data[6] * data[15] -
            data[8] * data[7] * data[14] -
            data[12] * data[6] * data[11] +
            data[12] * data[7] * data[10];

    res.data[8] = data[4] * data[9] * data[15] -
            data[4] * data[11] * data[13] -
            data[8] * data[5] * data[15] +
            data[8] * data[7] * data[13] +
            data[12] * data[5] * data[11] -
            data[12] * data[7] * data[9];

    res.data[12] = -data[4] * data[9] * data[14] +
            data[4] * data[10] * data[13] +
            data[8] * data[5] * data[14] -
            data[8] * data[6] * data[13] -
            data[12] * data[5] * data[10] +
            data[12] * data[6] * data[9];

    res.data[1] = -data[1] * data[10] * data[15] +
            data[1] * data[11] * data[14] +
            data[9] * data[2] * data[15] -
            data[9] * data[3] * data[14] -
            data[13] * data[2] * data[11] +
            data[13] * data[3] * data[10];

    res.data[5] = data[0] * data[10] * data[15] -
            data[0] * data[11] * data[14] -
            data[8] * data[2] * data[15] +
            data[8] * data[3] * data[14] +
            data[12] * data[2] * data[11] -
            data[12] * data[3] * data[10];

    res.data[9] = -data[0] * data[9] * data[15] +
            data[0] * data[11] * data[13] +
            data[8] * data[1] * data[15] -
            data[8] * data[3] * data[13] -
            data[12] * data[1] * data[11] +
            data[12] * data[3] * data[9];

    res.data[13] = data[0] * data[9] * data[14] -
            data[0] * data[10] * data[13] -
            data[8] * data[1] * data[14] +
            data[8] * data[2] * data[13] +
            data[12] * data[1] * data[10] -
            data[12] * data[2] * data[9];

    res.data[2] = data[1] * data[6] * data[15] -
            data[1] * data[7] * data[14] -
            data[5] * data[2] * data[15] +
            data[5] * data[3] * data[14] +
            data[13] * data[2] * data[7] -
            data[13] * data[3] * data[6];

    res.data[6] = -data[0] * data[6] * data[15] +
            data[0] * data[7] * data[14] +
            data[4] * data[2] * data[15] -
            data[4] * data[3] * data[14] -
            data[12] * data[2] * data[7] +
            data[12] * data[3] * data[6];

    res.data[10] = data[0] * data[5] * data[15] -
            data[0] * data[7] * data[13] -
            data[4] * data[1] * data[15] +
            data[4] * data[3] * data[13] +
            data[12] * data[1] * data[7] -
            data[12] * data[3] * data[5];

    res.data[14] = -data[0] * data[5] * data[14] +
            data[0] * data[6] * data[13] +
            data[4] * data[1] * data[14] -
            data[4] * data[2] * data[13] -
            data[12] * data[1] * data[6] +
            data[12] * data[2] * data[5];

    res.data[3] = -data[1] * data[6] * data[11] +
            data[1] * data[7] * data[10] +
            data[5] * data[2] * data[11] -
            data[5] * data[3] * data[10] -
            data[9] * data[2] * data[7] +
            data[9] * data[3] * data[6];

    res.data[7] = data[0] * data[6] * data[11] -
            data[0] * data[7] * data[10] -
            data[4] * data[2] * data[11] +
            data[4] * data[3] * data[10] +
            data[8] * data[2] * data[7] -
            data[8] * data[3] * data[6];

    res.data[11] = -data[0] * data[5] * data[11] +
            data[0] * data[7] * data[9] +
            data[4] * data[1] * data[11] -
            data[4] * data[3] * data[9] -
            data[8] * data[1] * data[7] +
            data[8] * data[3] * data[5];

    res.data[15] = data[0] * data[5] * data[10] -
            data[0] * data[6] * data[9] -
            data[4] * data[1] * data[10] +
            data[4] * data[2] * data[9] +
            data[8] * data[1] * data[6] -
            data[8] * data[2] * data[5];

    det = data[0] * res.data[0] + data[1] * res.data[4] + data[2] * res.data[8] + data[3] * res.data[12];
    det = 1.0f / det;

    for (i = 0; i < 16; i++)
        res.data[i] = res.data[i] * det;

    return res;
}
