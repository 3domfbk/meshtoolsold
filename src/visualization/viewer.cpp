#include <MeshtoolsConfig.h>
#include <visualization/viewer.h>
#include <datatypes/pointcloud.h>
#include <datatypes/mesh.h>
#include <datatypes/circle.h>
#include <datatypes/ray.h>
#include <utils/types.h>
#include <math.h>

#include <iostream>

using namespace mt;
using namespace std;

// Custom implementation of gluProject
int gluProject2(float objx, float objy, float objz,
  Matrix& modelview, Matrix& projection, int* viewport, float& wCx, float& wCy, float& wCz)
{
    float fTempo[8];
    //Modelview transform
    fTempo[0]=modelview[0]*objx+modelview[4]*objy+modelview[8]*objz+modelview[12];  //w is always 1
    fTempo[1]=modelview[1]*objx+modelview[5]*objy+modelview[9]*objz+modelview[13];
    fTempo[2]=modelview[2]*objx+modelview[6]*objy+modelview[10]*objz+modelview[14];
    fTempo[3]=modelview[3]*objx+modelview[7]*objy+modelview[11]*objz+modelview[15];
    //Projection transform, the final row of projection matrix is always [0 0 -1 0]
    //so we optimize for that.
    fTempo[4]=projection[0]*fTempo[0]+projection[4]*fTempo[1]+projection[8]*fTempo[2]+projection[12]*fTempo[3];
    fTempo[5]=projection[1]*fTempo[0]+projection[5]*fTempo[1]+projection[9]*fTempo[2]+projection[13]*fTempo[3];
    fTempo[6]=projection[2]*fTempo[0]+projection[6]*fTempo[1]+projection[10]*fTempo[2]+projection[14]*fTempo[3];
    fTempo[7]=-fTempo[2];
    //The result normalizes between -1 and 1
    if(fTempo[7]==0.0)    //The w value
       return 0;
    fTempo[7]=1.0/fTempo[7];
    //Perspective division
    fTempo[4]*=fTempo[7];
    fTempo[5]*=fTempo[7];
    fTempo[6]*=fTempo[7];
    //Window coordinates
    //Map x, y to range 0-1
    wCx=(fTempo[4]*0.5+0.5)*viewport[2]+viewport[0];
    wCy=(fTempo[5]*0.5+0.5)*viewport[3]+viewport[1];
    //This is only correct when glDepthRange(0.0, 1.0)
    wCz=(1.0+fTempo[6])*0.5;	//Between 0 and 1
    return 1;
}

// Standard Shader
static const char* vertex_shader_text =
    "#ifdef GL_ES\nprecision mediump float;\n#endif\n"
    "uniform mat4 MVP;\n"
    "attribute vec4 vCol;\n"
    "attribute vec3 vPos;\n"
    "varying vec4 color;\n"
    "void main()\n"
    "{\n"
#ifdef BUILD_ANDROID
    "    gl_PointSize = 2.0;"
#else
    "    gl_PointSize = 1.0;"
#endif
    "    gl_Position = MVP * vec4(vPos, 1.0);\n"
    "    color = vCol;\n"
    "}\n";

static const char* fragment_shader_text =
    "#ifdef GL_ES\nprecision mediump float;\n#endif\n"
    "varying vec4 color;\n"
    "void main()\n"
    "{\n"
    "    gl_FragColor = color;\n"
    "}\n";

Viewer::Viewer() {
    this->scale = 3;
    this->znear = -1;
    this->distance = -1;
    this->dragging = false;
    this->program = -1;
    this->camx = 0;
    this->camy = 0;
    this->camz = 0;
    this->panning = false;
    this->panDx = 0;
    this->panDy = 0;
    this->selOx = -1;
	this->fov = 45;
	this->sceneBBox = BBox();

    CREATE_MUTEX(mutex);
}

void Viewer::dispose() {
    auto it = objects.cbegin();
    for (; it != objects.cend(); ++it) {
      delete it->second;
    }
    RELEASE_MUTEX(mutex);
}

void Viewer::addPointCloud(string name, PointCloud* pointCloud) {
    // Overwrite
    removeObject(name);
	PointCloudView* object = new PointCloudView(this, pointCloud);
	objects[name] = object;
	updateSceneBBox(object);
}

void Viewer::addFsOctree(string name, PointCloud* pointCloud, FsOctree* octree) {
    // Overwrite
    removeObject(name);
	FsOctreeView* object = new FsOctreeView(this, pointCloud, octree);
	objects[name] = object;
	updateSceneBBox(object);
}

void Viewer::addMesh(string name, Mesh* mesh) {
    // Overwrite
    removeObject(name);
	MeshView* object = new MeshView(this, mesh);
	objects[name] = object;
	updateSceneBBox(object);
}

void Viewer::addCircle(string name, Circle* circle) {
	// Overwrite
	removeObject(name);
	CircleView* object = new CircleView(this, circle);
	objects[name] = object;
	updateSceneBBox(object);
}

void Viewer::removeObject(string name) {
    if (objects.count(name) > 0) {
        toRelease.push_back(objects[name]);
        objects.erase(name);
    }
}

void Viewer::invalidateObject(string name) {
    if (objects.count(name) > 0) {
        objects[name]->invalidate();
    }
}

void Viewer::setVisible(string name, bool visible) {
    if (objects.count(name) > 0) {
        objects[name]->setVisible(visible);
    }
}

void Viewer::focusToObject(string name) {
    if (objects.count(name) > 0) {
        camx = objects[name]->getBBox()->getCenterX();
        camy = objects[name]->getBBox()->getCenterY();
        camz = objects[name]->getBBox()->getCenterZ();

		center[0] = objects[name]->getBBox()->getCenterX();
		center[1] = objects[name]->getBBox()->getCenterY();
		center[2] = objects[name]->getBBox()->getCenterZ();
    }
}

void Viewer::centerOnObject(string name) {
	if (objects.count(name) > 0) {
		center[0] = objects[name]->getBBox()->getCenterX();
		center[1] = objects[name]->getBBox()->getCenterY();
		center[2] = objects[name]->getBBox()->getCenterZ();
	}
}

void Viewer::deleteSelection(string name) {
    objects[name]->onDeleteSelection();
}

void Viewer::setOctreeLevel(string name, int level){

    if (objects.count(name) > 0) {
        objects[name]->setOctreeLevel(level);
    }
}
void Viewer::lock() {
    LOCK(mutex);
}

void Viewer::unlock() {
    UNLOCK(mutex);
}

int Viewer::getWidth(){
    return w;
}

int Viewer::getHeight(){
    return h;
}

void Viewer::initialize(int width, int height) {
    this->w = width;
    this->h = height;

    // Setup GL
    glViewport(0, 0, w, h);
    glEnable(GL_DEPTH_TEST);

    // Initialize GLEW
    #ifndef BUILD_ANDROID
    glEnable(GL_VERTEX_PROGRAM_POINT_SIZE);

    GLenum err = glewInit();
    if (err != GLEW_OK) {
        cout << "Cannot initialize GLEW:" << err << endl;
        exit(EXIT_FAILURE);
    }
    #endif
}

void Viewer::onResize(int width, int height) {
    this->w = width;
    this->h = height;
    this->znear = -1;

    // Setup GL
    glViewport(0, 0, w, h);
}

void Viewer::renderFrame() {
    glClearColor(0.25f, 0.25f, 0.25f, 1);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Initialize if necessary
    if (program == -1) {
        GLuint vshader = compileShader(vertex_shader_text, GL_VERTEX_SHADER);
        GLuint fshader = compileShader(fragment_shader_text, GL_FRAGMENT_SHADER);

        program = glCreateProgram();
        glAttachShader(program, vshader);
        glAttachShader(program, fshader);
        glLinkProgram(program);

        mvp_location = glGetUniformLocation(program, "MVP");
        vpos_location = glGetAttribLocation(program, "vPos");
        vcol_location = glGetAttribLocation(program, "vCol");

        // Set wireframe mode (cool)
        #ifndef BUILD_ANDROID
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        #endif
    }

    // Setup shader
    glUseProgram(program);
	
	//if (znear == -1) {
		// Re-calculate perspective considering the bbox
	znear = sceneBBox.extent() < 1 ? 0.1f : 0.5f;
	zfar = sceneBBox.extent() * 8.0f;
	//}

	projection.setPerspective(fov, (float)w / h, znear, zfar);

    // Render objects
    auto it = objects.cbegin();
    for (; it != objects.cend(); ++it) {
        if (it->second->visible) {
            // Setup distance
            if( distance == -1 ){
                scale = 3;
                distance = it->second->getBBox()->extent() * 3;
                center[0] = it->second->getBBox()->getCenterX();
                center[1] = it->second->getBBox()->getCenterY();
                center[2] = it->second->getBBox()->getCenterZ();
            }            			

            // MVP
            updateMvp(camx, camy, camz);
            glUniformMatrix4fv(mvp_location, 1, GL_FALSE, mvp.getData());

            it->second->update();
            it->second->onRender();

			// TODO
			//renderString(it->first);

            // Render bbox
            //renderBbox(it->second->getBBox(), 0, 1, 0);			
        }
    }

    // Draw selection rect
    if (selOx != -1) {
        renderRect(selOx, h - selOy, selCx, h - selCy, 1, 1, 1);
    }

    // Objects to release
    auto rit = toRelease.cbegin();
    for (; rit != toRelease.cend(); ++rit) {
        (*rit)->onRelease();
        delete (*rit);
    }
    toRelease.clear();

    int error = glGetError();
    if (error != 0) {
        printf("GLERROR: %d", error);
    }
}

GLuint Viewer::compileShader(const GLchar* source, GLenum type){
    GLuint shader = glCreateShader(type);
    glShaderSource(shader, 1, &source, NULL);
    glCompileShader(shader);
    GLint isCompiled = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &isCompiled);
    if(isCompiled == GL_FALSE)
    {
        GLint maxLength = 0;
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &maxLength);

        // The maxLength includes the NULL character
        GLchar *errorLog = new GLchar[maxLength];
        glGetShaderInfoLog(shader, maxLength, &maxLength, errorLog);

        LOGE("Shader compile error: %s", string(errorLog).c_str());

        // Exit with failure.
        glDeleteShader(shader); // Don't leak the shader.

        delete errorLog;
        return -1;
    }
    return shader;
}

void Viewer::renderRect(int left, int top, int right, int bottom, float r, float g, float b) {
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);

    Matrix mvp;
    mvp.setOrtho((float) 0, (float) w, (float) 0, (float) h, (float) - 1, (float) 1);
    glUniformMatrix4fv(mvp_location, 1, GL_FALSE, mvp.getData());

    GLfloat vertices[] = {
        (float) left, (float) top, 0,
        (float) left, (float) bottom, 0,
        (float) right, (float) bottom, 0,
        (float) right, (float) top, 0
    };
    float a = 0.4f;
    GLfloat colors[] = {
        r, g, b, a,
        r, g, b, a,
        r, g, b, a,
        r, g, b, a
    };
    GLushort elements[] = {
        0, 1, 2,
        0, 2, 3
    };

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    #ifndef BUILD_ANDROID
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    #endif
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 3, GL_FLOAT, GL_FALSE, 0, (void*) vertices);
    glEnableVertexAttribArray(vcol_location);
    glVertexAttribPointer(vcol_location, 4, GL_FLOAT, GL_FALSE, 0, (void*) colors);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_SHORT, elements);

    glDisable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    #ifndef BUILD_ANDROID
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    #endif
}

void Viewer::renderCircle(float x, float y, float z, float radius, float r, float g, float b) {

	GLfloat vertices[360 * 3];
	GLfloat colors[360 * 3];
	GLushort elements[360];

	for (int i = 0, j = 0; i < 360; i++, j += 3) {

		float angle = i * 2.0*M_PI / 360.0;

		vertices[j] = x + radius * cos(angle);
		vertices[j+1] = y + radius * sin(angle);
		vertices[j+2] = z;

		colors[j] = r;
		colors[j + 1] = g;
		colors[j + 2] = b;
		
		elements[i] = i;
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(vpos_location);
	glVertexAttribPointer(vpos_location, 3, GL_FLOAT, GL_FALSE, 0, (void*)vertices);
	glEnableVertexAttribArray(vcol_location);
	glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE, 0, (void*)colors);

	glDrawElements(GL_LINE_LOOP, 360, GL_UNSIGNED_SHORT, elements);
}

void Viewer::renderBbox(BBox* bbox, float r, float g, float b) {
    GLfloat vertices[] = {
        bbox->bounds[0], bbox->bounds[2], bbox->bounds[4],
        bbox->bounds[1], bbox->bounds[2], bbox->bounds[4],
        bbox->bounds[1], bbox->bounds[3], bbox->bounds[4],
        bbox->bounds[0], bbox->bounds[3], bbox->bounds[4],
        bbox->bounds[0], bbox->bounds[2], bbox->bounds[5],
        bbox->bounds[1], bbox->bounds[2], bbox->bounds[5],
        bbox->bounds[1], bbox->bounds[3], bbox->bounds[5],
        bbox->bounds[0], bbox->bounds[3], bbox->bounds[5],
    };
    GLfloat colors[] = {
        r, g, b,
        r, g, b,
        r, g, b,
        r, g, b,
        r, g, b,
        r, g, b,
        r, g, b,
        r, g, b
    };

    GLushort elements[] = {
        0, 1, 2, 3,
        4, 5, 6, 7,
        0, 4, 1, 5, 2, 6, 3, 7
    };

    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glEnableVertexAttribArray(vpos_location);
    glVertexAttribPointer(vpos_location, 3, GL_FLOAT, GL_FALSE, 0, (void*) vertices);
    glEnableVertexAttribArray(vcol_location);
    glVertexAttribPointer(vcol_location, 3, GL_FLOAT, GL_FALSE, 0, (void*) colors);
	
    glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_SHORT, elements);
    glDrawElements(GL_LINE_LOOP, 4, GL_UNSIGNED_SHORT, elements + 4);
    glDrawElements(GL_LINES, 8, GL_UNSIGNED_SHORT, elements + 8);
}

bool Viewer::pointVisible(float oX, float oY, float oZ, int vx, int vy, int vw, int vh) {
    float wX, wY, wZ;
    GLint vp[] = {0, 0, w, h};

    gluProject2(oX, oY, oZ, view, projection, vp, wX, wY, wZ);
    wY = h - wY;

    return wX >= vx && wX < vx + vw && wY >= vy && wY < vy + vh && wZ > 0 && wZ < 1;
}

int Viewer::boxVisible(BBox &box, int vx, int vy, int vw, int vh) {
    GLint vp[] = {0, 0, w, h};
    float wX[8], wY[8], wZ[8];

    for (int i = 0; i < 8; i++) {
        double x = box.bounds[i >> 2];
        double y = box.bounds[2 + ((i >> 1) & 0x1)];
        double z = box.bounds[4 + (i & 0x1)];

        gluProject2(x, y, z, view, projection, vp, wX[i], wY[i], wZ[i]);
        wY[i] = h - wY[i];
    }

    // All contained
    bool allContained = true;
    for (int i = 0; i < 8; i++) {
        if (wX[i] < vx || wX[i] >= vx + vw || wY[i] < vy || wY[i] >= vy + vh || wZ[i] <= 0 || wZ[i] >= 1) {
            allContained = false;
            break;
        }
    }
    if (allContained) return 2;

    // All side
    bool allLeft = true;
    bool allRight = true;
    bool allTop = true;
    bool allBottom = true;
    bool allFar = true;
    bool allNear = true;
    for (int i = 0; i < 8; i++) {
        if (allLeft && wX[i] >= vx) allLeft = false;
        if (allRight && wX[i] < vx + vw) allRight = false;
        if (allTop && wY[i] >= vy) allTop = false;
        if (allBottom && wY[i] < vy + vh) allBottom = false;
        if (allFar && wZ[i] > 0) allFar = false;
        if (allNear && wZ[i] < 1) allNear = false;
    }

    // Not intersecting
    if (allLeft || allRight || allTop || allBottom || allFar || allNear ) return 0;

    // Partially contained
    return 1;
}

int Viewer::getVPosLocation() {
    return vpos_location;
}

int Viewer::getVColLocation() {
    return vcol_location;
}

void Viewer::clearSelection(string name) {

    if (objects.count(name) > 0) {
        objects[name]->onClearSelection();
    }
}

void Viewer::updateMvp(float cx, float cy, float cz) {
	
    // Object rotation
    Quaternion totalRotation = cloudRotation;
    if (dragging) {
        totalRotation = totalRotation * dragRotation;
    }
    totalRotation.toMatrix(cloudTransform);
	view.setLookAt(cx + panDx /scale, cy + panDy / scale, cz + distance, cx + panDx / scale, cy + panDy / scale, cz, 0, 1, 0);
    view.translate(center[0], center[1], center[2]); // De-Center the cloud!
    view *= cloudTransform; // Rotate
    view.translate(-center[0], -center[1], -center[2]); // Center the cloud!

    // Calculate total
    mvp = projection * view;
}

void Viewer::setScale(float scale) {
    LOCK(mutex);
    this->scale = scale;
    UNLOCK(mutex);
}

void Viewer::onMouseMoved(double x, double y) {
    LOCK(mutex);
    if (dragging) {
        Vec3f sphereDragPoint = sphereProject((float) x, (float) y);
        dragRotation = Quaternion(sphereClickPoint, sphereDragPoint);
    }
    if (panning) {
        panDx = panClickX - (float) x;
        panDy = (float) y - panClickY;
    }
    if (selOx != -1) {
        selCx = (int) x;
        selCy = (int) y;
    }
    UNLOCK(mutex);
}

void Viewer::onMousePressed(ViewerMouseBtn button, double x, double y) {
    LOCK(mutex);
    switch (button) {
        case VW_MIDDLE_BTN:
        {
            selOx = (int) x;
            selOy = (int) y;
            selCx = (int) x;
            selCy = (int) y;
        }
            break;

        case VW_RIGHT_BTN:
            panning = true;
            panClickX = (float) x;
            panClickY = (float) y;
            panDx = 0;
            panDy = 0;
            break;

        case VW_LEFT_BTN:
            dragging = true;
            // Calculate starting vector on drag sphere
            sphereClickPoint = sphereProject((float) x, (float) y);
            break;
    }
    UNLOCK(mutex);
}

void Viewer::onMouseReleased(ViewerMouseBtn button, double x, double y) {
    LOCK(mutex);
    switch (button) {
        case VW_RIGHT_BTN:
            panning = false;

            camx += panDx / scale;
            camy += panDy / scale;
            panDx = 0;
            panDy = 0;
            break;

        case VW_LEFT_BTN:
            dragging = false;

            // Update current rotation
            cloudRotation = cloudRotation * dragRotation;
            dragRotation.setIdentity();
            break;

        case VW_MIDDLE_BTN:
            for (auto it = objects.begin(); it != objects.end(); it++) {
                int t = MIN(selOy, (int) y);
                int b = MAX(selOy, (int) y);
                int l = MIN(selOx, (int) x);
                int r = MAX(selOx, (int) x);
                it->second->onPick(l, t, r - l, b - t);
            }

            selCx = (int) x;
            selCy = (int) y;
            selOx = -1;
            selOy = -1;
            break;
    }
    UNLOCK(mutex);
}

void Viewer::onMouseWheelScrolled(double x, double y) {
    for (int i = 0; i < abs((float) y); i++) {
		if ((float)y <= 0) {
			//setScale(scale*0.9f);
			if (fov < 150) {
				fov *= 1.1f;
				scale *= 0.9f;
			}
		}
		else {
			//setScale(scale*1.1f);			
			fov *= 0.9f;
			scale *= 1.1f;
		}
    }
}

Vec3f Viewer::sphereProject(float x, float y) {
    // Inspired by https://www.raywenderlich.com/12667/how-to-rotate-a-3d-object-using-touches-with-opengl

    float radius = w / 2.0f;
    Vec3f touch = Vec3f((float) x, (float) y, 0);
    Vec3f center = Vec3f(w / 2, h / 2, 0);
    Vec3f P = touch - center;
    P = Vec3f(P[0], -P[1], P[2]); // Flip y

    float radius2 = radius * radius;
    float length2 = P[0] * P[0] + P[1] * P[1];

    if (length2 <= radius2)
        P[2] = sqrt(radius2 - length2);
    else {
        P[0] *= radius / sqrt(length2);
        P[1] *= radius / sqrt(length2);
        P[2] = 0;
    }

    P.normalize();
    return P;
}

void Viewer::updateSceneBBox(View* object) {

	sceneBBox.bounds[0] = MIN(sceneBBox.bounds[0], object->getBBox()->bounds[0]);
	sceneBBox.bounds[1] = MAX(sceneBBox.bounds[1], object->getBBox()->bounds[1]);
	sceneBBox.bounds[2] = MIN(sceneBBox.bounds[2], object->getBBox()->bounds[2]);
	sceneBBox.bounds[3] = MAX(sceneBBox.bounds[3], object->getBBox()->bounds[3]);
	sceneBBox.bounds[4] = MIN(sceneBBox.bounds[4], object->getBBox()->bounds[4]);
	sceneBBox.bounds[5] = MAX(sceneBBox.bounds[5], object->getBBox()->bounds[5]);

	znear = sceneBBox.extent() < 1 ? 0.1f : 0.5f;
	zfar = sceneBBox.extent() * 8.0f;

	scale = 3;
	distance = sceneBBox.extent() * 3;
}
