#include <visualization/view.h>

using namespace mt;

View::View(Viewer* viewer) {
    this->viewer = viewer;
    dirty = true;
    visible = true;
}

void View::invalidate() {
    dirty = true;
}

void View::setVisible(bool visible) {
    this->visible = visible;
}

void View::update() {
    if (dirty) {
        dirty = false;
        onObjectReload();
    }
}