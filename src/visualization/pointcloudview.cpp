#include <iostream>
#include <visualization/pointcloudview.h>
#include <visualization/viewer.h>
#include <datatypes/octree.h>
#include <utils/types.h>

using namespace mt;
using namespace std;

PointCloudView::PointCloudView(Viewer* viewer, PointCloud* cloud) : View(viewer) {
    this->pointCloud = cloud;
    this->allVbo = -1;
    this->octreeLevel = 2;
}

void PointCloudView::onRelease() {
    GLuint vbos[] = {allVbo};
    glDeleteBuffers(1, vbos);
    allVbo = -1;
}

void PointCloudView::onObjectReload() {
    // Create point array
    Point* data = new Point[pointCloud->getNumPoints()];
    PointCell* it = pointCloud->getData()->begin();

    for (int i = 0; i<pointCloud->getNumPoints(); i++, it = it->next) {
        data[i] = it->data;
    }

    // Load vbo
    if (allVbo == -1) glGenBuffers(1, &allVbo);
    glBindBuffer(GL_ARRAY_BUFFER, allVbo);
    glBufferData(GL_ARRAY_BUFFER, pointCloud->getNumPoints() * sizeof (Point), data, GL_STATIC_DRAW);
    delete[] data;
}

void PointCloudView::onRender() {
    glBindBuffer(GL_ARRAY_BUFFER, allVbo);

    // Positions
    glEnableVertexAttribArray(viewer->getVPosLocation());
    glVertexAttribPointer(viewer->getVPosLocation(), 3, GL_FLOAT, GL_FALSE,
            sizeof (Point), (void*) 0);
    // VColors
    glEnableVertexAttribArray(viewer->getVColLocation());
    glVertexAttribPointer(viewer->getVColLocation(), 3, GL_UNSIGNED_BYTE, GL_TRUE,
            sizeof (Point), (void*) (sizeof (float) * 6)); // 6 = x,y,z,nx,ny,nz -> Skip normals for now

    // Render
    glDrawArrays(GL_POINTS, 0, (int) pointCloud->getNumPoints());

    // Render selection
    if (!selection.empty()) {
        if (selectionPoints.size() != selection.size()) {
            selectionPoints.clear();
            selectionColors.clear();
            for (int i = 0; i < selection.size(); i++) {
                selectionPoints.push_back(Point(
                        selection[i]->data.x,
                        selection[i]->data.y,
                        selection[i]->data.z,
                        selection[i]->data.rgb,
                        selection[i]->data.nx,
                        selection[i]->data.ny,
                        selection[i]->data.nz));
                selectionColors.push_back(BGR(255, 0, 0));
            }
        }

        glClear(GL_DEPTH_BUFFER_BIT);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glVertexAttribPointer(viewer->getVPosLocation(), 3, GL_FLOAT, GL_FALSE,
                sizeof (Point), &selectionPoints[0]);
        glVertexAttribPointer(viewer->getVColLocation(), 3, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (int), &selectionColors[0]);
        glDrawArrays(GL_POINTS, 0, (int) selection.size());
    }

    // Octree
    #ifndef BUILD_ANDROID
    if (pointCloud->getOctree() != NULL) {
        renderOctant(pointCloud->getOctree()->getRoot(), 1);
    }
    #endif
}

void PointCloudView::renderOctant(Octree::Octant *octant, int level) {
    // Not too deep, for gawd sake
    if (level > octreeLevel) return;

    for (int i = 0; i < 8; i++) {
        if (!octant->children[i]) continue;
        Vec3f c = octant->children[i]->center;
        float e = octant->children[i]->extent;

        BBox box;
        box.bounds[0] = c[0] - e;
        box.bounds[1] = c[0] + e;
        box.bounds[2] = c[1] - e;
        box.bounds[3] = c[1] + e;
        box.bounds[4] = c[2] - e;
        box.bounds[5] = c[2] + e;

        viewer->renderBbox(&box, 0, 0, 1.0f * powf(0.8f, (float) level));

        // Recursion
        renderOctant(octant->children[i], level + 1);
    }
}

void PointCloudView::onPick(Ray &ray) {
    selection.clear();

    if (pointCloud->getOctree() == NULL) {
        pointCloud->buildOctreeNP(1000);
    }

    // Ray-nearest
    float d;
    PointCell* n = pointCloud->getOctree()->nearest(ray, d);
    if (d < 0.5f) {
        selection.push_back(n);
    }
}

void PointCloudView::onPick(int vx, int vy, int vw, int vh) {
    selection.clear();

    if (pointCloud->getOctree() == NULL) {
        pointCloud->buildOctreeNP(1000);
    }

    // Recursively intersect octants
    selectVisiblePoints(pointCloud->getOctree()->getRoot(), vx, vy, vw, vh, &selection);
}

void PointCloudView::onDeleteSelection() {
    for (int i = 0; i < selection.size(); i++) {
        pointCloud->markForDeletion(selection[i]);
    }

    selection.clear();

    // Update
    pointCloud->update();
}

void PointCloudView::onClearSelection() {
    selection.clear();
}

void PointCloudView::selectVisiblePoints(Octree::Octant* octant,
        int vx, int vy, int vw, int vh, vector<PointCell*>* out) {

    Vec3f c = octant->center;
    float e = octant->extent;

    BBox box;
    box.bounds[0] = c[0] - e;
    box.bounds[1] = c[0] + e;
    box.bounds[2] = c[1] - e;
    box.bounds[3] = c[1] + e;
    box.bounds[4] = c[2] - e;
    box.bounds[5] = c[2] + e;

    int visibility = viewer->boxVisible(box, vx, vy, vw, vh);
    switch (visibility) {
            // Not visible
        case 0:
            break;

            // Partially visible
        case 1:
            // Leaf
            if (octant->leaf) {
                PointCell* p = octant->start;
                for (int i = 0; i < octant->numPoints; i++) {
                    float x = p->data.x;
                    float y = p->data.y;
                    float z = p->data.z;
                    if (viewer->pointVisible(x, y, z, vx, vy, vw, vh)) {
                        out->push_back(p);
                    }

                    p = p->next;
                }
                return;
            } else {
                for (int i = 0; i < 8; i++) {
                    // Recursion
                    if (octant->children[i]) {
                        selectVisiblePoints(octant->children[i], vx, vy, vw, vh, out);
                    }
                }
            }
            break;

            // Fully visible
        case 2:
            PointCell* p = octant->start;
            for (int j = 0; j < octant->numPoints; j++) {
                out->push_back(p);
                p = p->next;
            }
            break;
    }
}

void PointCloudView::setOctreeLevel(int level){
    octreeLevel = level;
}

BBox* PointCloudView::getBBox() {
    return pointCloud->getBBox();
}
