#pragma once

#include <datatypes/bbox.h>
#include <datatypes/ray.h>
#include <math/matrix.h>

namespace mt {

    class Viewer;

    /**
     * An object that can be displayed by the viewer
     */
    class View {
        friend class Viewer;

    protected:
        Viewer* viewer;
        
        /**
         * Creates a new mesh view for a mesh
         */
        View(Viewer* viewer);

    public:
        /* Tells the object that it has to be reloaded */
        void invalidate();
        /* Sets visibility */
        void setVisible(bool visible);                

        /* Checks if the view is invalid and reloads the object if it is */
        void update();

        // Release all data
        // This method gets called on GL thread
        virtual void onRelease() = 0;

        // Render
        // This method gets called on GL thread
        virtual void onRender() = 0;

        // Reload everything that is needed to view the underlying object (as it became invalid)
        // This method gets called on GL thread
        virtual void onObjectReload() = 0;

        // Pick element of this view with a picking ray
        virtual void onPick(Ray &ray) = 0;
        // Pick element of this view with a picking viewport
        virtual void onPick(int vx, int vy, int vw, int vh) = 0;
        // Deletes current selection
        virtual void onDeleteSelection() = 0;
        // Clears current selection 
        virtual void onClearSelection() = 0;        
        // Displays the octree with the specified level (0 hides the octree)
        virtual void setOctreeLevel(int level) = 0;

        // Obtains the visual bbox of the object
        virtual BBox* getBBox() = 0;

    private:
        bool dirty;
        bool visible;
    };
}
