#include <iostream>
#include <visualization/circleview.h>
#include <visualization/viewer.h>
#include <datatypes/octree.h>
#include <utils/types.h>

using namespace mt;
using namespace std;

CircleView::CircleView(Viewer* viewer, Circle* circle) : View(viewer) {
    this->circle = circle;
    this->posVbo = -1;
	this->colVbo = -1;
}

void CircleView::onRelease() {
	GLuint vbos[] = { posVbo, colVbo };
	glDeleteBuffers(2, vbos);

	this->posVbo = -1;
	this->colVbo = -1;
}

void CircleView::onObjectReload() {

}

void CircleView::onRender() {   	

	glLineWidth(4.0f);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(viewer->getVPosLocation());
	glVertexAttribPointer(viewer->getVPosLocation(), 3, GL_FLOAT, GL_FALSE, 0, (void*)circle->vertices);
	glEnableVertexAttribArray(viewer->getVColLocation());
	glVertexAttribPointer(viewer->getVColLocation(), 3, GL_FLOAT, GL_FALSE, 0, (void*)circle->colors);

	glDrawElements(GL_LINE_LOOP, 360, GL_UNSIGNED_SHORT, circle->elements);
	glLineWidth(1.0f);
}

void CircleView::onPick(Ray &ray) {
 
}

void CircleView::onPick(int vx, int vy, int vw, int vh) {
    
}

void CircleView::onDeleteSelection() {
   
}

void CircleView::onClearSelection() {

}

void CircleView::setOctreeLevel(int level){

}

BBox* CircleView::getBBox() {
    return circle->getBBox();
}
