/*
 * File:   fsoctreeview.cpp
 * Author: lake
 *
 * Created on January 3, 2017, 4:24 PM
 */

#include <MeshtoolsConfig.h>
#include <visualization/viewer.h>
#include <visualization/fsoctreeview.h>
#include <queue>

#include "io/pointcloudreader.h"

using namespace mt;

FsOctreeView::FsOctreeView(Viewer* viewer, PointCloud* cloud, FsOctree* octree) : View(viewer) {
    this->pointCloud = cloud;
    this->octree = octree;
}

void FsOctreeView::onRelease() {
    for( auto it=nodes.begin(); it!=nodes.end(); it++){
        GLuint vbos[] = {it->second->allVbo};
        glDeleteBuffers(1, vbos);
    }
}

void FsOctreeView::onObjectReload() {
    // TODO: Unload all nodes and reload? maybe
    updateNodes();
}

void FsOctreeView::updateNodes() {
    // Unload all stupid shit and jerk off
    unloadBranch(octree->getRoot());

    int pointBudget = 400000;

    // Queue
    queue<FsOctree::Octant*> q;
    q.push(octree->getRoot());

    while(!q.empty() && pointBudget>0){
        FsOctree::Octant* o = q.front();
        q.pop();
        if( isVisible(o) ){
            if( nodes.count(o->filename) == 0 ){
                NodeView* view = loadNode(o);
                nodes[o->filename] = view;
                pointBudget -= view->numpoints;
            }

            for( int i=0; i<8; i++ ){
                if( o->children[i] != NULL ){
                    q.push(o->children[i]);
                }
            }
        }
    }
}

void FsOctreeView::unloadBranch(FsOctree::Octant* o){
    // Unload
    if( nodes.count(o->filename) != 0 ){
        NodeView* view = nodes[o->filename];
        unloadNode(view);
        delete view;
        nodes.erase(o->filename);

        for( int i=0; i<8; i++ ){
            if( o->children[i] != NULL ){
                unloadBranch(o->children[i]);
            }
        }
    }
}

bool FsOctreeView::isVisible(FsOctree::Octant* o){
    Vec3f c = o->center;
    float e = o->extent;

    BBox box;
    box.bounds[0] = c[0] - e;
    box.bounds[1] = c[0] + e;
    box.bounds[2] = c[1] - e;
    box.bounds[3] = c[1] + e;
    box.bounds[4] = c[2] - e;
    box.bounds[5] = c[2] + e;

    return viewer->boxVisible(box, 0, 0, viewer->getWidth(), viewer->getHeight());
}

FsOctreeView::NodeView* FsOctreeView::loadNode(FsOctree::Octant* octant){
    // Read node pc
    PointCloudReader* reader = PointCloudReader::createReader(octant->filename, 400000);
    PointCloud* pc = reader->readPointCloud();
    delete reader;

    // Create point array
    Point* data = new Point[pc->getNumPoints()];
    PointCell* it = pc->getData()->begin();
    for (int i = 0; it != NULL; i++, it = it->next) {
        data[i] = it->data;
    }

    NodeView* view = new NodeView();
    view->octant = octant;
    view->numpoints = pc->getNumPoints();
    glGenBuffers(1, &view->allVbo);
    glBindBuffer(GL_ARRAY_BUFFER, view->allVbo);
    glBufferData(GL_ARRAY_BUFFER, pc->getNumPoints() * sizeof (Point), data, GL_STATIC_DRAW);
    delete[] data;
    return view;
}

void FsOctreeView::unloadNode(NodeView* node){
    uint bfs[] = {node->allVbo};
    glDeleteBuffers(1, bfs);
}


void FsOctreeView::onRender() {
    for( auto it=nodes.begin(); it!=nodes.end(); it++){
        if( it->second->octant == octree->getRoot() ) continue;

        glBindBuffer(GL_ARRAY_BUFFER, it->second->allVbo);

        // Positions
        glEnableVertexAttribArray(viewer->getVPosLocation());
        glVertexAttribPointer(viewer->getVPosLocation(), 3, GL_FLOAT, GL_FALSE,
                sizeof (Point), (void*) 0);
        // VColors
        glEnableVertexAttribArray(viewer->getVColLocation());
        glVertexAttribPointer(viewer->getVColLocation(), 3, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (Point), (void*) (sizeof (float) * 3));

        // Render
        glDrawArrays(GL_POINTS, 0, (int) it->second->numpoints);
    }
}

void FsOctreeView::onPick(Ray &ray) {

}

void FsOctreeView::onPick(int vx, int vy, int vw, int vh) {
    updateNodes();
}

void FsOctreeView::onDeleteSelection() {
}

void FsOctreeView::onClearSelection() {
}

void FsOctreeView::setOctreeLevel(int level) {
}

BBox* FsOctreeView::getBBox() {
    return pointCloud->getBBox();
}
