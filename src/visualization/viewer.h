#pragma once

#ifdef BUILD_ANDROID
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#else
#include <GL/glew.h>
#endif

#include <utils/threadutils.h>
#include <math/quaternion.h>
#include <visualization/meshview.h>
#include <visualization/pointcloudview.h>
#include <visualization/circleview.h>
#include <utils/types.h>
#include <visualization/fsoctreeview.h>

#include <unordered_map>

using namespace std;

namespace mt {
    // Fwd
    class PointCloud;
    class PointIsland;
    class Mesh;

    /**
     * Viewer virtual mouse events
     **/
    enum ViewerMouseBtn {
        VW_LEFT_BTN,
        VW_RIGHT_BTN,
        VW_MIDDLE_BTN
    };

    /**
     * Mesh and point cloud viewer implemented with GLFW
     */
    class Viewer {
    public:
        Viewer();

        /* Disposes resources */
        void dispose();

        /* Initialize the viewer */
        void initialize(int width, int height);

        /* Shows a point cloud */
        void addPointCloud(string name, PointCloud* pointCloud);
        /* Shows a fsoctree */
        void addFsOctree(string name, PointCloud* pointCloud, FsOctree* octree);
        /* Shows a mesh */
        void addMesh(string name, Mesh* mesh);
		/* Shows a circle */
		void addCircle(string name, Circle* circle);

        /* Remove object */
        void removeObject(string name);
        /* Invalidate object */
        void invalidateObject(string name);
        /* Set object visibility */
        void setVisible(string name, bool visible);
        /* Focus to object*/
        void focusToObject(string name);
		/* Center on object*/
		void centerOnObject(string name);
        /* Deletes the selection */
        void deleteSelection(string name);
        /* Cancel the current selection on the object */
        void clearSelection(string name);
        /*  */
        void setOctreeLevel(string name, int level);

        /* Render one frame */
        void renderFrame();
        /* Render a rectangle */
        void renderRect(int left, int top, int right, int bottom, float r, float g, float b);
		/* Render a bbox */
		void renderCircle(float x, float y, float z, float radius, float r, float g, float b);
        /* Render a bbox */
        void renderBbox(BBox* bbox, float r, float g, float b);
        /* Asserts if point is visible in the specified viewport */
        bool pointVisible(float oX, float oY, float oZ, int vx, int vy, int vw, int vh);
        /* Asserts if box is visible in the specified viewport (0=not visible, 1=partially visible, 2=fully visible)*/
        int boxVisible(BBox &box, int vx, int vy, int vw, int vh);

        // Access
        int getVPosLocation();
        int getVColLocation();

        // Lock view semaphore
        void lock();
        void unlock();
        
        // Set scale directly
        void setScale(float scale);

        // Get viewport size
        int getWidth();
        int getHeight();

        // -- Mouse controls --
        void onMouseMoved(double x, double y);
        void onMousePressed(ViewerMouseBtn button, double x, double y);
        void onMouseReleased(ViewerMouseBtn button, double x, double y);
        void onMouseWheelScrolled(double x, double y);
        void onResize(int width, int height);

		void updateSceneBBox(View* object);

    private:
        /* Compile shader */
        GLuint compileShader(const GLchar* source, GLenum type);
        
        /* Update the MVP */
        void updateMvp(float cx, float cy, float cz);

        /* Project screen point to control sphere */
        Vec3f sphereProject(float x, float y);

        // Controls
        bool dragging;
        Vec3f sphereClickPoint;
        Quaternion dragRotation;
        // Panning
        float panDx, panDy, panClickX, panClickY;
        bool panning;
        // Drag-selection
        int selOx, selOy;
        int selCx, selCy;

        // Viewport size
        int w, h;

        // Camera position
        float camx, camy, camz;
        // Camera zoom
        float distance;
        float scale;
        float znear, zfar;
		float fov;
        Vec3f center;


        // Object current rotation
        Quaternion cloudRotation;
        Matrix cloudTransform;

        // The view objects
        unordered_map<string, View*> objects;
        list<View*> toRelease;

        // GL objects
        GLuint program;
        GLint mvp_location;
        GLint vpos_location;
        GLint vcol_location;

        // Transform
        Matrix projection;
        Matrix view;
        Matrix mvp;
		BBox sceneBBox;

        // Mutex
        MUTEX mutex;
    };
}
