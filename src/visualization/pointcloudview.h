#pragma once
#ifdef _WIN32
#include <windows.h>
#endif

#include "view.h"
#include <datatypes/pointcloud.h>
#include <datatypes/octree.h>

#include <vector>

using namespace std;

namespace mt {

    /**
     * A GL view of a pointcloud
     */
    class PointCloudView : public View {
        friend class Viewer;

    public:
        /**
         * Creates a new point cloud view for a mesh
         */
        PointCloudView(Viewer* viewer, PointCloud* pointCloud);

        // Release
        void onRelease();
        // Render
        void onRender();
        // Reload
        void onObjectReload();
        // Pick
        void onPick(Ray &ray);
        void onPick(int vx, int vy, int vw, int vh);
        void onDeleteSelection();
        void onClearSelection();
        void setOctreeLevel(int level);

        // BBox
        BBox* getBBox();

        // Render octant and all children
        void renderOctant(Octree::Octant *octant, int level);

    private:
        // Visible points in viewport
        void selectVisiblePoints(Octree::Octant* octant,
                int vx, int vy, int vw, int vh, vector<PointCell*>* out);

        // PC
        PointCloud* pointCloud;
        // Selection
        vector<PointCell*> selection;
        // Selection points for rendering
        vector<Point> selectionPoints;
        vector<int> selectionColors;

        // GL
        uint allVbo;

        // Octree level to display
        int octreeLevel;
    };
}