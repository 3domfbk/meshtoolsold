/*
 * File:   fsoctreeview.h
 * Author: lake
 *
 * Created on January 3, 2017, 4:24 PM
 */
#pragma once

#include "view.h"
#include <map>
#include <datatypes/pointcloud.h>
#include <datatypes/fsoctree.h>

namespace mt {

    class FsOctreeView : public View {
            friend class Viewer;

    public:
        typedef struct NodeView {
            FsOctree::Octant* octant;
            // GL
            int numpoints;
            uint allVbo;
        } NodeView;

        /**
         * Creates a new point cloud view for a mesh
         */
        FsOctreeView(Viewer* viewer, PointCloud* pointCloud, FsOctree* octree);

        // Release
        void onRelease();
        // Render
        void onRender();
        // Reload
        void onObjectReload();
        // Pick
        void onPick(Ray &ray);
        void onPick(int vx, int vy, int vw, int vh);
        void onDeleteSelection();
        void onClearSelection();
        void setOctreeLevel(int level);
        // BBox
        BBox* getBBox();
        // Determines whether an octant is visible
        bool isVisible(FsOctree::Octant* o);

        // Load/release nodes
        void updateNodes();
        NodeView* loadNode(FsOctree::Octant* octant);
        void unloadNode(NodeView* node);
        void unloadBranch(FsOctree::Octant* o);

    private:
        // PC
        PointCloud* pointCloud;
        // Octree
        FsOctree* octree;

        // Nodes
        map<string, NodeView*> nodes;
    };

}
