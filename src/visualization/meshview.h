#pragma once
#ifdef _WIN32
#include <windows.h>
#endif

//#include <GL/glew.h>
//#define GLFW_INCLUDE_GLU
//#include <GLFW/glfw3.h>

#include "view.h"
#include <datatypes/mesh.h>

#include <vector>

using namespace std;

namespace mt {

    /**
     * A GL view of a mesh
     */
    class MeshView : public View {
        friend class Viewer;

    public:
        /**
         * Creates a new mesh view for a mesh
         */
        MeshView(Viewer* viewer, Mesh* mesh);

        // Release
        void onRelease();
        // Render
        void onRender();
        // Reload
        void onObjectReload();
        // Pick
        void onPick(Ray &ray);
        void onPick(int vx, int vy, int vw, int vh);
        void onDeleteSelection();
        void onClearSelection();
        void setOctreeLevel(int level);

        // BBox
        BBox* getBBox();

    private:
        // Mesh
        Mesh* mesh;
        // Indices
        vector<unsigned int> indices;

        // GL
        uint posVbo;
        uint colVbo;
    };
}