#pragma once
#ifdef _WIN32
#include <windows.h>
#endif

#include "view.h"
#include <datatypes/circle.h>

#include <vector>

using namespace std;

namespace mt {

    /**
     * A GL view of a circle
     */
    class CircleView : public View {
        friend class Viewer;

    public:
        /**
         * Creates a new point cloud view for a mesh
         */
		CircleView(Viewer* viewer, Circle* circle);

        // Release
        void onRelease();
        // Render
        void onRender();
        // Reload
        void onObjectReload();
        // Pick
        void onPick(Ray &ray);
        void onPick(int vx, int vy, int vw, int vh);
        void onDeleteSelection();
        void onClearSelection();
        void setOctreeLevel(int level);

        // BBox
        BBox* getBBox();
		
    private:
        
        // PC
        Circle* circle;

		// GL
		uint posVbo;
		uint colVbo;
    };
}