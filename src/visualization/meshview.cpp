#include <visualization/meshview.h>
#include <visualization/viewer.h>

using namespace mt;

MeshView::MeshView(Viewer* viewer, Mesh* mesh) : View(viewer) {
    this->mesh = mesh;
    this->posVbo = -1;
    this->colVbo = -1;
}

void MeshView::onRelease() {
    GLuint vbos[] = {posVbo, colVbo};
    glDeleteBuffers(2, vbos);

    this->posVbo = -1;
    this->colVbo = -1;
}

void MeshView::onObjectReload() {
    if (posVbo == -1) glGenBuffers(1, &posVbo);
    if (colVbo == -1) glGenBuffers(1, &colVbo);
    glBindBuffer(GL_ARRAY_BUFFER, posVbo);
    glBufferData(GL_ARRAY_BUFFER, mesh->n_vertices() * sizeof (GLfloat) * 3, mesh->points(), GL_STATIC_DRAW);

    if (mesh->has_vertex_colors()) {
        glBindBuffer(GL_ARRAY_BUFFER, colVbo);
        glBufferData(GL_ARRAY_BUFFER, mesh->n_vertices() * sizeof (GLubyte) * 3, mesh->vertex_colors(), GL_STATIC_DRAW);
    }

    // Recalculate indices
    Mesh::ConstFaceIter f_it(mesh->faces_sbegin()), f_end(mesh->faces_end());
    Mesh::ConstFaceVertexIter fv_it;

    indices.clear();
    indices.reserve(mesh->n_faces() * 3);

    for (; f_it != f_end; ++f_it) {
        indices.push_back((fv_it = mesh->cfv_iter(*f_it))->idx());
        indices.push_back((++fv_it)->idx());
        indices.push_back((++fv_it)->idx());
    }
}

void MeshView::onRender() {
    // Positions
    glEnableVertexAttribArray(viewer->getVPosLocation());
    glBindBuffer(GL_ARRAY_BUFFER, posVbo);
    glVertexAttribPointer(viewer->getVPosLocation(), 3, GL_FLOAT, GL_FALSE,
            sizeof (GLfloat) * 3, (void*) 0);

    // Vcolors
    if (mesh->has_vertex_colors()) {
        glEnableVertexAttribArray(viewer->getVColLocation());
        glBindBuffer(GL_ARRAY_BUFFER, colVbo);
        glVertexAttribPointer(viewer->getVColLocation(), 3, GL_UNSIGNED_BYTE, GL_TRUE,
                sizeof (GLubyte) * 3, (void*) 0);
    }

    // Render
    glDrawElements(GL_TRIANGLES, (int) indices.size(), GL_UNSIGNED_INT, &indices[0]);
}

void MeshView::onPick(Ray &ray) {

}

void MeshView::onPick(int vx, int vy, int vw, int vh) {
}

void MeshView::onDeleteSelection() {
}

void MeshView::onClearSelection() {
}

void MeshView::setOctreeLevel(int level) {
}

BBox* MeshView::getBBox() {
    return mesh->getBBox();
}