#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Remesh a mesh with adaptive remesh
     */
    void aremesh(Mesh* mesh, float error, float minEdgeLength, float maxEdgeLength, int iterations);
}