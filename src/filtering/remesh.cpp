#include <filtering/remesh.h>
#include <IsotropicRemesher/IsotropicRemesherT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>

#include <iostream>

using namespace std;

namespace mt {

    void remesh(Mesh* mesh, double targetEdgeLength) {
        IsotropicRemesher<Mesh> remesher;
        remesher.remesh(*mesh, targetEdgeLength);

        mesh->update();
    }
}