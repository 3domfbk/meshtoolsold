#include <stack>
#include <algorithm>
#include <utils/types.h>
#include <datatypes/mesh.h>
#include <filtering/fedgelength.h>

namespace mt {

    void fedgelength(Mesh* mesh, float minArea) {
        // Process all faces
        for (Mesh::FaceIter fx = mesh->faces_begin();
                fx != mesh->faces_end(); fx++) {
            for (Mesh::FaceHalfedgeIter hx = mesh->cfh_begin(*fx); hx != mesh->cfh_end(*fx); hx++) {
                double l = mesh->calc_edge_length(*hx);
                if (l > minArea) {
                    mesh->delete_face(*fx);
                    break;
                }
            }
        }

        mesh->update();
    }

}