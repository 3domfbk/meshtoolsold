#include <filtering/nonmanifolds.h>
#include <MeshRepair/NonManifoldVertexFixingT.hh>

namespace mt {

    void nonmanifolds(Mesh* mesh) {
        NonManifoldVertexFixingT<Mesh> fixer(*mesh);
        fixer.fix();

        mesh->update();
    }
}