#pragma once
#include <datatypes/octree.h>

namespace mt {
    /**
     * Downsample a cloud
     */
    PointCloud* downsample(PointCloud* cloud, float voxelSide, bool useVoxelCenter = false);
}