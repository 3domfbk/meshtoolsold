#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Remove non-manifold faces from mesh
     */
    void nonmanifolds(Mesh* mesh);
}