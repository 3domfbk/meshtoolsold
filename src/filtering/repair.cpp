#include <filtering/repair.h>
#include <MeshRepair/MeshFixingT.hh>

namespace mt {

    void repair(Mesh* mesh, float epsilon) {
        MeshFixing<Mesh> fixer(*mesh, epsilon);
        fixer.fix();

        mesh->update();
    }
}