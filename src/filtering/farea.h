#pragma once
#include <datatypes/mesh.h>

#include <vector>

using namespace std;

namespace mt {

    /**
     * Filter out all faces bigger than specified size
     */
    void farea(Mesh* mesh, float minArea);
}