#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Repair a mesh using openFlipper algorithm
     */
    void repair(Mesh* mesh, float epsilon);
}