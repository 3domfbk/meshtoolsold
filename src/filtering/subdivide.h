#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Decimate a mesh
     */
    void subdivide(Mesh* mesh, int times);
}