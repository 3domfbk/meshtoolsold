#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Repair a mesh using openFlipper algorithm
     */
    void holefill(Mesh* mesh);
}