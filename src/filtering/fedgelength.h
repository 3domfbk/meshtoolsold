#pragma once
#include <datatypes/mesh.h>

#include <vector>

using namespace std;

namespace mt {

    /**
     * Filter out all faces where at least 1 edge is longer than specified size
     */
    void fedgelength(Mesh* mesh, float minLength);
}