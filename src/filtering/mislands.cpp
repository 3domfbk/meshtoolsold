#include <stack>
#include <unordered_set>
#include <unordered_map>
#include <filtering/mislands.h>

namespace mt {

    bool meshCompare(Mesh* i1, Mesh* i2) {
        return i1->getBBox()->volume() > i2->getBBox()->volume();
    }

    vector<Mesh*> mislands(Mesh* mesh) {
        // Status hashtable for faces!
        unordered_set<int> processed;

        // Handle map
        unordered_map<int, int> old2new;

        // Resulting list
        vector<Mesh*> res;

        // List of faces to process
        stack<int> toProcess;

        // Triangle
        VertexHandle v[3];

        // Process all faces
        for (Mesh::FaceIter fx = mesh->faces_begin();
                fx != mesh->faces_end(); fx++) {
            int fidx = fx->idx();
            // Skip already processed
            if (processed.count(fidx) > 0) continue;

            // Mark to process
            toProcess.push(fidx);

            old2new.clear();
            Mesh* island = new Mesh();
            island->request_vertex_normals();
            island->request_face_normals();
            island->request_halfedge_normals();
            island->request_vertex_status();
            island->request_halfedge_status();
            island->request_face_status();
            island->request_edge_status();
            island->request_vertex_colors();
            island->request_face_colors();

            while (!toProcess.empty()) {
                int fid = toProcess.top();
                toProcess.pop();
                if (processed.count(fid) > 0) continue;

                // Add face to island
                FaceHandle fh(fid);
                if (!fh.is_valid()) continue;
                auto fv_it = mesh->cfv_ccwiter(fh);
                if (!fv_it->is_valid()) continue;

                // Assume it's a triangle.
                for (int i = 0; i < 3; i++) {
                    if (old2new.count(fv_it->idx()) > 0) {
                        v[i] = VertexHandle(old2new[fv_it->idx()]);
                    } else {
                        Vec3f p1 = mesh->point(*fv_it);
                        v[i] = island->add_vertex(p1);
                        Vec3uc C1 = mesh->color(*fv_it);
                        island->set_color(v[i], C1);
                        old2new[fv_it->idx()] = v[i].idx();
                    }
                    fv_it++;
                }

                FaceHandle f = island->add_face(v[0], v[1], v[2]);

                // Set as processed
                processed.insert(fid);

                // Add all neighbouring faces to process queue
                Mesh::FaceFaceIter ffi = mesh->ff_begin(FaceHandle(fid));
                for (; ffi != mesh->ff_end(FaceHandle(fid)); ++ffi) {
                    int idx = ffi->idx();
                    if (processed.count(idx) == 0) {
                        toProcess.push(idx);
                    }
                }
            }

            island->update();
            res.push_back(island);
        }

        sort(res.begin(), res.end(), meshCompare);
        return res;
    }

}