#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Smooth out a mesh
     */
    void smooth(Mesh* mesh, int steps);
}