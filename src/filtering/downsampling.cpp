#include <filtering/downsampling.h>
#include <utils/types.h>
#include <stack>

using namespace std;

namespace mt {

    PointCloud* downsample(PointCloud* cloud, float leafSize, bool useVoxelCenter) {
        // Create octree if needed
        cloud->buildOctreeLS(leafSize);

        // Create resulting point cloud
        PointCloud* res = new PointCloud(cloud->getOctree()->leafsLS(leafSize));

        // Iterate the octants
        stack<Octree::Octant*> toProcess;
        toProcess.push(cloud->getOctree()->getRoot());

        while (!toProcess.empty()) {
            Octree::Octant* o = toProcess.top();
            toProcess.pop();

            if (o->leaf) {
                // Process
                if (o->numPoints == 0) continue;

                // Take barycenter of points inside voxel
                float xs = 0, ys = 0, zs = 0;
                float nxs = 0, nys = 0, nzs = 0;
                int rs = 0, gs = 0, bs = 0;
                
                PointCell* p = o->start;
                for (int j = 0; j < o->numPoints; j++) {
                    xs += p->data.x;
                    ys += p->data.y;
                    zs += p->data.z;
                    nxs += p->data.nx;
                    nys += p->data.ny;
                    nzs += p->data.nz;
                    int rgb = p->data.rgb;
                    rs += RED(rgb);
                    gs += GREEN(rgb);
                    bs += BLUE(rgb);

                    p = p->next;
                }

                int psize = (int) o->numPoints;
                xs /= psize;
                ys /= psize;
                zs /= psize;
                nxs /= psize;
                nys /= psize;
                nzs /= psize;
                rs /= psize;
                gs /= psize;
                bs /= psize;
                
                // Specify to use voxel points barycenter or voxel center (regular grid)
                if(!useVoxelCenter)
                    res->addPoint(xs, ys, zs, BGR(rs, gs, bs),nxs,nys,nzs);
                else
                    res->addPoint(o->center[0], o->center[1], o->center[2], BGR(rs, gs, bs), nxs, nys, nzs);
            }

            // Recursion
            for (int i = 0; i < 8; i++) {
                if (o->children[i]) toProcess.push(o->children[i]);
            }
        }

        res->copyProps(cloud);
        res->update();
        return res;
    }
}
