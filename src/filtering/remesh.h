#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Remesh a mesh
     */
    void remesh(Mesh* mesh, double targetEdgeLength);
}