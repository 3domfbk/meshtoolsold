#pragma once
#include <datatypes/mesh.h>

namespace mt {

    /**
     * Decimate a mesh
     */
    void decimate(Mesh* mesh, int targetVertices);
}