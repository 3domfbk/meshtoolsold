#include <filtering/smooth.h>
#include <OpenMesh/Tools/Smoother/JacobiLaplaceSmootherT.hh>

#include <iostream>

using namespace std;
using namespace OpenMesh::Smoother;

namespace mt {

    void smooth(Mesh* mesh, int steps) {
        JacobiLaplaceSmootherT<Mesh> smoother(*mesh);
        smoother.initialize(JacobiLaplaceSmootherT<Mesh>::Normal, JacobiLaplaceSmootherT<Mesh>::C2);
        smoother.smooth(steps);

        mesh->update();
    }
}