#include <stack>
#include <algorithm>
#include <utils/types.h>
#include <filtering/islands.h>
#include <datatypes/octree.h>
#include <datatypes/voxelgrid.h>
#include <string.h>

namespace mt {

    bool cloudsCompare(PointCloud* i1, PointCloud* i2) {
        return i1->getBBox()->volume() > i2->getBBox()->volume();
    }

    vector<PointCloud*> islands(PointCloud* cloud, float minDistance) {
        // Make the grid
        VoxelGrid grid(cloud, minDistance);
        // Keep the state of the voxels
        byte* state = new byte[grid.getNumVoxels()];
        memset(state, 0, sizeof (byte) * grid.getNumVoxels());

        // Resulting list
        vector<PointCloud*> res;

        // List of voxels to process
        stack<int> toProcess;

        // Process all voxels
        for (int i = 0; i < grid.getNumVoxels(); i++) {
            if (state[i]) continue;

            vector<PointCell*> island;
            toProcess.push(i);
            while (!toProcess.empty()) {
                int vid = toProcess.top();
                toProcess.pop();
                Voxel* v = &grid.getVoxels()[vid];

                // Skip if already processed
                if (state[vid]) continue;
                // Skip if empty
                if (v->numPoints == 0) continue;

                // Add all points of the voxel to island
                VoxelPoint* p = v->start;
                for (int j = 0; j < v->numPoints; j++) {
                    island.push_back(p->data);
                    p = p->next;
                }
                // Set as "processed"
                state[vid] = 1;

                // Add all neighbors
                int x = grid.idX(vid);
                int y = grid.idY(vid);
                int z = grid.idZ(vid);
                // Left
                if (x > 0) toProcess.push(grid.id(x - 1, y, z));
                // Right
                if (x < grid.getWidth() - 1) toProcess.push(grid.id(x + 1, y, z));
                // Up
                if (y > 0) toProcess.push(grid.id(x, y - 1, z));
                // Down
                if (y < grid.getHeight() - 1) toProcess.push(grid.id(x, y + 1, z));
                // Fwd
                if (z > 0) toProcess.push(grid.id(x, y, z - 1));
                // Bwd
                if (z < grid.getDepth() - 1) toProcess.push(grid.id(x, y, z + 1));
            }
            if (!island.empty()) {
                PointCloud* icl = new PointCloud((int) island.size());
                for (int i = 0; i < island.size(); i++) {
                    float x = island[i]->data.x;
                    float y = island[i]->data.y;
                    float z = island[i]->data.z;
                    float nx = island[i]->data.nx;
                    float ny = island[i]->data.ny;
                    float nz = island[i]->data.nz;
                    int rgb = island[i]->data.rgb;
                    icl->addPoint(x, y, z, rgb, nx, ny, nz);
                }
                res.push_back(icl);
                icl->copyProps(cloud);
                icl->update();
            }
        }

        sort(res.begin(), res.end(), cloudsCompare);
        delete[] state;
        return res;
    }

}
