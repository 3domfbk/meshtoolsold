#pragma once
#include <datatypes/pointcloud.h>

#include <vector>

using namespace std;

namespace mt {
    /**
     * Divide point cloud in islands
     */
    vector<PointCloud*> islands(PointCloud* grid, float maxDistance);
}
