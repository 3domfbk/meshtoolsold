#pragma once
#include <datatypes/mesh.h>

#include <vector>

using namespace std;

namespace mt {

    /**
     * Divide mesh in connected islands
     */
    vector<Mesh*> mislands(Mesh* mesh);
}