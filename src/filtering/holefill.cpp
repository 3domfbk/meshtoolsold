#include <filtering/holefill.h>
#include <HoleFilling/HoleFillerT.hh>

namespace mt {

    void holefill(Mesh* mesh) {
        HoleFiller<Mesh> filler(*mesh);
        filler.fill_all_holes();

        mesh->update();
    }
}