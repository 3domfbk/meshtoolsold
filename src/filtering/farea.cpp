#include <stack>
#include <algorithm>
#include <utils/types.h>
#include <datatypes/mesh.h>
#include <filtering/farea.h>

namespace mt {

    void farea(Mesh* mesh, float minArea) {
        // Process all faces
        for (Mesh::FaceIter fx = mesh->faces_begin();
                fx != mesh->faces_end(); fx++) {
            double area = mesh->calc_sector_area(mesh->halfedge_handle(*fx));
            if (area > minArea) {
                mesh->delete_face(*fx);
            }
        }

        mesh->update();
    }

}