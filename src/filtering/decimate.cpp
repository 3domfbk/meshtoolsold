#include <filtering/decimate.h>
#include <OpenMesh/Tools/Decimater/DecimaterT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>

#include <iostream>

using namespace std;
using namespace OpenMesh::Decimater;

namespace mt {

    void decimate(Mesh* mesh, int targetVertices) {
        cout << "Vertices: " << mesh->n_vertices() << endl;

        DecimaterT<Mesh> decimater(*mesh); // a decimater object, connected to a mesh
        ModQuadricT<Mesh>::Handle hModQuadric; // use a quadric module

        decimater.add(hModQuadric); // register module at the decimater
        decimater.initialize(); // let the decimater initialize the mesh and the
        // modules
        decimater.decimate_to(targetVertices); // do decimation
        mesh->update();

        cout << "Vertices: " << decimater.mesh().n_vertices() << endl;
    }
}