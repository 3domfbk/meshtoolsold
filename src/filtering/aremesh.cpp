#include <filtering/aremesh.h>
#include <Remesher/AdaptiveRemesherT.hh>

using namespace Remeshing;

namespace mt {

    void aremesh(Mesh* mesh, float error, float minEdgeLength, float maxEdgeLength, int iterations) {
        AdaptiveRemesherT<Mesh> remesher(*mesh);
        remesher.remesh(error, minEdgeLength, maxEdgeLength, iterations, true);

        mesh->update();
    }
}