#include <filtering/decimate.h>
#include <OpenMesh/Tools/Subdivider/Uniform/CatmullClarkT.hh>
#include <OpenMesh/Tools/Decimater/ModQuadricT.hh>

#include <iostream>

using namespace std;
using namespace OpenMesh::Subdivider;

namespace mt {

    void subdivide(Mesh* mesh, int times) {
        cout << "Vertices: " << mesh->n_vertices() << endl;

        Uniform::CatmullClarkT<Mesh> divider;
        divider.attach(*mesh);
        divider(times);
        divider.detach();

        mesh->update();

        cout << "Vertices: " << mesh->n_vertices() << endl;
    }
}