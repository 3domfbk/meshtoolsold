#include <jni.h>
#include <MeshtoolsConfig.h>
#include <visualization/viewer.h>

using namespace mt;

extern "C" {

    jlong Java_eu_fbk_threedom_meshtools_visualization_Viewer_ncreate(
            JNIEnv *env,
            jclass clazz) {

        Viewer* v = new Viewer();
        return (jlong) v;
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nrelease(
            JNIEnv *env,
            jclass clazz,
            jlong handle) {

        Viewer* v = (Viewer*) handle;
        delete v;
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_ninitialize(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jint width,
            jint height) {

        Viewer* v = (Viewer*) handle;
        v->initialize(width, height);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_naddPointCloud(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jstring name,
            jlong pointcloud) {

        const char* cname = env->GetStringUTFChars(name, 0);
        Viewer* v = (Viewer*) handle;
        PointCloud* pc = (PointCloud*) pointcloud;
        v->addPointCloud(cname, pc);
        env->ReleaseStringUTFChars(name, cname);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_ndeleteSelection(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jstring name) {

        const char* cname = env->GetStringUTFChars(name, 0);
        Viewer* v = (Viewer*) handle;
        v->deleteSelection(cname);
        v->invalidateObject(cname);
        env->ReleaseStringUTFChars(name, cname);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nfocusToObject(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jstring name) {

        Viewer* v = (Viewer*) handle;
        const char* cname = env->GetStringUTFChars(name, 0);
        v->focusToObject(cname);
        env->ReleaseStringUTFChars(name, cname);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nrenderFrame(
            JNIEnv *env,
            jclass clazz,
            jlong handle) {

        Viewer* v = (Viewer*) handle;
        v->renderFrame();
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nonMouseMoved(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jfloat x,
            jfloat y) {

        Viewer* v = (Viewer*) handle;
        v->onMouseMoved(x,y);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nonMousePressed(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jint type,
            jfloat x,
            jfloat y) {

        Viewer* v = (Viewer*) handle;
        v->onMousePressed((ViewerMouseBtn) type , x, y);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nonMouseReleased(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jint type,
            jfloat x,
            jfloat y) {

        Viewer* v = (Viewer*) handle;
        v->onMouseReleased((ViewerMouseBtn) type, x, y);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nsetScale(
        JNIEnv *env,
            jclass clazz,
            jlong handle,
            jfloat scale) {
        Viewer* v = (Viewer*) handle;
        v->setScale(scale);
    }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nonMouseWheelScrolled(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jfloat numberOfScroll) {

          Viewer* v = (Viewer*) handle;
          v->onMouseWheelScrolled(1.0, numberOfScroll);
      }

    void Java_eu_fbk_threedom_meshtools_visualization_Viewer_nremoveObject(
            JNIEnv *env,
            jclass clazz,
            jlong handle,
            jstring name) {

        Viewer* v = (Viewer*) handle;
        const char* cname = env->GetStringUTFChars(name, 0);
        v->removeObject(cname);
        env->ReleaseStringUTFChars(name, cname);
    }
}
