#include <jni.h>
#include <MeshtoolsConfig.h>
#include <filtering/islands.h>
#include <datatypes/pointcloud.h>
#include <visualization/viewer.h>
#include <vector>
#include <string.h>

using namespace mt;

extern "C"{
  jlongArray Java_eu_fbk_threedom_meshtools_filtering_Islands_nislands(
          JNIEnv *env,
          jclass clazz,
          jlong handle,
          jfloat maxDistance) {

      jlongArray result;

      PointCloud* cloud = (PointCloud*) handle;
      vector<PointCloud*> handleVector = islands(cloud, maxDistance);
      int size =  (int) handleVector.size();

      jlong *fill = new jlong[size];
      for(int i = 0; i < size; i++){
        fill[i] = (jlong) handleVector[i];
      }

      result = env->NewLongArray(size);
      if (result == NULL) {
        return NULL; /* out of memory error thrown */
      }
      // move from the temp structure to the java structure
      env->SetLongArrayRegion(result, 0, size, fill);

      delete fill;
      return result;
  }
}
