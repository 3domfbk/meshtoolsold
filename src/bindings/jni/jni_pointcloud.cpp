#include <jni.h>
#include <datatypes/pointcloud.h>
#include <MeshtoolsConfig.h>

using namespace mt;


extern "C" {
jlong Java_eu_fbk_threedom_meshtools_datatypes_PointCloud_ncreate(
        JNIEnv *env,
        jclass clazz,
        jint initialCapacity) {

    PointCloud* cloud = new PointCloud(initialCapacity);
    return (jlong) cloud;
}

void Java_eu_fbk_threedom_meshtools_datatypes_PointCloud_nrelease(
        JNIEnv *env,
        jclass clazz,
        jlong handle) {

    PointCloud* cloud = (PointCloud*) handle;
    delete cloud;
}

void Java_eu_fbk_threedom_meshtools_datatypes_PointCloud_naddPoint(
        JNIEnv *env,
        jclass clazz,
        jlong handle,
        jdouble x,
        jdouble y,
        jdouble z,
        jint rgb) {

    PointCloud* cloud = (PointCloud*) handle;
    cloud->addPoint(x, y, z, rgb, 0.0, 0.0, 0.0); // To fix with normals
}

jlong Java_eu_fbk_threedom_meshtools_datatypes_PointCloud_ngetNumPoints(
        JNIEnv *env,
        jclass clazz,
        jlong handle) {

    PointCloud* cloud = (PointCloud*) handle;
    return cloud->getNumPoints();
}

jfloat Java_eu_fbk_threedom_meshtools_datatypes_PointCloud_ngetAvgPointDistance(
        JNIEnv *env,
        jclass clazz,
        jlong handle) {

    PointCloud* cloud = (PointCloud*) handle;
    return cloud->getAvgPointDistance();
}

}
