#include <jni.h>
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <io/pointcloudwriter.h>

using namespace mt;

extern "C" {

  void Java_eu_fbk_threedom_meshtools_io_PointCloudWriter_nwrite(
          JNIEnv *env,
          jclass clazz,
          jstring path,
          jlong handle) {

      const char* cpath = env->GetStringUTFChars(path, 0);
      PointCloud* pc = (PointCloud*) handle;
      PcFormat fmt = PF_LIL_ENDIAN;
      PointCloudWriter::write(cpath, pc, fmt);
      env->ReleaseStringUTFChars(path, cpath);
  }

}
