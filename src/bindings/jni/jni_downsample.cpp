#include <jni.h>
#include <filtering/downsampling.h>
#include <string.h>
#include <datatypes/solid.h>
#include <datatypes/linkedlist.h>
#include <unordered_set>

using namespace mt;

extern "C" {
jlong Java_eu_fbk_threedom_meshtools_filtering_Downsampling_ndownsample(
        JNIEnv *env,
        jclass clazz,
        jlong handle,
        jfloat voxelSize) {

        PointCloud* cloud = (PointCloud*) handle;
        PointCloud* tmp = downsample(cloud, voxelSize);
        return (jlong) tmp;

  }
}
