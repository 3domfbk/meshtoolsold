#include <jni.h>

#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <MeshtoolsConfig.h>
#include <io/pointcloudreader.h>

using namespace mt;

extern "C" {

  jlong Java_eu_fbk_threedom_meshtools_io_PointCloudReader_nread(
          JNIEnv *env,
          jclass clazz,
          jstring path) {

      const char* cpath = env->GetStringUTFChars(path, 0);
      PointCloud* ppc = PointCloudReader::createReader(cpath, 400000)->readPointCloud();
      env->ReleaseStringUTFChars(path, cpath);
      return (jlong) ppc;
  }

}
