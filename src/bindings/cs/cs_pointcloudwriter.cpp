#include <utils/types.h>
#include <io/pointcloudwriter.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif

    MODULE_API bool pointcloudwriter_write(const char* path, PointCloud* cloud, const char* format) {
        PcFormat fmt = PF_LIL_ENDIAN;
        string saveFormat = format;
        if ("ASCII" == saveFormat || "ascii" == saveFormat) fmt = PF_ASCII;
        else if ("BIG_ENDIAN" == saveFormat || "big_endian" == saveFormat) fmt = PF_BIG_ENDIAN;
        return PointCloudWriter::write(path, cloud, fmt);
    }

#ifdef __cplusplus
}
#endif
