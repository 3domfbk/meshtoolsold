#include <utils/types.h>
#include <datatypes/pointcloud.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API PointCloud* pointcloud_new(int initialCapacity) {
        return new PointCloud(initialCapacity);
    }
    MODULE_API void pointcloud_dispose(PointCloud* cloud) {
        delete cloud;
    }
    MODULE_API long long pointcloud_getNumPoints(PointCloud* cloud) {
        return cloud->getNumPoints();
    }
    MODULE_API float pointcloud_getAvgPointDistance(PointCloud* cloud) {
        return cloud->getAvgPointDistance();
    }
    MODULE_API BBox* pointcloud_getBBox(PointCloud* cloud) {
        return cloud->getBBox();
    }
	MODULE_API LinkedList<Point>* pointcloud_getData(PointCloud* cloud) {
		return cloud->getData();
	}
	MODULE_API PointCell* pointcloud_getPoint(PointCloud* cloud, int index) {
		return cloud->getPoint(index);
	}
#ifdef __cplusplus
}
#endif
