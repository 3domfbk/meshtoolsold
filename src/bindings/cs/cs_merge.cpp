#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <processing/merge.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API PointCloud* merge_clouds(PointCloud** input, int size) {
        vector<PointCloud*> clouds;
        for (int i = 0; i < size; i++) {
            clouds.push_back(input[i]);
        }

        return merge(clouds);
    }
#ifdef __cplusplus
}
#endif
