#include <utils/types.h>
#include <io/pointcloudreader.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif

    MODULE_API PointCloudReader* pointcloudreader_createReader(const char* path, int buffersize) {
        return PointCloudReader::createReader(path, buffersize);
    }
    MODULE_API void pointcloudreader_dipose(PointCloudReader* reader) {
        delete reader;
    }
    MODULE_API PointCloud* pointcloudreader_readPointCloud(PointCloudReader* reader) {
        return reader->readPointCloud();
    }

#ifdef __cplusplus
}
#endif
