#include <utils/types.h>
#include <datatypes/circle.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API Circle* circle_new(float x, float y, float z, float radius, int r, int g, int b) {
        return new Circle(x, y, z, radius, BGR(r,g,b));
    }
    MODULE_API void circle_dispose(Circle* circle) {
        delete circle;
    }
#ifdef __cplusplus
}
#endif
