#include <utils/types.h>
#include <datatypes/mesh.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API Mesh* mesh_new() {
        return new Mesh();
    }
    MODULE_API void mesh_dispose(Mesh* mesh) {
        delete mesh;
    }
#ifdef __cplusplus
}
#endif
