#include <utils/types.h>
#include <datatypes/linkedlist.h>
#include <datatypes/pointcloud.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif

    MODULE_API LinkedList<Point>* linkedlist_point_new() {
        return new LinkedList<Point>();
    }

	MODULE_API void linkedlist_point_dispose(LinkedList<Point>* list) {
		delete list;
	}

	MODULE_API long long linkedlist_point_size(LinkedList<Point>* list) {
		return list->size();
	}

	MODULE_API void linkedlist_point_add(LinkedList<Point>* list, float x, float y, float z, float nx, float ny, float nz, int rgb) {
		return list->add(Point(x, y, z, rgb, nx, ny, nz));
	}

	MODULE_API PointCell* linkedlist_point_begin(LinkedList<Point>* list) {
		return list->begin();
	}

	MODULE_API PointCell* pointcell_next(PointCell* cell) {
		return cell->next;
	}

	MODULE_API float pointcell_getX(PointCell* cell) {
		return cell->data.x;
	}

	MODULE_API float pointcell_getY(PointCell* cell) {
		return cell->data.y;
	}

	MODULE_API float pointcell_getZ(PointCell* cell) {
		return cell->data.z;
	}

	MODULE_API float pointcell_getNX(PointCell* cell) {
		return cell->data.nx;
	}

	MODULE_API float pointcell_getNY(PointCell* cell) {
		return cell->data.ny;
	}

	MODULE_API float pointcell_getNZ(PointCell* cell) {
		return cell->data.nz;
	}

	MODULE_API int pointcell_getRGB(PointCell* cell) {
		return cell->data.rgb;
	}

#ifdef __cplusplus
}
#endif
