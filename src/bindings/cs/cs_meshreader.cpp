#include <utils/types.h>
#include <io/meshreader.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API MeshReader* meshreader_new(const char* path) {
        return new MeshReader(path);
    }
    MODULE_API void meshreader_dispose(MeshReader* reader) {
        delete reader;
    }
    MODULE_API Mesh* meshreader_read(MeshReader* reader) {
        return reader->readMesh();
    }

#ifdef __cplusplus
}
#endif
