#include <utils/types.h>
#include <visualization/viewer.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API Viewer* viewer_new() {
        return new Viewer();
    }
    MODULE_API void viewer_dispose(Viewer* viewer) {
        viewer->dispose();
    }
    MODULE_API void viewer_initialize(Viewer* viewer, int width, int height) {
        viewer->initialize(width, height);
    }
    MODULE_API void viewer_renderFrame(Viewer* viewer) {
        viewer->renderFrame();
    }
    MODULE_API void viewer_addPointCloud(Viewer* viewer, const char* name, PointCloud* cloud) {
        viewer->addPointCloud(name, cloud);
    }
    MODULE_API void viewer_addMesh(Viewer* viewer, const char* name, Mesh* mesh) {
        viewer->addMesh(name, mesh);
    }
	MODULE_API void viewer_addCircle(Viewer* viewer, const char* name, Circle* circle) {
		viewer->addCircle(name, circle);
	}
    MODULE_API void viewer_removeObject(Viewer* viewer, const char* name) {
        viewer->removeObject(name);
    }
    MODULE_API void viewer_onMousePressed(Viewer* viewer, int button, double x, double y) {
        viewer->onMousePressed((ViewerMouseBtn)button, x, y);
    }
    MODULE_API void viewer_onMouseReleased(Viewer* viewer, int button, double x, double y) {
        viewer->onMouseReleased((ViewerMouseBtn)button, x, y);
    }
    MODULE_API void viewer_onMouseMoved(Viewer* viewer, double x, double y) {
        viewer->onMouseMoved(x, y);
    }
    MODULE_API void viewer_onMouseWheelScrolled(Viewer* viewer, double x, double y) {
        viewer->onMouseWheelScrolled(x, y);
    }
    MODULE_API void viewer_onResize(Viewer* viewer, int x, int y) {
        viewer->onResize(x, y);
    }
    MODULE_API void viewer_deleteSelection(Viewer* viewer, const char * name) {
        viewer->deleteSelection(name);
    }
    MODULE_API void viewer_invalidateObject(Viewer* viewer, const char * name) {
        viewer->invalidateObject(name);
    }
    MODULE_API void viewer_focusToObject(Viewer* viewer, const char * name) {
        viewer->focusToObject(name);
    }
	MODULE_API void viewer_centerOnObject(Viewer* viewer, const char * name) {
		viewer->centerOnObject(name);
	}
    MODULE_API void viewer_clearSelection(Viewer* viewer, const char* name) {
        viewer->clearSelection(name);
    }
    MODULE_API void viewer_setOctreeLevel(Viewer* viewer, const char * name, int level) {
        viewer->setOctreeLevel(name, level);
    }

#ifdef __cplusplus
}
#endif