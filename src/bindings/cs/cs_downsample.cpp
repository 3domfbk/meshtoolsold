#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <filtering/downsampling.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API PointCloud* downsample_cloud(PointCloud* cloud, float leafSize, bool useVoxelCenter) {
        
        return downsample(cloud, leafSize, useVoxelCenter);
    }
#ifdef __cplusplus
}
#endif
