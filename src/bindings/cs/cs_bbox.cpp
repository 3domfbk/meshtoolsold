#include <utils/types.h>
#include <datatypes/bbox.h>

using namespace mt;

#ifdef __cplusplus
extern "C" {
#endif
    MODULE_API BBox* bbox_new() {
        return new BBox();
    }
    MODULE_API void bbox_dispose(BBox* bbox) {
        delete bbox;
    }

    MODULE_API float bbox_getCenterX(BBox* bbox) {
        return bbox->getCenterX();
    }
    MODULE_API float bbox_getCenterY(BBox* bbox) {
        return bbox->getCenterY();
    }

    MODULE_API float bbox_getCenterZ(BBox* bbox) {
        return bbox->getCenterZ();
    }

    MODULE_API float bbox_width(BBox* bbox) {
        return bbox->width();
    }

    MODULE_API float bbox_height(BBox* bbox) {
        return bbox->height();
    }

    MODULE_API float bbox_depth(BBox* bbox)
    {
        return bbox->depth();
    }

    MODULE_API float bbox_volume(BBox* bbox) {
        return bbox->volume();
    }

    MODULE_API float bbox_extent(BBox* bbox) {
        return bbox->extent();
    }

#ifdef __cplusplus
}
#endif
