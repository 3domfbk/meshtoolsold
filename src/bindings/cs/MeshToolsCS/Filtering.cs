﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class Filtering
    {
        public static PointCloud Downsample(PointCloud cloud, float leafSize, bool useVoxelCenter)
        {
            return new PointCloud(downsample_cloud(cloud.Handle, leafSize, useVoxelCenter));
        }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr downsample_cloud(IntPtr cloud, float leafSize, bool useVoxelCenter);
    }
}
