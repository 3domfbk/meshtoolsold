﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class Circle : NativeHandle
    {
        public Circle(float x, float y, float z, float radius, int r, int g, int b)
        {
            Handle = circle_new(x, y, z, radius, r, g, b);
        }

        public Circle(IntPtr intPtr)
        {
            this.Handle = intPtr;
        }

        public void Dispose() { circle_dispose(Handle); }

        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr circle_new(float x, float y, float z, float radius, int r, int g, int b);
        [DllImport("meshtools_shared.dll")]
        private static extern void circle_dispose(IntPtr circle);
    }
}
