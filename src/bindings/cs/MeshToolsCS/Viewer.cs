﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace MeshToolsCS
{
    public class Viewer : NativeHandle
    {
        public Viewer() { Handle = viewer_new(); }
        public void Initialize(int width, int height) { viewer_initialize(Handle, width, height); }
        public void Dispose() { viewer_dispose(Handle); }
        public void RenderFrame() { viewer_renderFrame(Handle); }
        public void AddPointCloud(string name, PointCloud cloud) { viewer_addPointCloud(Handle, name, cloud.Handle); }
        public void AddMesh(string name, Mesh mesh) { viewer_addMesh(Handle, name, mesh.Handle); }
        public void AddCircle(string name, Circle circle) { viewer_addCircle(Handle, name, circle.Handle); }
        public void RemoveObject(string name) { viewer_removeObject(Handle, name); }
        public void OnMouseDown(int button, double x, double y) { viewer_onMousePressed(Handle, button, x, y); }
        public void OnMouseUp(int button, double x, double y) { viewer_onMouseReleased(Handle, button, x, y); }
        public void OnMouseMove(double x, double y) { viewer_onMouseMoved(Handle, x, y); }
        public void OnResize(int x, int y) { viewer_onResize(Handle, x, y); }
        public void OnMouseWheelScrolled(double x, double y) { viewer_onMouseWheelScrolled(Handle, x, y); }
        public void DeleteSelection(string name) { viewer_deleteSelection(Handle, name); }
        public void InvalidateObject(string name) { viewer_invalidateObject(Handle, name); }
        public void FocusToObject(string name) { viewer_focusToObject(Handle, name); }
        public void CenterOnObject(string name) { viewer_centerOnObject(Handle, name); }
        public void ClearSelection(string name) { viewer_clearSelection(Handle, name); }
        public void SetOctreeLevel(string name, int level) { viewer_setOctreeLevel(Handle, name, level); }
        //public void RenderCircle(float x, float y, float z, float radius, float r, float g, float b)
        //{
        //    viewer_renderCircle(Handle, x, y, z, radius, r, g, b);
        //}

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr viewer_new();
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_dispose(IntPtr handle);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_initialize(IntPtr handle, int width, int height);
        [DllImport("meshtools_shared.dll")]
        static extern int viewer_renderFrame(IntPtr viewer);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_addPointCloud(IntPtr viewer, string name, IntPtr cloud);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_addMesh(IntPtr viewer, string name, IntPtr mesh);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_addCircle(IntPtr viewer, string name, IntPtr circle);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_removeObject(IntPtr viewer, string name);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_onMousePressed(IntPtr viewer, int button, double x, double y);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_onMouseReleased(IntPtr viewer, int button, double x, double y);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_onMouseMoved(IntPtr viewer, double x, double y);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_onResize(IntPtr viewer, int x, int y);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_onMouseWheelScrolled(IntPtr viewer, double x, double y);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_deleteSelection(IntPtr viewer, string name);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_invalidateObject(IntPtr viewer, string name);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_focusToObject(IntPtr viewer, string name);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_centerOnObject(IntPtr viewer, string name);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_clearSelection(IntPtr viewer, string name);
        [DllImport("meshtools_shared.dll")]
        static extern void viewer_setOctreeLevel(IntPtr viewer, string name, int level);
        //[DllImport("meshtools_shared.dll")]
        //static extern void viewer_renderCircle(IntPtr viewer, float x, float y, float z, float radius, float r, float g, float b);
    }
}
