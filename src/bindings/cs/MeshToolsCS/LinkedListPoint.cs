﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public struct Point
    {
        public float x, y, z, nx, ny, nz;
        public int rgb;

        public int R
        {
            get
            {
                return (rgb & 0xFF);
            }
        }

        public int G
        {
            get
            {
                return ((rgb >> 8) & 0xFF);
            }
        }

        public int B
        {
            get
            {
                return ((rgb >> 16) & 0xFF);
            }
        }
    }

    public class PointCell : NativeHandle
    {
        public PointCell(IntPtr handle)
        {
            Handle = handle;
        }

        public void Next()
        {
            Handle = pointcell_next(Handle);
        }

        public bool IsEnd()
        {
            return Handle == IntPtr.Zero;
        }

        public Point GetPoint()
        {
            Point point;

            point.x = pointcell_getX(Handle);
            point.y = pointcell_getY(Handle);
            point.z = pointcell_getZ(Handle);
            point.nx = pointcell_getNX(Handle);
            point.ny = pointcell_getNY(Handle);
            point.nz = pointcell_getNZ(Handle);
            point.rgb = pointcell_getRGB(Handle);

            return point;
        }

        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr pointcell_next(IntPtr cell);        
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcell_getX(IntPtr cell);
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcell_getY(IntPtr cell);
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcell_getZ(IntPtr cell);
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcell_getNX(IntPtr cell);
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcell_getNY(IntPtr cell);
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcell_getNZ(IntPtr cell);
        [DllImport("meshtools_shared.dll")]
        private static extern int pointcell_getRGB(IntPtr cell);
    }

    public class LinkedListPoint : NativeHandle
    {        
        public LinkedListPoint(IntPtr handle)
        {
            Handle = handle;
        }

        public LinkedListPoint()
        {
            Handle = linkedlist_point_new();
        }

        public void Dispose()
        {
            linkedlist_point_dispose(Handle);
        }

        public long Size()
        {
            return linkedlist_point_size(Handle);
        }

        public void Add(Point point)
        {
            linkedlist_point_add(Handle, point.x, point.y, point.z, point.nx, point.ny, point.nz, point.rgb);
        }

        public PointCell Begin()
        {
            return new PointCell(linkedlist_point_begin(Handle));
        }

        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr linkedlist_point_new();
        [DllImport("meshtools_shared.dll")]
        private static extern void linkedlist_point_dispose(IntPtr list);
        [DllImport("meshtools_shared.dll")]
        private static extern long linkedlist_point_size(IntPtr list);
        [DllImport("meshtools_shared.dll")]
        private static extern void linkedlist_point_add(IntPtr list, float x, float y, float z, float nx, float ny, float nz, int rgb);
        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr linkedlist_point_begin(IntPtr list);
    }    
}
