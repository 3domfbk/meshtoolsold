﻿using System;
using System.Runtime.InteropServices;

namespace MeshToolsCS
{
    public class PointCloud : NativeHandle
    {
        public PointCloud(int initialCapacity) { Handle = pointcloud_new(initialCapacity); }

        public PointCloud(IntPtr intPtr)
        {
            this.Handle = intPtr;
        }

        public void Dispose() { pointcloud_dispose(Handle); }
        public long GetNumPoints() { return pointcloud_getNumPoints(Handle); }
        public float GetAveragePointsDistance() { return pointcloud_getAvgPointDistance(Handle); }
        public BoundingBox GetBoundingBox() { return new BoundingBox(pointcloud_getBBox(Handle)); }
        public LinkedListPoint GetData() { return new LinkedListPoint(pointcloud_getData(Handle)); }
        public PointCell GetPoint(int index) { return new PointCell(pointcloud_getPoint(Handle, index)); }

        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr pointcloud_new(int initialCapacity);
        [DllImport("meshtools_shared.dll")]
        private static extern void pointcloud_dispose(IntPtr cloud);
        [DllImport("meshtools_shared.dll")]
        private static extern long pointcloud_getNumPoints(IntPtr cloud);
        [DllImport("meshtools_shared.dll")]
        private static extern float pointcloud_getAvgPointDistance(IntPtr cloud);
        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr pointcloud_getBBox(IntPtr cloud);
        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr pointcloud_getData(IntPtr cloud);
        [DllImport("meshtools_shared.dll")]
        private static extern IntPtr pointcloud_getPoint(IntPtr cloud, int index);
    }
}