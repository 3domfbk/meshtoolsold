﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class MeshReader : NativeHandle
    {
        public MeshReader(string path) { Handle = meshreader_new(path); }
        public void Dispose() { meshreader_dispose(Handle); }
        public Mesh Read() { return new Mesh(meshreader_read(Handle)); }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr meshreader_new(string path);
        [DllImport("meshtools_shared.dll")]
        static extern void meshreader_dispose(IntPtr meshreader);
        [DllImport("meshtools_shared.dll")]
        static extern IntPtr meshreader_read(IntPtr meshreader);
    }
}
