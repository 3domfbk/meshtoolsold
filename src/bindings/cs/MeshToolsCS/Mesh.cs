﻿using System;
using System.Runtime.InteropServices;

namespace MeshToolsCS
{
    public class Mesh : NativeHandle
    {
        public Mesh() { Handle = mesh_new(); }

        public Mesh(IntPtr intPtr)
        {
            this.Handle = intPtr;
        }

        public void Dispose() { mesh_dispose(Handle); }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr mesh_new();
        [DllImport("meshtools_shared.dll")]
        static extern void mesh_dispose(IntPtr mesh);
    }
}