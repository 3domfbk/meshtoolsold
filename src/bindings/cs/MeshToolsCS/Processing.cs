﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class Processing
    {
        public static PointCloud Merge(List<PointCloud> input)
        {
            IntPtr[] handles = new IntPtr[input.Count];
            for (int i = 0; i < input.Count; i++)
            {
                handles[i] = input[i].Handle;
            }
            return new PointCloud(merge_clouds(handles, handles.Length));
        }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr merge_clouds(IntPtr[] clouds, int size);
    }
}
