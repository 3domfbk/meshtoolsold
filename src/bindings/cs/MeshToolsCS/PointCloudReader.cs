﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class PointCloudReader : NativeHandle
    {
        public PointCloudReader(string path)
        {
            Handle = pointcloudreader_createReader(path, 400000);
        }

        public void Dispose()
        {
            pointcloudreader_dipose(Handle);
        }

        public PointCloud ReadPointCloud()
        {
            return new PointCloud(pointcloudreader_readPointCloud(Handle));
        }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr pointcloudreader_createReader(string path, int bufferSize);
        [DllImport("meshtools_shared.dll")]
        static extern void pointcloudreader_dipose(IntPtr reader);
        [DllImport("meshtools_shared.dll")]
        static extern IntPtr pointcloudreader_readPointCloud(IntPtr reader);
    
    }
}
