﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class PointCloudWriter
    {
        public static PointCloud Write(string path, PointCloud cloud, string format) { return new PointCloud(pointcloudwriter_write(path, cloud.Handle, format)); }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr pointcloudwriter_write(string path, IntPtr cloud, string format);
    }
}
