﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace MeshToolsCS
{
    public class BoundingBox : NativeHandle
    {
        public BoundingBox()
        {
            Handle = bbox_new();
        }

        public BoundingBox(IntPtr intPtr)
        {
            Handle = intPtr;
        }

        public void Dispose()
        {
            bbox_dispose(Handle);
        }

        public float GetCenterX()
        {
            return bbox_getCenterX(Handle);
        }
        public float GetCenterY()
        {
            return bbox_getCenterY(Handle);
        }
        public float GetCenterZ()
        {
            return bbox_getCenterZ(Handle);
        }

        public float Width()
        {
            return bbox_width(Handle);
        }

        public float Height()
        {
            return bbox_height(Handle);
        }

        public float Depth()
        {
            return bbox_depth(Handle);
        }

        public float Volume()
        {
            return bbox_volume(Handle);
        }

        public float Extent()
        {
            return bbox_extent(Handle);
        }

        [DllImport("meshtools_shared.dll")]
        static extern IntPtr bbox_new();
        [DllImport("meshtools_shared.dll")]
        static extern void bbox_dispose(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_getCenterX(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_getCenterY(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_getCenterZ(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_width(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_height(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_depth(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_volume(IntPtr bbox);
        [DllImport("meshtools_shared.dll")]
        static extern float bbox_extent(IntPtr bbox);        
    }
}
