#pragma once
#include <utils/types.h>
#include <datatypes/mesh.h>

using namespace std;

namespace mt {

    /**
     * Utility for reading PLY mesh files
     */
    class MeshReader {
    public:

        /**
         * Creates a reader for a specific file
         */
        MeshReader(string plyFile);

        /**
         * Reads a mesh from a ply file
         */
        Mesh* readMesh();


    private:
        string plyFile;
    };

}