#pragma once
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <io/plyreader.h>
#include <io/pointcloudwriter.h>

namespace mt {

    /**
     * Utility for writing PLY files
     */
    class PlyWriter : public PointCloudWriter {
    public:

        /**
         * Writes a point cloud to a ply file
         */
        bool writePointCloud(string path, PointCloud* cloud, PcFormat format);


    private:
        void writeHeader(PointCloud* cloud, PcFormat format);
        void writeBinary(PointCloud* cloud);
        void writeASCII(PointCloud* cloud);

        // Params
        FILE* file;
    };

}