#include <io/plyreader.h>
#include <utils/stringutils.h>
#include <utils/fileutils.h>
#include <utils/byteutils.h>
#include <stdlib.h>
#include <string.h>

using namespace mt;

int PlyReader::startRead(){
    this->plyFile = fopen(path.c_str(), "rb");
    this->xoff = -1;
    this->roff = -1;
    this->rpos = -1;
    this->goff = -1;
    this->gpos = -1;
    this->boff = -1;
    this->bpos = -1;
    this->nxoff = -1;
    this->nxpos = -1;
    this->nyoff = -1;
    this->nypos = -1;
    this->nzoff = -1;
    this->nzpos = -1;
    this->format = (PcFormat) - 1;
    this->curpos = 0;
    this->stripeBytes = 0;
    this->bytesRead = 0;
    this->cur = 0;
    this->buffer = NULL;

    // Calculate file size
    fseek(plyFile, 0, SEEK_END); // Jump to the end of the file
    fileSize = ftell(plyFile); // Get the current byte offset in the file
    rewind(plyFile); // Jump back to the beginning of the file

    readHeader();

    // If binary, allocate buffer
    if( format == PF_LIL_ENDIAN || format == PF_BIG_ENDIAN ){
        buffer = new byte[bufferSize * stripeBytes];
    }

    return vertices;
}

void PlyReader::endRead(){
    fclose(plyFile);
    delete buffer;
}

bool PlyReader::readPoints(PointCloud* cloud){
    // Read data
    switch (format) {
        case PF_ASCII:
            return readASCII(cloud);
        case PF_LIL_ENDIAN:
        case PF_BIG_ENDIAN:
            return readBinary(cloud);
        default:
            return NULL;
    }
}

void PlyReader::readHeader() {
    // Read header
    bool readingVertices = false;
    char line[1024];

    while (true) {
        bytesRead += readLine(plyFile, line);

        // End of header
        if (strEquals(line, "end_header")) break;

        // Format
        if (strStartsWith(line, "format ascii")) {
            format = PF_ASCII;
        } else if (strStartsWith(line, "format binary_little_endian")) {
            format = PF_LIL_ENDIAN;
        } else if (strStartsWith(line, "format binary_big_endian")) {
            format = PF_BIG_ENDIAN;
        }
            // Vertices
        else if (strStartsWith(line, "element vertex ")) {
            vertices = atoi(line + 15);
            readingVertices = true;
        } else if (strStartsWith(line, "element ")) {
            readingVertices = false;
        } else if (strStartsWith(line, "property ")) {
            // Parse property
            if (strStartsWith(line + 9, "float x")) {
                xpos = curpos;
                curpos++;
                xoff = stripeBytes;
                stripeBytes += 4;
                hasDoublePrecision = false;
            } else if (strStartsWith(line + 9, "float y")) {
                ypos = curpos;
                curpos++;
                yoff = stripeBytes;
                stripeBytes += 4;
            } else if (strStartsWith(line + 9, "float z")) {
                zpos = curpos;
                curpos++;
                zoff = stripeBytes;
                stripeBytes += 4;
            } else if (strStartsWith(line + 9, "double x")) {
                xpos = curpos;
                curpos++;
                xoff = stripeBytes;
                stripeBytes += 8;
                hasDoublePrecision = true;
            } else if (strStartsWith(line + 9, "double y")) {
                ypos = curpos;
                curpos++;
                yoff = stripeBytes;
                stripeBytes += 8;
            } else if (strStartsWith(line + 9, "double z")) {
                zpos = curpos;
                curpos++;
                zoff = stripeBytes;
                stripeBytes += 8;
            } else if (strStartsWith(line + 9, "uchar red") || strStartsWith(line + 9, "uchar diffuse_red")) {
                rpos = curpos;
                curpos++;
                roff = stripeBytes;
                stripeBytes += 1;
            } else if (strStartsWith(line + 9, "uchar green") || strStartsWith(line + 9, "uchar diffuse_green")) {
                gpos = curpos;
                curpos++;
                goff = stripeBytes;
                stripeBytes += 1;
            } else if (strStartsWith(line + 9, "uchar blue") || strStartsWith(line + 9, "uchar diffuse_blue")) {
                bpos = curpos;
                curpos++;
                boff = stripeBytes;
                stripeBytes += 1;
            } else if (strStartsWith(line + 9, "float nx")) {
                nxpos = curpos;
                curpos++;
                nxoff = stripeBytes;
                stripeBytes += 4;
            } else if (strStartsWith(line + 9, "float ny")) {
                nypos = curpos;
                curpos++;
                nyoff = stripeBytes;
                stripeBytes += 4;
            } else if (strStartsWith(line + 9, "float nz")) {
                nzpos = curpos;
                curpos++;
                nzoff = stripeBytes;
                stripeBytes += 4;
            }

            // Unsupported stuff
            else if (strStartsWith(line + 9, "float ")) {
                curpos++;
                stripeBytes += 4;
            } else if (strStartsWith(line + 9, "uchar ")) {
                curpos++;
                stripeBytes += 1;
            } else if (strStartsWith(line + 9, "double ")) {
                curpos++;
                stripeBytes += 8;
            } else if (strStartsWith(line + 9, "int ")) {
                curpos++;
                stripeBytes += 4;
            }
        }
    }
}

bool PlyReader::readBinary(PointCloud* cloud) {
    // Read buffer
    int vread = bufferSize;
    if( cur + bufferSize > vertices ){
        vread = vertices - cur;
    }
    fread(buffer, vread * stripeBytes, 1, plyFile);

    // Parse the binary into the point cloud points
    for (long long i = 0; i < vread; i++) {

        double x = hasDoublePrecision ? readDouble(&buffer[i * stripeBytes + xoff], format == PF_LIL_ENDIAN)
                                      : readFloat(&buffer[i * stripeBytes + xoff], format == PF_LIL_ENDIAN);
        double y = hasDoublePrecision ? readDouble(&buffer[i * stripeBytes + yoff], format == PF_LIL_ENDIAN)
                                      : readFloat(&buffer[i * stripeBytes + yoff], format == PF_LIL_ENDIAN);
        double z = hasDoublePrecision ? readDouble(&buffer[i * stripeBytes + zoff], format == PF_LIL_ENDIAN)
                                      : readFloat(&buffer[i * stripeBytes + zoff], format == PF_LIL_ENDIAN);
        byte r = (roff == -1) ? 255 : buffer[i * stripeBytes + roff];
        byte g = (roff == -1) ? 255 : buffer[i * stripeBytes + goff];
        byte b = (roff == -1) ? 255 : buffer[i * stripeBytes + boff];
        float nx = (nxoff == -1) ? -2.0f : readFloat(&buffer[i * stripeBytes + nxoff], format == PF_LIL_ENDIAN);
        float ny = (nyoff == -1) ? -2.0f : readFloat(&buffer[i * stripeBytes + nyoff], format == PF_LIL_ENDIAN);
        float nz = (nzoff == -1) ? -2.0f : readFloat(&buffer[i * stripeBytes + nzoff], format == PF_LIL_ENDIAN);

        int rgb = BGR(r, g, b);
        cloud->addPoint(x, y, z, rgb, nx, ny, nz); // Read binary, normals are not supported for now
    }

    cur += vread;
    return cur >= vertices;
}

bool PlyReader::readASCII(PointCloud* cloud) {
    // Can't think of a way to buffer this.
    // So fuck it
    char* text = new char[fileSize - bytesRead];
    char* deleteme = text;
    fread(text, fileSize - bytesRead, 1, plyFile);

    double x, y, z;
    float nx = -2, ny = -2, nz = -2;

    byte r = 255, g = 255, b = 255;
    // Parse the ascii!
    char* line = strSep(&text, "\n");
    while (line != NULL) {
        char* it = line;
        int p = 0;
        while (*it!='\n' && *it!='\0') {
            while(*it==' ' || *it=='\t') it++;
            if (p == xpos) x = (double) atof(it);
            else if (p == ypos) y = (double) atof(it);
            else if (p == zpos) z = (double) atof(it);
            else if (p == rpos) r = atoi(it);
            else if (p == gpos) g = atoi(it);
            else if (p == bpos) b = atoi(it);
            else if (p == nxpos) nx = atof(it);
            else if (p == nypos) ny = atof(it);
            else if (p == nzpos) nz = atof(it);

            p++;
            while(*it!=' ' && *it!='\t' && *it!='\n' && *it!='\0') it++;
        }

        cloud->addPoint(x, y, z, BGR(r, g, b), nx, ny, nz);
        line = strSep(&text, "\n");

        if( cloud->getNumPoints() == vertices ) break;
    }

    delete[] deleteme;
    return true;
}
