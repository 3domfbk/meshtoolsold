#include <io/plywriter.h>
#include <utils/stringutils.h>
#include <utils/fileutils.h>
#include <utils/byteutils.h>
#include <stdlib.h>

using namespace mt;

bool PlyWriter::writePointCloud(string path, PointCloud* cloud, PcFormat format) {
    file = fopen(path.c_str(), "wb");
    writeHeader(cloud, format);

    // Read data
    switch (format) {
        case PF_ASCII:
            writeASCII(cloud);
            break;
        case PF_LIL_ENDIAN:
        case PF_BIG_ENDIAN:
            writeBinary(cloud);
            break;
    }

    fclose(file);
    return true;
}

void PlyWriter::writeHeader(PointCloud* cloud, PcFormat format) {
    fprintf(file, "ply\n");
    fprintf(file, "format ");
    switch (format) {
        case PF_ASCII: fprintf(file, "ascii 1.0\n");
            break;
        case PF_LIL_ENDIAN: fprintf(file, "binary_little_endian 1.0\n");
            break;
        case PF_BIG_ENDIAN: fprintf(file, "binary_big_endian 1.0\n");
            break;
    }
    fprintf(file, "element vertex %llu\n", cloud->getNumPoints());

    if (cloud->getShiftX() > 0 || cloud->getShiftY() > 0 || cloud->getShiftZ() > 0) {
        fprintf(file, "property double x\n");
        fprintf(file, "property double y\n");
        fprintf(file, "property double z\n");
    } else {
        fprintf(file, "property float x\n");
        fprintf(file, "property float y\n");
        fprintf(file, "property float z\n");
    }

    if (cloud->hasNormals()) {
        fprintf(file, "property float nx\n");
        fprintf(file, "property float ny\n");
        fprintf(file, "property float nz\n");
    }

    fprintf(file, "property uchar red\n");
    fprintf(file, "property uchar green\n");
    fprintf(file, "property uchar blue\n");
    fprintf(file, "end_header\n");
}

void PlyWriter::writeBinary(PointCloud* cloud) {
    int stripBytes = (cloud->getShiftX() > 0 || cloud->getShiftY() > 0 || cloud->getShiftZ() > 0) ? 27 : 15;
    if (cloud->hasNormals()) stripBytes += 12;

    byte* data = new byte[cloud->getNumPoints() * stripBytes];

    // Create byte data
    int i = 0;
    for (auto p = cloud->getData()->begin(); p != NULL; p = p->next) {
        double x = p->data.x + cloud->getShiftX();
        double y = p->data.y + cloud->getShiftY();
        double z = p->data.z + cloud->getShiftZ();
        double nx = p->data.nx;
        double ny = p->data.ny;
        double nz = p->data.nz;

        int rgb = p->data.rgb;

        int pos = 0;
        int posSize = (cloud->getShiftX() > 0 || cloud->getShiftY() > 0 || cloud->getShiftZ() > 0) ? 8 : 4;
        if (posSize == 8) {
            writeDouble(x, &data[i * stripBytes + pos]); pos += posSize;
            writeDouble(y, &data[i * stripBytes + pos]); pos += posSize;
            writeDouble(z, &data[i * stripBytes + pos]); pos += posSize;
        }
        else {
            writeFloat(x, &data[i * stripBytes + pos]); pos += posSize;
            writeFloat(y, &data[i * stripBytes + pos]); pos += posSize;
            writeFloat(z, &data[i * stripBytes + pos]); pos += posSize;
        }
        if (cloud->hasNormals()) {
            writeFloat(nx, &data[i * stripBytes + pos]); pos += 4;
            writeFloat(ny, &data[i * stripBytes + pos]); pos += 4;
            writeFloat(nz, &data[i * stripBytes + pos]); pos += 4;
        }

        data[i * stripBytes + pos] = RED(rgb); pos++;
        data[i * stripBytes + pos] = GREEN(rgb); pos++;
        data[i * stripBytes + pos] = BLUE(rgb); pos++;
        i++;
    }

    // Write to file
    fwrite(data, cloud->getNumPoints() * stripBytes, 1, file);
    delete[] data;
}

void PlyWriter::writeASCII(PointCloud* cloud) {
    for (auto p = cloud->getData()->begin(); p != NULL; p = p->next) {
        double x = p->data.x + cloud->getShiftX();
        double y = p->data.y + cloud->getShiftY();
        double z = p->data.z + cloud->getShiftZ();
        float nx = p->data.nx;
        float ny = p->data.ny;
        float nz = p->data.nz;
        byte r = RED(p->data.rgb);
        byte g = GREEN(p->data.rgb);
        byte b = BLUE(p->data.rgb);

        if (cloud->hasNormals()) {
            fprintf(file, "%f %f %f %f %f %f %d %d %d\n", x, y, z, nx, ny, nz, r, g, b);
        }
        else {
            fprintf(file, "%f %f %f %d %d %d\n", x, y, z, r, g, b);
        }
    }
}
