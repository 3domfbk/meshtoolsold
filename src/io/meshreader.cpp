#include <MeshtoolsConfig.h>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <io/meshreader.h>

using namespace mt;
using namespace OpenMesh::IO;

MeshReader::MeshReader(string plyFile) {
    this->plyFile = plyFile;
}

Mesh* MeshReader::readMesh() {
    Mesh* mesh = new Mesh();

    Options opt = Options::Default;
    opt += Options::VertexColor;
    mesh->request_vertex_normals();
    mesh->request_face_normals();
    mesh->request_vertex_status();
    mesh->request_halfedge_status();
    mesh->request_halfedge_normals();
    mesh->request_face_status();
    mesh->request_edge_status();
    mesh->request_vertex_colors();
    mesh->request_face_colors();    
    read_mesh(*mesh, plyFile, opt);

    // Calculate indices, bbox, etc
    mesh->update();
    return mesh;
}