#pragma once
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <io/pointcloudwriter.h>

namespace mt {

    /**
     * Utility for reading PLY files
     */
    class PlyReader : public PointCloudReader {
    protected:
        int startRead();
        void endRead();
        bool readPoints(PointCloud* cloud);
        
    private:
        
        void readHeader();
        bool readBinary(PointCloud* cloud);
        bool readASCII(PointCloud* cloud);
        
        // Stream point
        int cur;
        
        // Buffer
        byte* buffer;

        // Header
        FILE* plyFile;
        PcFormat format;
        int xoff, yoff, zoff, roff, goff, boff, nxoff, nyoff, nzoff;
        int xpos, ypos, zpos, rpos, gpos, bpos, nxpos, nypos, nzpos;
        int vertices;
        int stripeBytes;
        int curpos;
        bool hasDoublePrecision;

        // Stream info
        int fileSize;
        int bytesRead;
    };

}
