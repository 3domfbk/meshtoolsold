#include <MeshtoolsConfig.h>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <io/meshwriter.h>

using namespace mt;
using namespace OpenMesh::IO;

MeshWriter::MeshWriter(string plyFile) {
    this->plyFile = plyFile;
}

void MeshWriter::writeMesh(Mesh* mesh, bool binary, bool colors) {
    
    Options opt = Options::Default;
    
    if (binary) {
        opt += Options::Binary;
    }
    if (colors) {
        opt += Options::VertexColor;
    }

    write_mesh(*mesh, plyFile, opt);
}