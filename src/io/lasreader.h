#pragma once
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <io/pointcloudreader.h>

#include "lasreader.hpp"

namespace mt {

    /**
     * Utility for reading LAS files
     */
    class LasReader : public PointCloudReader {
    protected:
        int startRead();
        void endRead();
        bool readPoints(PointCloud* cloud);
        
    private:
        LASreader* reader;
    };

}