#include <io/pointcloudreader.h>
#include <io/plyreader.h>
#include <io/lasreader.h>
#include <utils/stringutils.h>
#include <stdlib.h>
#include <string.h>

using namespace mt;

PointCloudReader* PointCloudReader::createReader(string path, int bufferSize) {
    PointCloudReader* reader = NULL;

    // Switch extension
    if (strEndsWith(path.c_str(), ".ply")) {
        reader = new PlyReader();
    } else if (
            strEndsWith((char*) path.c_str(), ".las") ||
            strEndsWith((char*) path.c_str(), ".laz")) {
        reader = new LasReader();
    }

    reader->path = path;
    reader->bufferSize = bufferSize;
    return reader;
}

PointCloud* PointCloudReader::readPointCloud() {
    int points = startRead();
    PointCloud* cloud = new PointCloud(points);
    
    // Read all points at once
    while(!readPoints(cloud));
    endRead();
    
    cloud->update();
    return cloud;
}