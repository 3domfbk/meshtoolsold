#include <io/pointcloudwriter.h>
#include <io/plywriter.h>
#include <io/xyzwriter.h>
#include <utils/stringutils.h>
#include <stdlib.h>
#include <string.h>

using namespace mt;

bool PointCloudWriter::write(string path, PointCloud* cloud, PcFormat format) {
    PointCloudWriter* writer = NULL;

    // Switch extension
    if (strEndsWith((char*) path.c_str(), ".ply")) {
        writer = new PlyWriter();
    } else if (strEndsWith((char*) path.c_str(), ".xyz")) {
        writer = new XyzWriter();
    }

    if (writer == NULL) return false;
    return writer->writePointCloud(path, cloud, format);
}