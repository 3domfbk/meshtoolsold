#pragma once
#include <utils/types.h>
#include <datatypes/mesh.h>

using namespace std;

namespace mt {

    /**
     * Utility for writing PLY mesh files
     */
    class MeshWriter {
    public:

        /**
         * Creates a reader for a specific file
         */
        MeshWriter(string plyFile);

        /**
         * Writes a mesh to a ply file
         */
        void writeMesh(Mesh* mesh, bool binary = false, bool colors = false);


    private:
        string plyFile;
    };

}