#include <io/lasreader.h>
#include <utils/stringutils.h>
#include <utils/fileutils.h>
#include <utils/byteutils.h>
#include <stdlib.h>
#include <string.h>

using namespace mt;

int LasReader::startRead(){
    LASreadOpener opener;
    opener.set_file_name(path.c_str());
    reader = opener.open();
    
    return reader->npoints;
}

void LasReader::endRead(){
    reader->close();
}

bool LasReader::readPoints(PointCloud* cloud){
    for (int i=0; i<bufferSize; i++) {
        if( !reader->read_point() ) return true;
        int rgb = BGR((reader->point.get_R() >> 8), (reader->point.get_G() >> 8), (reader->point.get_B() >> 8));
        double x = reader->point.get_x();
        double y = reader->point.get_y();
        double z = reader->point.get_z();

        cloud->addPoint(
                x,
                y,
                z,
                rgb,
                -2.0,
                -2.0,
                -2.0); // To fix with normals
    }
    
    return false;
}
