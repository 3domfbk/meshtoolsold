#pragma once
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>

namespace mt {

    /**
     * Utility for reading PLY files
     */
    class PointCloudReader {         
    protected:
        string path;
        int bufferSize;
        
    public:
        /**
         * Create a suitable reader for the specified point cloud file
         * @param path The path to the point cloud file
         * @return A suitable reader
         */
        static PointCloudReader* createReader(string path, int bufferSize);
        PointCloud* readPointCloud();
        
        /**
         * Begin reading from a data source
         * @param bufferSize points that are read at once
         * @return the number of points in the cloud
         */
        virtual int startRead() = 0;
        
        /**
         * End reading from a data source
         * @param path
         */
        virtual void endRead() = 0;
        
        /**
         * Reads points from a source into the specified cloud
         * @return true-data ended false-more data to be read
         */
        virtual bool readPoints(PointCloud* cloud) = 0;
    };

}