#include <io/xyzwriter.h>
#include <utils/stringutils.h>
#include <utils/fileutils.h>
#include <utils/byteutils.h>
#include <stdlib.h>

using namespace mt;

bool XyzWriter::writePointCloud(string path, PointCloud* cloud, PcFormat format) {
    file = fopen(path.c_str(), "wb");

    for (auto p = cloud->getData()->begin(); p != NULL; p = p->next) {

        double x = p->data.x;
        double y = p->data.y;
        double z = p->data.z;
        int rgb = p->data.rgb;

        x += cloud->getShiftX();
        y += cloud->getShiftY();
        z += cloud->getShiftZ();

        fprintf(file, "%f %f %f %d %d %d\n", x, y, z, RED(rgb), GREEN(rgb), BLUE(rgb));
    }

    fclose(file);
    return true;
}
