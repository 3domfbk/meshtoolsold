#pragma once
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <io/pointcloudwriter.h>

namespace mt {

    /**
     * Utility for writing PLY files
     */
    class XyzWriter : public PointCloudWriter {
    public:

        /**
         * Writes a point cloud to a ply file
         */
        bool writePointCloud(string path, PointCloud* cloud, PcFormat format);

    private:
        FILE* file;
    };

}