/* 
 * File:   lodcreator.cpp
 * Author: lake
 * 
 * Created on December 9, 2016, 4:32 PM
 */

#include <io/lodcreator.h>
#include <io/pointcloudreader.h>
#include <datatypes/pointcloud.h>
#include <datatypes/fsoctree.h>
#include <datatypes/linkedlist.h>
#include <filtering/downsampling.h>
#include <unordered_map>
#include <math.h>
#include <sys/stat.h>
#include <string.h>

#include "utils/byteutils.h"
#include "utils/osutils.h"
#include "pointcloudwriter.h"
#include "utils/fileutils.h"

#define BUFSIZE 400000
#define TARGET_PPT 100000

using namespace mt;

void LodCreator::writeLeaf(Octree::Octant* node, char* path, char* dir){
    // Write ply header
    char ofname[64];
    sprintf(ofname, "%s/%s.ply", dir, path);
    FILE* f = fopen(ofname, "wb");
    fprintf(f, "ply\n");
    fprintf(f, "format binary_little_endian 1.0\n");
    fprintf(f, "element vertex %d\n", node->numPoints);
    fprintf(f, "property float x\n");
    fprintf(f, "property float y\n");
    fprintf(f, "property float z\n");
    fprintf(f, "property uchar red\n");
    fprintf(f, "property uchar green\n");
    fprintf(f, "property uchar blue\n");
    fprintf(f, "end_header\n");
    
    int wrote = 0;
    for( auto it=node->start; wrote<node->numPoints; it=it->next ){
        byte buf[15];
        writeFloat(it->data.x, buf);
        writeFloat(it->data.y, buf+4);
        writeFloat(it->data.z, buf+8);
        buf[12] = RED(it->data.rgb);
        buf[13] = GREEN(it->data.rgb);
        buf[14] = BLUE(it->data.rgb);
        wrote++;

        fwrite(buf, sizeof(buf), 1, f);
    }
    
    fclose(f);
}

void LodCreator::writeBranch(Octree::Octant* node, char* path, char* dir){
    // Write ply header
    char ofname[64];
    sprintf(ofname, "%s/%s.ply", dir, path);
    FILE* f = fopen(ofname, "wb");
    fprintf(f, "ply\n");
    fprintf(f, "format binary_little_endian 1.0\n");
    fprintf(f, "element vertex %d\n", TARGET_PPT);
    fprintf(f, "property float x\n");
    fprintf(f, "property float y\n");
    fprintf(f, "property float z\n");
    fprintf(f, "property uchar red\n");
    fprintf(f, "property uchar green\n");
    fprintf(f, "property uchar blue\n");
    fprintf(f, "end_header\n");
    
    // Pick TARGET_PPT uniformly distributed points (just picks at an interval)
    float step = (float)node->numPoints / TARGET_PPT;
    float pos = 0;
    int lpos = 0;
    int wrote = 0;
    for( auto it=node->start; wrote<TARGET_PPT; pos+=step ){
        // Advance lpos until is equal to pos
        while( lpos < (int)pos ) {
            it = it->next;
            lpos++;
        }
        
        byte buf[15];
        writeFloat(it->data.x, buf);
        writeFloat(it->data.y, buf+4);
        writeFloat(it->data.z, buf+8);
        buf[12] = RED(it->data.rgb);
        buf[13] = GREEN(it->data.rgb);
        buf[14] = BLUE(it->data.rgb);
        wrote++;

        fwrite(buf, sizeof(buf), 1, f);
    }
    
    fclose(f);
}

void LodCreator::writeNode(Octree::Octant* node, int level, char* path, char* dir, FsOctree::Octant* octant){
    if( node->leaf ){
        // Write leaf
        writeLeaf(node, path, dir);
    }
    
    else{
        // Write interior node
        writeBranch(node, path, dir);
        
        // Recursion
        for( int i=0; i<8; i++ ){
            if( node->children[i] != NULL ){
                path[level+1] = '0' + i;
                                
                char ofname[64];
                sprintf(ofname, "%s/%s.ply", dir, path);
                octant->children[i] = new FsOctree::Octant(
                        node->children[i]->center, 
                        node->children[i]->extent, 
                        ofname, 
                        node->children[i]->numPoints,
                        node->children[i]->leaf);
                
                writeNode(node->children[i], level+1, path, dir, octant->children[i]);                
                path[level+1] = '\0';
            }
        }
    }
}

void LodCreator::createLods(const char* path) {
    char ofname[128];
    memset(ofname, 0, 128);
    const char* ext = strrchr(path, '.');
    strncpy(ofname, path, strlen(path) - strlen(ext));
    
    // Create tiles dir
    MKDIR(ofname);

    // Read point cloud at once and jerk off
    PointCloudReader* reader = PointCloudReader::createReader(path, BUFSIZE);
    PointCloud* original = reader->readPointCloud();
    
    // Create octree
    original->buildOctreeNP(TARGET_PPT);
    
    FsOctree* fsoctree = new FsOctree(ofname, original->getBBox(), original->getNumPoints());
    
    // Write nodes
    char nodepath[16];
    memset(nodepath, 0, sizeof(nodepath));
    nodepath[0] = 'R';
    
    int l=0;
    writeNode(original->getOctree()->getRoot(), l, nodepath, ofname, fsoctree->getRoot());
    
    // Write tree structure
    char tfname[128];
    sprintf(tfname, "%s.fst", ofname);
    FILE* fout = fopen(tfname, "w");
    fprintf(fout, "%f %f %f %f %f %f %d\n", 
            fsoctree->getBBox()->bounds[0],
            fsoctree->getBBox()->bounds[1],
            fsoctree->getBBox()->bounds[2],
            fsoctree->getBBox()->bounds[3],
            fsoctree->getBBox()->bounds[4],
            fsoctree->getBBox()->bounds[5],
            fsoctree->getRoot()->numPoints);
    writeNodeSpec(fout, fsoctree->getRoot(), -1);
    fclose(fout);
}

void LodCreator::writeNodeSpec(FILE* f, FsOctree::Octant* octant, int cidx){
    int nchildren = 0;
    for( int c=0; c<8; c++ ){
        if( octant->children[c] != NULL ) nchildren++;
    }
    
    fprintf(f, "%d %f %f %f %f %s %d %d %d\n", 
            cidx,
            octant->center[0],
            octant->center[1],
            octant->center[2],
            octant->extent,
            octant->filename,
            octant->numPoints,
            octant->leaf,
            nchildren);
    
    // recursion
    for( int c=0; c<8; c++ ){
        if( octant->children[c] != NULL ) {
            writeNodeSpec(f, octant->children[c], c);
        }
    }
}

FsOctree* LodCreator::readFsOctree(const char* path){
    FILE* f = fopen(path, "r");
    char line[4096];

    int npoints;
    BBox bbox;
    fscanf(f, "%f %f %f %f %f %f %d\n", 
            &bbox.bounds[0],
            &bbox.bounds[1],
            &bbox.bounds[2],
            &bbox.bounds[3],
            &bbox.bounds[4],
            &bbox.bounds[5],
            &npoints);
    
    char ofname[128];
    memset(ofname, 0, 128);
    const char* ext = strrchr(path, '.');
    strncpy(ofname, path, strlen(path) - strlen(ext));
    
    int cid;
    FsOctree::Octant* root = readNodeSpec(f, cid);
    FsOctree* octree = new FsOctree(ofname, &bbox, root);
        
    fclose(f);
    
    return octree;
}

FsOctree::Octant* LodCreator::readNodeSpec(FILE* f, int &cidx){
    int nchildren;
    FsOctree::Octant* o = new FsOctree::Octant();
    int ci;
    int leaf;
    fscanf(f, "%d %f %f %f %f %s %d %d %d\n", 
            &ci,
            &o->center[0],
            &o->center[1],
            &o->center[2],
            &o->extent,
            o->filename,
            &o->numPoints,
            &leaf,
            &nchildren);
    o->leaf = leaf;
    
    // Recursion
    for( int i=0; i<nchildren; i++ ){
        int cci;
        FsOctree::Octant* child = readNodeSpec(f, cci);
        o->children[cci] = child;
    }
    
    cidx = ci;
    return o;
}