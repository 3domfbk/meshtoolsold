#pragma once
#include <stdio.h>
#include <utils/types.h>
#include <datatypes/pointcloud.h>
#include <io/pointcloudreader.h>

namespace mt {

    // Cloud format
    enum PcFormat {
        PF_ASCII,
        PF_LIL_ENDIAN,
        PF_BIG_ENDIAN
    };

    /**
     * Utility for reading PLY files
     */
    class PointCloudWriter {
    public:
        static bool write(string path, PointCloud* cloud, PcFormat format);

    protected:
        /**
         * Reads a point cloud from a file
         */
        virtual bool writePointCloud(string path, PointCloud* cloud, PcFormat format) = 0;
    };

}