/* 
 * File:   lodcreator.h
 * Author: lake
 *
 * Created on December 9, 2016, 4:32 PM
 */
#pragma once

#include <datatypes/octree.h>
#include <datatypes/fsoctree.h>

namespace mt {
    
    class LodCreator {
    public:

        /**
         * Create FS lods for the specified file
         * @param path Path of huge point cloud file
         */
        static void createLods(const char* path);
        
        /**
         * Read a FsOctree from a file system spec
         * @param path The .fst file
         * @return A loaded FsOctree
         */
        static FsOctree* readFsOctree(const char* path);
        static FsOctree::Octant* readNodeSpec(FILE* f, int &cid);
        
    private:
        static void writeNode(Octree::Octant* node, int level, char* path, char* dir, FsOctree::Octant* octant);
        static void writeBranch(Octree::Octant* node, char* path, char* dir);
        static void writeLeaf(Octree::Octant* node, char* path, char* dir);
        
        static void writeNodeSpec(FILE* f, FsOctree::Octant* octant, int cid);
    };
    
}