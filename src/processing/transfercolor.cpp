#include <processing/transfercolor.h>
#include <datatypes/voxelgrid.h>
#include <utils/types.h>

#include <queue>

using namespace std;

namespace mt {

    // Dist Cell for PC transfer color algorithm

    struct DistCell {
        PointCell* point;
        float distance;

        DistCell(PointCell* pt, float d) {
            this->point = pt;
            this->distance = d;
        }

        friend bool operator<(const DistCell &o1, const DistCell &o2) {
            return o1.distance > o2.distance;
        }
    };

    void transferColor(Mesh* source, Mesh* target) {
        // Create old mesh BSP if needed
        if (source->getBsp() == NULL) source->buildBsp();
        OpenMeshTriangleBSPT< Mesh >* triangleBSP = source->getBsp();

        // Project every new vertex to old mesh
        for (TriMesh_ArrayKernelT<>::VertexIter vx = target->vertices_begin();
                vx != target->vertices_end(); vx++) {
            Vec3f P = target->point(*vx);
            BSPImplT<TriangleBSPCoreT<OpenMeshTriangleBSPTraits < Mesh>>>::NearestNeighbor nn = triangleBSP->nearest(P);

            Mesh::ConstFaceVertexIter fv_it = source->cfv_iter(nn.handle);

            // Assume it's a triangle.
            Vec3f p1 = source->point(*fv_it);
            Vec3uc C1 = source->color(*fv_it);
            fv_it++;
            Vec3f p2 = source->point(*fv_it);
            Vec3uc C2 = source->color(*fv_it);
            fv_it++;
            Vec3f p3 = source->point(*fv_it);
            Vec3uc C3 = source->color(*fv_it);

            // Project and interpolate (magic.)
            Vec3f u = p2 - p1;
            Vec3f v = p3 - p1;
            Vec3f n = u % v; // Cross product
            Vec3f w = P - p1;

            float n2 = n | n; // Dot product
            float gamma = ((u % w) | n) / n2;
            float beta = ((w % v) | n) / n2;
            float alpha = 1 - (gamma + beta);

            int red = (int) (C1[0] * alpha + C2[0] * beta + C3[0] * gamma);
            int green = (int) (C1[1] * alpha + C2[1] * beta + C3[1] * gamma);
            int blue = (int) (C1[2] * alpha + C2[2] * beta + C3[2] * gamma);

            target->set_color(*vx, Vec3uc(red, green, blue));
        }
    }

    void transferColor(PointCloud* source, PointCloud* target, int numNeighs) {
        // Calculate voxel side based on cloud average density
        float l = 10 * numNeighs * source->getAvgPointDistance();
        cout << l << endl;

        // Build voxel grid for source
        VoxelGrid voxelGrid(source, l);

        int tot = target->getData()->size();
        int lastPerc = 0;

        priority_queue<DistCell> queue;
        int p = 0;
        for (auto it = target->getData()->begin(); it != NULL; it = it->next) {
            // Progress
            int perc = int(((float) (p++) / tot) * 100);
            if (perc > lastPerc) {
                lastPerc = perc;
                cout << perc << "% ";
            }

            while (!queue.empty()) queue.pop();

            // Find 3 nearest points in the same voxel, because why not
            Voxel* v = voxelGrid.getVoxel(it->data.x, it->data.y, it->data.z);
            // If this target point is in no source voxel, skip it (no color change)
            if (!v) continue;

            int i = 0;
            for (auto pit = v->start; i < v->numPoints; pit = pit->next, i++) {
                // Distance
                float dx = abs(pit->data->data.x - it->data.x);
                float dy = abs(pit->data->data.y - it->data.y);
                float dz = abs(pit->data->data.z - it->data.z);

                float dsq = dx * dx + dy * dy + dz*dz;

                queue.push(DistCell(pit->data, dsq));
            }

            // Inverse distance weighting (IDW) interpolation
            if (!queue.empty()) {
                float r = 0, g = 0, b = 0;
                float weights = 0;
                for (int i = 0; i < numNeighs && !queue.empty(); i++) {
                    DistCell cell = queue.top();
                    queue.pop();

                    // no 0 distance plz
                    if (cell.distance == 0) {
                        r = (float) RED(cell.point->data.rgb);
                        g = (float) GREEN(cell.point->data.rgb);
                        b = (float) BLUE(cell.point->data.rgb);
                        weights = 1;
                        break;
                    }

                    float w = 1.0f / cell.distance;
                    r += w * RED(cell.point->data.rgb);
                    g += w * GREEN(cell.point->data.rgb);
                    b += w * BLUE(cell.point->data.rgb);
                    weights += w;
                }

                int rb = (int) (r / weights);
                int gb = (int) (g / weights);
                int bb = (int) (b / weights);
                it->data.rgb = BGR(rb, gb, bb);
            }
        }

        cout << endl;
    }
}