#pragma once
#include <datatypes/mesh.h>
#include <datatypes/octree.h>
#include <ACG/Geometry/bsp/TriangleBSPT.hh>

namespace mt {

    /**
     * Transfer vertex color reprojecting target to source mesh
     */
    void transferColor(Mesh* source, Mesh* target);

    /**
     * Transfer vertex color to point cloud
     */
    void transferColor(PointCloud* source, PointCloud* target, int numNeighs);
}