#include <processing/merge.h>
#include <utils/types.h>

namespace mt {

    PointCloud* merge(vector<PointCloud*> input) {
        if (input.size() == 0) return NULL;
        if (input.size() == 1) return new PointCloud(input[0]);

        PointCloud* result = new PointCloud(input[0]->getNumPoints() * (int)input.size());
        for (auto cit = input.begin(); cit != input.end(); cit++) {
            LinkedList<Point>* points = (*cit)->getData();
            for (auto it = points->begin(); it != NULL; it = it->next) {
                result->addPoint(it->data.x + (*cit)->getShiftX(), it->data.y + (*cit)->getShiftY(), it->data.z + (*cit)->getShiftZ(),
                    it->data.rgb, it->data.nx, it->data.ny, it->data.nz);
            }
        }

        result->update();
        return result;
    }
}