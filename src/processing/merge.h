#pragma once
#include <datatypes/pointcloud.h>
#include <vector>

using namespace std;

namespace mt {

    /**
     * Transfer vertex color reprojecting target to source mesh
     */
    PointCloud* merge(vector<PointCloud*> input);
}