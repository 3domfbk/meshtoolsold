#include <stdio.h>
#include <string.h>
#include <utils/stringutils.h>
#include "commands/command.h"
#include "commands/cmd_open.h"
#include "commands/cmd_save.h"
#include "commands/cmd_mopen.h"
#include "commands/cmd_list.h"
#include "commands/cmd_stats.h"
#include "commands/cmd_close.h"
#include "commands/cmd_keep.h"
#include "commands/cmd_select.h"
#include "commands/cmd_unselect.h"
#include "commands/cmd_focus.h"
#include "commands/cmd_rename.h"
#include "commands/cmd_octreelevel.h"
#include "commands/cmd_circle.h"

#include "commands/cmd_downsample.h"
#include "commands/cmd_decimate.h"
#include "commands/cmd_subdivide.h"
#include "commands/cmd_smooth.h"
#include "commands/cmd_remesh.h"
#include "commands/cmd_aremesh.h"
#include "commands/cmd_repair.h"
#include "commands/cmd_nonmanifolds.h"
#include "commands/cmd_holefill.h"
#include "commands/cmd_farea.h"
#include "commands/cmd_fedgelength.h"
#include "commands/cmd_transfercolor.h"
#include "commands/cmd_merge.h"
#include "commands/cmd_islands.h"
#include "commands/cmd_octree.h"
#include "commands/cmd_delete.h"

#include "commands/cmd_makeok.h"
#include "commands/cmd_createlods.h"

#include <MeshtoolsConfig.h>

using namespace mt;

/**
 * Mesh tools cli interface
 */
int main(int argc, char** argv) {
    // Create state
    MtState state;

    // Line buffer
    char line[16384];

    // Listens for commands
    while (true) {
        printf(">");
        fflush(stdout);

        // Stdin closed, exit!
        if (fgets(line, sizeof (line), stdin) == NULL) {
            return 1;
        }

        // Lock viewer
        state.lockViewer();

        // Removes \n
        line[strlen(line) - 1] = '\0';

        // Lookup command
        Command* command = NULL;
        if (strStartsWith(line, "open ")) {
            command = new CmdOpen();
        } else if (strStartsWith(line, "save ")) {
            command = new CmdSave();
        } else if (strStartsWith(line, "mopen ")) {
            command = new CmdMopen();
        } else if (strStartsWith(line, "list")) {
            command = new CmdList();
        } else if (strStartsWith(line, "stats")) {
            command = new CmdStats();
        } else if (strStartsWith(line, "close")) {
            command = new CmdClose();
        } else if (strStartsWith(line, "keep")) {
            command = new CmdKeep();
        } else if (strStartsWith(line, "select")) {
            command = new CmdSelect();
        } else if (strStartsWith(line, "unselect")) {
            command = new CmdUnselect();
        } else if (strStartsWith(line, "rename")) {
            command = new CmdRename();
        } else if (strStartsWith(line, "octreelevel")) {
            command = new CmdOctreeLevel();
        } else if (strStartsWith(line, "focus ")) {
            command = new CmdFocus();
        } else if (strStartsWith(line, "downsample ")) {
            command = new CmdDownsample();
        } else if (strStartsWith(line, "decimate ")) {
            command = new CmdDecimate();
        } else if (strStartsWith(line, "subdivide ")) {
            command = new CmdSubdivide();
        } else if (strStartsWith(line, "smooth ")) {
            command = new CmdSmooth();
        } else if (strStartsWith(line, "remesh ")) {
            command = new CmdRemesh();
        } else if (strStartsWith(line, "aremesh ")) {
            command = new CmdAremesh();
        } else if (strStartsWith(line, "repair ")) {
            command = new CmdRepair();
        } else if (strStartsWith(line, "nonmanifolds")) {
            command = new CmdNonManifolds();
        } else if (strStartsWith(line, "holefill")) {
            command = new CmdHoleFill();
        } else if (strStartsWith(line, "farea")) {
            command = new CmdFarea();
        } else if (strStartsWith(line, "fedgelength")) {
            command = new CmdFedgeLength();
        } else if (strStartsWith(line, "transfercolor")) {
            command = new CmdTransferColor();
        } else if (strStartsWith(line, "merge")) {
            command = new CmdMerge();
        } else if (strStartsWith(line, "islands ")) {
            command = new CmdIslands();
        } else if (strStartsWith(line, "octree ")) {
            command = new CmdOctree();
        } else if (strStartsWith(line, "delete")) {
            command = new CmdDelete();
        } else if (strStartsWith(line, "makeok") || strStartsWith(line, "autoislands")) {
            command = new CmdMakeok();
		} else if (strStartsWith(line, "circle")) {
			command = new CmdCircle();
		} else if (strStartsWith(line, "delete")) {
			command = new CmdDelete();
        } else {
            printf("E: Unkown command.\n");
            fflush(stdout);

            // Unlock viewer
            state.unlockViewer();
            continue;
        }

        // Parse args & execute
        if (command->prepareArgs(line)) {
            command->execute(&state);
        }
        delete command;

        // Garbage collection
        state.garbageCollection();

        // Unlock viewer
        state.unlockViewer();
    }

    return 0;
}
