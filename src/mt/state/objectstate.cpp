#include "objectstate.h"
#include "../viewer/iviewer.h"

#include <string.h>

using namespace mt;

ObjectState::ObjectState(string name) {
    this->viewer = NULL;
    this->selected = true;
    this->name = name;
}

ObjectState::~ObjectState() {
}

void ObjectState::setSelected(bool selected) {
    if (viewer) viewer->setVisible(name, selected);
    this->selected = selected;
}

bool ObjectState::isSelected() {
    return selected;
}

string ObjectState::getName() {
    return name;
}

void ObjectState::setName(string name) {
    this->name = name;
}

void ObjectState::createView(IViewer* viewer) {
    this->viewer = viewer;

    addToViewer();
}

IViewer* ObjectState::getViewer() {
    return viewer;
}