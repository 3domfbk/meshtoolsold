#pragma once

#include <datatypes/circle.h>
#include "objectstate.h"

namespace mt {

    /**
     * An mt pc instance
     */
    class CircleState : public ObjectState {
    public:

        /**
         * Creates a new pointcloud state for a mesh
         */
		CircleState(Circle* circle, string name);

        /**
         * Release owned data
         **/
        ~CircleState();

        // Deletes viewer's selection
        void deleteSelection();

        // Interface
        void invalidate();
        void addToViewer();

        // Accessors
        Circle* getCircle();

    private:
        // PC
        Circle* circle;
    };
}