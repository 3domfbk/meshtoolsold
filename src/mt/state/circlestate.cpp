#include "circlestate.h"
#include "../viewer/iviewer.h"

using namespace mt;

CircleState::CircleState(Circle* circle, string name) : ObjectState(name) {
    this->circle = circle;
}

CircleState::~CircleState() {
    delete circle;
}

Circle* CircleState::getCircle() {
    return circle;
}

// Interface

void CircleState::invalidate() {
    // Refresh view
    if (viewer) viewer->invalidateObject(name);
}

void CircleState::addToViewer() {
    if (viewer) {
		viewer->addCircle(name, circle);
    }
}

void CircleState::deleteSelection() {
    if (viewer) viewer->deleteSelection(name);
}