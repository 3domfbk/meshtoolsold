#pragma once

#include <datatypes/pointcloud.h>
#include <datatypes/fsoctree.h>
#include "objectstate.h"

namespace mt {

    /**
     * An mt pc instance
     */
    class PointCloudState : public ObjectState {
    public:

        /**
         * Creates a new pointcloud state for a mesh
         */
        PointCloudState(PointCloud* cloud, FsOctree* octree, string name);

        /**
         * Release owned data
         **/
        ~PointCloudState();

        // Deletes viewer's selection
        void deleteSelection();

        // Interface
        void invalidate();
        void addToViewer();

        // Accessors
        PointCloud* getPointCloud();

    private:
        // PC
        PointCloud* pointCloud;
        // FsOctree
        FsOctree* octree;
    };
}