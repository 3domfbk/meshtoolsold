#pragma once

#include <string>

using namespace std;

namespace mt {

    // FWD
    class IViewer;

    /**
     * An mt object instance
     */
    class ObjectState {
    public:
        /**
         * Release owned data
         **/
        virtual ~ObjectState();

        /**
         * Adds to the viewer a view of
         * this object
         **/
        void createView(IViewer* viewer);

    protected:
        /**
         * Creates a new object state
         */
        ObjectState(string name);

        /**
         * Adds the view to the viewer
         **/
        virtual void addToViewer() = 0;

    public:
        // Accessors
        void setSelected(bool selected);
        bool isSelected();
        string getName();
        IViewer* getViewer();
        void setName(string name);

        // Interface
        virtual void invalidate() = 0;

    protected:
        // State
        IViewer* viewer;

        // Unique name for viewer
        string name;

        // Selected or not
        bool selected;
    };
}