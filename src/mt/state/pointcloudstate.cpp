#include "pointcloudstate.h"
#include "../viewer/iviewer.h"

using namespace mt;

PointCloudState::PointCloudState(PointCloud* cloud, FsOctree* octree, string name) : ObjectState(name) {
    this->pointCloud = cloud;
    this->octree = octree;
}

PointCloudState::~PointCloudState() {
    delete pointCloud;
    delete octree;
}

PointCloud* PointCloudState::getPointCloud() {
    return pointCloud;
}

// Interface

void PointCloudState::invalidate() {
    // Refresh view
    if (viewer) viewer->invalidateObject(name);
}

void PointCloudState::addToViewer() {
    if (viewer) {
        if( octree != NULL ){
            viewer->addFsOctree(name, pointCloud, octree);
        } else {
            viewer->addPointCloud(name, pointCloud);
        }
    }
}

void PointCloudState::deleteSelection() {
    if (viewer) viewer->deleteSelection(name);
}