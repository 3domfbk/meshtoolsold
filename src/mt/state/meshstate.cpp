#include "meshstate.h"
#include "../viewer/iviewer.h"

using namespace mt;

MeshState::MeshState(Mesh* mesh, string name) : ObjectState(name) {
    this->mesh = mesh;
}

MeshState::~MeshState() {
    delete mesh;
}

Mesh* MeshState::getMesh() {
    return mesh;
}

// Interface

void MeshState::invalidate() {
    // Refresh view
    if (viewer) viewer->invalidateObject(name);
}

void MeshState::addToViewer() {
    if (viewer) viewer->addMesh(name, mesh);
}