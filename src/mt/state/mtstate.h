#pragma once
#include "../viewer/iviewer.h"
#include "meshstate.h"
#include "pointcloudstate.h"
#include "circlestate.h"

#include <unordered_map>

using namespace std;

namespace mt {

    /**
     * Shared global state
     */
    class MtState {
    public:

        MtState();

        /**
         * Adds a point cloud
         **/
        void addObject(ObjectState* object);

        /**
         * Removes and deletes object
         **/
        void deleteObject(string name);

        /**
         * Renames an object
         **/
        void renameObject(string oldName, string newName);

        /**
         * Obtains an object
         **/
        template<class T = ObjectState*> T getObject(string name) {
            if (objects.count(name) == 0) return NULL;
            return objects[name];
        }

        /**
         * Gets object map
         **/
        unordered_map<string, ObjectState*>* getObjects();

        /**
         * Delete objects marked for deletion
         **/
        void garbageCollection();

        /**
         * Sets no object as selected
         **/
        void clearSelection();

        /**
         * Sets all object as selected
         **/
        void fillSelection();

        /**
         * Focus view on specified object if existing
         **/
        void focusToObject(string name);


        // Viewer locking
        void lockViewer();
        void unlockViewer();

    private:
        unordered_map<string, ObjectState*> objects;
        vector<ObjectState*> toDelete;

        // Optional viewer
        IViewer* viewer;
    };
}