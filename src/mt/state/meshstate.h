#pragma once

#include <datatypes/mesh.h>
#include "objectstate.h"

namespace mt {

    /**
     * An mt mesh instance
     */
    class MeshState : public ObjectState {
    public:

        /**
         * Creates a new mesh state for a mesh
         */
        MeshState(Mesh* mesh, string name);

        /**
         * Release owned data
         **/
        ~MeshState();

        // Interface
        void invalidate();
        void addToViewer();

        // Accessors
        Mesh* getMesh();

    private:
        // Mesh
        Mesh* mesh;
    };
}