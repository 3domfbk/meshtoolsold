#include "mtstate.h"
#include <string.h>
#include <stdlib.h>
#include "../viewer/viewerfactory.h"

namespace mt {

    MtState::MtState() {
        // Init Viewer
        viewer = ViewerFactory::createViewer();
        if (viewer != NULL) viewer->initialize();
    }

    void MtState::addObject(ObjectState* object) {
        // Overwrites previous
        deleteObject(object->getName());

        // Register object
        objects[object->getName()] = object;

        // Sets viewer
        if (viewer != NULL) {
            object->createView(viewer);
        }
    }

    void MtState::deleteObject(string name) {
        if (viewer != NULL) viewer->removeObject(name);

        if (objects.count(name) > 0) {
            toDelete.push_back(objects[name]);
        }
    }

    void MtState::renameObject(string oldName, string newName) {
        ObjectState* object = getObject(oldName);
        if (object == NULL) return;

        if (viewer != NULL) viewer->removeObject(oldName);
        objects.erase(oldName);
        object->setName(newName);

        objects[object->getName()] = object;
        if (viewer != NULL) {
            object->createView(viewer);
        }
    }

    unordered_map<string, ObjectState*>* MtState::getObjects() {
        return &objects;
    }

    void MtState::garbageCollection() {
        for (int i = 0; i < toDelete.size(); i++) {
            // Erase only if pointer is the same (I.E. it has not been overwritten)
            string name = toDelete[i]->getName();
            if (objects.count(name) > 0 && objects[name] == toDelete[i]) {
                objects.erase(toDelete[i]->getName());
            }

            delete toDelete[i];
        }

        toDelete.clear();
    }

    void MtState::clearSelection() {
        auto it = objects.cbegin();
        for (; it != objects.cend(); ++it) {
            it->second->setSelected(false);
        }
    }

    void MtState::fillSelection() {
        auto it = objects.cbegin();
        for (; it != objects.cend(); ++it) {
            it->second->setSelected(true);
        }
    }

    void MtState::focusToObject(string name) {
        if (viewer) {
            viewer->focusToObject(name);
        }
    }

    void MtState::lockViewer() {
        if (viewer) viewer->lock();
    }

    void MtState::unlockViewer() {
        if (viewer) viewer->unlock();
    }
}