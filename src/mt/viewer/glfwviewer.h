#pragma once
#ifdef _WIN32
#include <windows.h>
#endif

#include <GL/glew.h>
#define GLFW_INCLUDE_GLU
#include <GLFW/glfw3.h>
#include "iviewer.h"
#include <visualization/viewer.h>
#include <utils/threadutils.h>

namespace mt {

    /**
     * Viewer based on GLFW
     */
    class GLFWViewer : public IViewer {
    public:
        // Override
        void initialize();
        void addPointCloud(string name, PointCloud* pointCloud);
        void addFsOctree(string name, PointCloud* pointCloud, FsOctree* octree);
        void addMesh(string name, Mesh* mesh);
		void addCircle(string name, Circle* mesh);
        void invalidateObject(string name);
        void removeObject(string name);
        void setVisible(string name, bool visible);
        void focusToObject(string name);
        void deleteSelection(string name);
        void setOctreeLevel(string name, int level);
        void lock();
        void unlock();

    private:
        Viewer viewer;

        // The window
        GLFWwindow* window;
    };
}