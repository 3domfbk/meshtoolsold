#include "glfwviewer.h"
#include <string.h>
#include <stdlib.h>

using namespace mt;

// Viewer global handle
Viewer* gViewer;
double posx, posy;

static void error_callback(int error, const char* description) {
    fprintf(stderr, "Error: %s\n", description);
}

// Mouse callbacks

static void cursor_pos_callback(GLFWwindow* window, double xpos, double ypos) {
    posx = xpos;
    posy = ypos;
    gViewer->onMouseMoved(xpos, ypos);
}

static void mouse_button_callback(GLFWwindow* window, int button, int action, int mods) {
    switch (action) {
        case 1: gViewer->onMousePressed((ViewerMouseBtn) button, posx, posy);
            break; // PRESS
        case 0: gViewer->onMouseReleased((ViewerMouseBtn) button, posx, posy);
            break; // RELEASE
        default: break;
    }
}

static void scroll_callback(GLFWwindow* window, double xoffset, double yoffset) {
    gViewer->onMouseWheelScrolled(xoffset, yoffset);
}

static void resize_callback(GLFWwindow* window, int width, int height) {
    gViewer->onResize(width, height);
}

// Function to run on other thread

THREADFUNC(show_window) {

    int w = 640;
    int h = 480;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);

    // The window
    GLFWwindow* window = glfwCreateWindow(w, h, "PCMEditor", NULL, NULL);

    if (!window) {
        cerr << "Cannot initialize GLFW window" << endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetCursorPosCallback(window, cursor_pos_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetScrollCallback(window, scroll_callback);
    glfwSetWindowSizeCallback(window, resize_callback);

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    gViewer->initialize(w, h);

    while (!glfwWindowShouldClose(window)) {
        // Render
        gViewer->lock();
        gViewer->renderFrame();
        gViewer->unlock();

        // Commit rendering
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    return 0;
}

void GLFWViewer::initialize() {
    // Initialize GLFW
    if (!glfwInit()) {
        cerr << "Cannot initialize GLFW" << endl;
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    gViewer = &viewer;

    // Start viewer thread
    THREAD thread;
    THREADID id;
    CREATE_THREAD(thread, show_window, NULL, id);
}

void GLFWViewer::addPointCloud(string name, PointCloud* pointCloud) {
    viewer.addPointCloud(name, pointCloud);
}

void GLFWViewer::addFsOctree(string name, PointCloud* pointCloud, FsOctree* octree) {
    viewer.addFsOctree(name, pointCloud, octree);
}

void GLFWViewer::addMesh(string name, Mesh* mesh) {
    viewer.addMesh(name, mesh);
}

void GLFWViewer::addCircle(string name, Circle* circle) {
	viewer.addCircle(name, circle);
}

void GLFWViewer::invalidateObject(string name) {
    viewer.invalidateObject(name);
}

void GLFWViewer::removeObject(string name) {
    viewer.removeObject(name);
}

void GLFWViewer::setVisible(string name, bool visible) {
    viewer.setVisible(name, visible);
}

void GLFWViewer::focusToObject(string name) {
    viewer.focusToObject(name);
}

void GLFWViewer::deleteSelection(string name) {
    viewer.deleteSelection(name);
}

void GLFWViewer::setOctreeLevel(string name, int level) {
    viewer.setOctreeLevel(name, level);
}

void GLFWViewer::lock() {
    viewer.lock();
}

void GLFWViewer::unlock() {
    viewer.unlock();
}
