#pragma once

#include <string>

using namespace std;

namespace mt {

    // Fwd
    class PointCloud;
    class FsOctree;
    class Mesh;
	class Circle;

    /**
     * Viewer interface
     */
    class IViewer {
    public:
        virtual void initialize() = 0;
        virtual void addPointCloud(string name, PointCloud* pointCloud) = 0;
        virtual void addFsOctree(string name, PointCloud* pointCloud, FsOctree* octree) = 0;
        virtual void addMesh(string name, Mesh* mesh) = 0;
		virtual void addCircle(string name, Circle* mesh) = 0;
        virtual void invalidateObject(string name) = 0;
        virtual void removeObject(string name) = 0;
        virtual void setVisible(string name, bool visible) = 0;
        virtual void focusToObject(string name) = 0;
        virtual void deleteSelection(string name) = 0;
        virtual void setOctreeLevel(string name, int level) = 0;
        virtual void lock() = 0;
        virtual void unlock() = 0;
    };
}