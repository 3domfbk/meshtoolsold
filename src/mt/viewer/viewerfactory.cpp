#include "viewerfactory.h"
#include <MeshtoolsConfig.h>
#include <stdio.h>

#ifdef BUILD_VIEWER
#include "glfwviewer.h"
#endif

using namespace mt;

IViewer* ViewerFactory::createViewer() {
#ifdef BUILD_VIEWER
    return new GLFWViewer();
#else
    return NULL;
#endif
}