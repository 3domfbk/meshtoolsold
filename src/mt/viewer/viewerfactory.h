#pragma once
#include "iviewer.h"

namespace mt {

    /**
     * Viewer factory
     */
    class ViewerFactory {
    public:
        static IViewer* createViewer();
    };
}