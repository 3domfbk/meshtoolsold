#include "cmd_stats.h"
#include "../state/meshstate.h"
#include "../state/pointcloudstate.h"
#include <datatypes/octree.h>

#include <iomanip>

namespace mt {

    bool CmdStats::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdStats::execute(MtState* state) {
        // List all objects!
        for (auto it = state->getObjects()->cbegin(); it != state->getObjects()->cend(); it++) {
            cout << left << setw(20) << it->first << ": ";

            // Object type
            // -- Mesh
            MeshState* mesh = dynamic_cast<MeshState*> (it->second);
            if (mesh) {
                cout << "(ME) ";

                // Density
                float d = mesh->getMesh()->getPointDensity();
                float fedgeLength = 3.0f / sqrtf(d);
                cout << "d=" << d;
                cout << " fel=" << fedgeLength;
            }

            // -- Pc
            PointCloudState* cloud = dynamic_cast<PointCloudState*> (it->second);
            if (cloud) {
                cout << "(PC) ";

                // Density
                if (cloud->getPointCloud()->getOctree()) {
                    float d = cloud->getPointCloud()->getOctree()->getDensity();
                    cout << "od=" << d;
                    cout << " pd=" << cloud->getPointCloud()->getAvgPointDistance();
                }
            }

            cout << endl;
        }
    }
}