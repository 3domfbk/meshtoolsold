#pragma once
#include "batchcommand.h"
#include <stdio.h>

namespace mt {

    class CmdIslands : public BatchCommand {
    public:
        // Override
        void execute(MeshState* mesh);
        void execute(PointCloudState* pointCloud);
        void postProcess();

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

    private:
        float maxDistance;
        vector<PointCloud*> result;
        vector<string> names;
        vector<Mesh*> mresult;
        vector<string> mnames;
        vector<string> parentNames;
    };
}