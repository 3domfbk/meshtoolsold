#include "cmd_delete.h"
#include <datatypes/octree.h>
#include <string.h>

namespace mt {

    bool CmdDelete::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdDelete::execute(MeshState* state) {
        // Not supported
    }

    void CmdDelete::execute(PointCloudState* state) {
        // Delete selection
        state->deleteSelection();
        state->invalidate();
    }
}