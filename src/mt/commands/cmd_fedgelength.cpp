#include "cmd_fedgelength.h"
#include <filtering/fedgelength.h>
#include <string.h>

namespace mt {

    bool CmdFedgeLength::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        minLengh = (float) atof(argv[1]);
        if (minLengh == 0.0f) return false;
        return true;
    }

    void CmdFedgeLength::execute(MeshState* state) {
        fedgelength(state->getMesh(), minLengh);
        state->invalidate();
    }

    void CmdFedgeLength::execute(PointCloudState* state) {
        // Not possible
    }
}