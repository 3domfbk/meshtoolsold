#pragma once
#include "command.h"
#include <stdio.h>

namespace mt {

    class CmdStats : public Command {
    public:
        // Override
        void execute(MtState* state);

    protected:
        // Override
        bool parseArgs(int argc, char** argv);
    };
}