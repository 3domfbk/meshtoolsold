#include "cmd_decimate.h"
#include <filtering/decimate.h>
#include <string.h>

namespace mt {

    bool CmdDecimate::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        targetVertices = atoi(argv[1]);
        if (targetVertices == 0) return false;

        return true;
    }

    void CmdDecimate::execute(MeshState* state) {
        // Decimate the mesh
        decimate(state->getMesh(), targetVertices);
        state->invalidate();
    }

    void CmdDecimate::execute(PointCloudState* pc) {
        // Unsupported
    }
}