#include "cmd_transfercolor.h"
#include <processing/transfercolor.h>
#include <string.h>

namespace mt {

    bool CmdTransferColor::parseArgs(int argc, char** argv) {
        neighs = 3;

        if (argc < 3) return false;
        for (int i = 1; i < argc; i++) {
            names.push_back(argv[i]);
        }

        if (argc == 4) {
            neighs = atoi(argv[3]);
        }

        return true;
    }

    void CmdTransferColor::execute(MtState* state) {
        ObjectState* from = state->getObject(names[0]);
        ObjectState* to = state->getObject(names[1]);

        MeshState* fromMesh = dynamic_cast<MeshState*> (from);
        MeshState* toMesh = dynamic_cast<MeshState*> (to);
        PointCloudState* fromPc = dynamic_cast<PointCloudState*> (from);
        PointCloudState* toPc = dynamic_cast<PointCloudState*> (to);

        if (fromMesh && toMesh) {
            transferColor(fromMesh->getMesh(), toMesh->getMesh());
            toMesh->invalidate();
        } else if (fromPc && toPc) {
            transferColor(fromPc->getPointCloud(), toPc->getPointCloud(), neighs);
            toPc->invalidate();
        } else {
            commandError("Incompatible objects");
        }

    }
}