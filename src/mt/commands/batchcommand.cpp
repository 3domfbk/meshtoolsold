#include "batchcommand.h"

namespace mt {

    void BatchCommand::execute(MtState* state) {
        this->mtstate = state;

        // Pre-process
        preProcess();

        // Iterate all objects
        unordered_map<string, ObjectState*>::iterator it;
        for (it = state->getObjects()->begin(); it != state->getObjects()->end(); it++) {
            if (!it->second->isSelected()) continue;

            // Mesh
            MeshState* mesh = dynamic_cast<MeshState*> (it->second);
            if (mesh) {
                execute(mesh);
                continue;
            }

            // Point Cloud
            PointCloudState* pc = dynamic_cast<PointCloudState*> (it->second);
            if (pc) {
                execute(pc);
            }
        }

        // Post-process
        postProcess();
    }
}
