#include "cmd_merge.h"
#include <processing/merge.h>
#include <string.h>

namespace mt {

    bool CmdMerge::parseArgs(int argc, char** argv) {
        for (int i = 1; i < argc; i++) {
            names.push_back(argv[i]);
        }

        return true;
    }

    void CmdMerge::execute(MtState* state) {
        vector<PointCloud*> input;

        for (int i = 0; i < names.size(); i++ ) {
            ObjectState* os = state->getObject(names[i]);
            PointCloudState* pc = dynamic_cast<PointCloudState*> (os);
            if (pc != NULL) {
                input.push_back(pc->getPointCloud());
            }
        }

        if (input.empty()) {
            commandError("Incompatible objects");
            return;
        }

        PointCloud* result = merge(input);

        // Add point cloud to global state
        state->addObject(new PointCloudState(result, NULL, names[0] + "_merged"));

        // Autofocus
        state->focusToObject(names[0] + "_merged");
    }
}
