#include "cmd_open.h"
#include <io/lodcreator.h>
#include <io/pointcloudreader.h>
#include <string.h>

namespace mt {

    bool CmdOpen::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;
        inputFilePath = strdup(argv[1]);

        return true;
    }

    void CmdOpen::execute(MtState* state) {
        // Check file existence
        FILE* pcFile = fopen(inputFilePath.c_str(), "rb");
        if (pcFile == NULL) {
            commandError("File not found");
            return;
        }
        // Release file
        fclose(pcFile);

        // Read point cloud
        PointCloudReader* reader = PointCloudReader::createReader(inputFilePath, 400000);
        PointCloud* cloud = reader->readPointCloud();
        if (cloud == NULL) {
            commandError("Cannot open cloud");
            return;
        }
        
        // Read FsOctree if present
        char ofname[128];
        memset(ofname, 0, 128);
        const char* ext = strrchr(inputFilePath.c_str(), '.');
        strncpy(ofname, inputFilePath.c_str(), strlen(inputFilePath.c_str()) - strlen(ext));
        sprintf(ofname, "%s.fst", ofname);
        FILE* fst = fopen(ofname, "rb");
        FsOctree* octree = NULL;
        if( fst != NULL ){
            fclose(fst);
            octree= LodCreator::readFsOctree(ofname);
        }
        
        // Add point cloud to global state
        state->addObject(new PointCloudState(cloud, octree, inputFilePath));

        // Autofocus
        state->focusToObject(inputFilePath);
    }
}
