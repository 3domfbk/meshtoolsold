#include "cmd_focus.h"
#include <string.h>

namespace mt {

    bool CmdFocus::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        objectName = argv[1];

        return true;
    }

    void CmdFocus::execute(MtState* state) {
        state->focusToObject(objectName);
    }
}