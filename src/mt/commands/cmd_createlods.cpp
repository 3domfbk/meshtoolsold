/* 
 * File:   cmd_createlods.cpp
 * Author: lake
 * 
 * Created on December 12, 2016, 5:31 PM
 */

#include "cmd_createlods.h"
#include <io/lodcreator.h>

using namespace mt;

bool CmdCreateLods::parseArgs(int argc, char** argv){
    if (argc != 2) return false;
    inputFilePath = string(argv[1]);

    return true;
}

void CmdCreateLods::execute(MtState* state){
    // Check file existence
    FILE* ply = fopen(inputFilePath.c_str(), "rb");
    if (ply == NULL) {
        commandError("File not found");
        return;
    }
    
    LodCreator::createLods(inputFilePath.c_str());
}