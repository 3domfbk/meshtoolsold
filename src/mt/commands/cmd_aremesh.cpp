#include "cmd_aremesh.h"
#include <filtering/aremesh.h>
#include <string.h>

namespace mt {

    bool CmdAremesh::parseArgs(int argc, char** argv) {
        if (argc != 5) return false;

        error = (float) atof(argv[1]);
        if (error == 0) return false;

        minEdgeLength = (float) atof(argv[2]);
        if (minEdgeLength == 0) return false;

        maxEdgeLength = (float) atof(argv[3]);
        if (maxEdgeLength == 0) return false;

        iterations = atoi(argv[4]);
        if (iterations == 0) return false;

        return true;
    }

    void CmdAremesh::execute(MeshState* mesh) {
        // Adaptive remesh!
        aremesh(mesh->getMesh(), error, minEdgeLength, maxEdgeLength, iterations);
        mesh->invalidate();
    }

    void CmdAremesh::execute(PointCloudState* pc) {
        // Unsupported
    }
}