#include "cmd_nonmanifolds.h"
#include <filtering/nonmanifolds.h>
#include <string.h>

namespace mt {

    bool CmdNonManifolds::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdNonManifolds::execute(MeshState* state) {
        // Decimate the mesh
        nonmanifolds(state->getMesh());
        state->invalidate();
    }

    void CmdNonManifolds::execute(PointCloudState* pc) {
        // Not supported
    }
}