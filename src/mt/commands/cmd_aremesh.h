#pragma once
#include "batchcommand.h"
#include <stdio.h>

namespace mt {

    class CmdAremesh : public BatchCommand {
    public:
        // Override
        void execute(MeshState* mesh);
        void execute(PointCloudState* pointCloud);

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

    private:
        float error;
        float minEdgeLength;
        float maxEdgeLength;
        int iterations;
    };
}