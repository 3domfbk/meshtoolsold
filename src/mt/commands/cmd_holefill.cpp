#include "cmd_holefill.h"
#include <filtering/holefill.h>
#include <string.h>

namespace mt {

    bool CmdHoleFill::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdHoleFill::execute(MeshState* state) {
        // Decimate the mesh
        holefill(state->getMesh());
        state->invalidate();
    }

    void CmdHoleFill::execute(PointCloudState* pc) {
        // Unsupported
    }
}