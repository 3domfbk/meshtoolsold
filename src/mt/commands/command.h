#pragma once
#include "../state/mtstate.h"

namespace mt {

    /**
     * Abstract command class
     */
    class Command {
    public:
        /**
         * Splits the commandLine into tokens and calls 
         * the subclass' parseArgs
         */
        bool prepareArgs(char* commandLine);

        /**
         * Defines the execution of the command
         */
        virtual void execute(MtState* state) = 0;

    protected:
        /**
         * Command subclasses use this to parse arguments
         **/
        virtual bool parseArgs(int argc, char** argv) = 0;

        /**
         * Prints a command error
         */
        void commandError(const char* error, ...);

        /**
         * Prints an output message
         */
        void output(char* error, ...);
    };
}