#include "cmd_farea.h"
#include <filtering/farea.h>
#include <string.h>

namespace mt {

    bool CmdFarea::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        minArea = (float) atof(argv[1]);
        if (minArea == 0.0f) return false;
        return true;
    }

    void CmdFarea::execute(MeshState* state) {
        farea(state->getMesh(), minArea);
        state->invalidate();
    }

    void CmdFarea::execute(PointCloudState* state) {
        // Not possible
    }
}