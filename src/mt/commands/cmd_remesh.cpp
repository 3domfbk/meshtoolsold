#include "cmd_remesh.h"
#include <filtering/remesh.h>
#include <string.h>

namespace mt {

    bool CmdRemesh::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        targetEdgeLength = atof(argv[1]);
        if (targetEdgeLength == 0) return false;

        return true;
    }

    void CmdRemesh::execute(MeshState* state) {
        // Decimate the mesh
        remesh(state->getMesh(), targetEdgeLength);
        state->invalidate();
    }

    void CmdRemesh::execute(PointCloudState* state) {
        // Not supported
    }
}