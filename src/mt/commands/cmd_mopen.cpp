#include "cmd_mopen.h"
#include <io/meshreader.h>
#include <string.h>

using namespace std;

namespace mt {

    bool CmdMopen::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;
        inputFilePath = argv[1];

        return true;
    }

    void CmdMopen::execute(MtState* state) {
        // Read mesh
        MeshReader reader(inputFilePath);
        Mesh* mesh = reader.readMesh();
        if (mesh == NULL) {
            commandError("Cannot open mesh");
            return;
        }

        // Add the mesh to state
        state->addObject(new MeshState(mesh, inputFilePath));

        // Autofocus
        state->focusToObject(inputFilePath);
    }
}