#include "cmd_smooth.h"
#include <filtering/smooth.h>
#include <string.h>

namespace mt {

    bool CmdSmooth::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        steps = atoi(argv[1]);
        if (steps == 0) return false;

        return true;
    }

    void CmdSmooth::execute(MeshState* state) {
        // Smooth the mesh
        smooth(state->getMesh(), steps);
        state->invalidate();
    }

    void CmdSmooth::execute(PointCloudState* state) {
        // Not supported
    }
}