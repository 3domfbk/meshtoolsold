#include "cmd_list.h"
#include "../state/meshstate.h"
#include "../state/pointcloudstate.h"

#include <iomanip>

namespace mt {

    bool CmdList::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdList::execute(MtState* state) {
        // List all objects!
        for (auto it = state->getObjects()->cbegin(); it != state->getObjects()->cend(); it++) {
            cout << left << setw(20) << it->first << ": ";

            // Hidden or visible
            if (!it->second->isSelected()) {
                cout << "[H]";
            } else {
                cout << "   ";
            }

            // Object type
            // -- Mesh
            MeshState* mesh = dynamic_cast<MeshState*> (it->second);
            if (mesh) {
                cout << "(ME) " << mesh->getMesh()->n_vertices() << " vertices";
            }

            // -- Pc
            PointCloudState* cloud = dynamic_cast<PointCloudState*> (it->second);
            if (cloud) {
                cout << "(PC) " << cloud->getPointCloud()->getNumPoints() << " vertices";
            }

            cout << endl;
        }
    }
}