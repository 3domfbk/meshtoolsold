#include "cmd_octree.h"
#include <datatypes/octree.h>
#include <string.h>

namespace mt {

    bool CmdOctree::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        nodeMaxPoints = atoi(argv[1]);
        if (nodeMaxPoints == 0) return false;
        return true;
    }

    void CmdOctree::execute(MeshState* state) {
        // Not supported
    }

    void CmdOctree::execute(PointCloudState* state) {
        // Create octree
        state->getPointCloud()->buildOctreeNP(nodeMaxPoints);
    }
}