#include "cmd_rename.h"
#include <string.h>

namespace mt {

    bool CmdRename::parseArgs(int argc, char** argv) {
        if (argc < 3) return false;
        for (int i = 1; i < argc; i++) {
            names.push_back(argv[i]);
        }

        return true;
    }

    void CmdRename::execute(MtState* state) {
        state->renameObject(names[0], names[1]);
    }
}