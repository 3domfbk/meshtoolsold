#pragma once
#include "batchcommand.h"
#include <stdio.h>

namespace mt {

    class CmdMakeok : public BatchCommand {
    public:
        // Override
        void execute(MeshState* mesh);
        void execute(PointCloudState* pointCloud);
        void postProcess();

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

        // Result
        vector<PointCloud*> results;
        vector<Mesh*> meshResults;
        vector<string> names;
    };
}