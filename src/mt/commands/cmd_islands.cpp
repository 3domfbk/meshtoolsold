#include "cmd_islands.h"
#include <filtering/islands.h>
#include <filtering/mislands.h>
#include <string.h>

namespace mt {

    bool CmdIslands::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        maxDistance = (float) atof(argv[1]);
        if (maxDistance == 0.0f) return false;
        return true;
    }

    void CmdIslands::execute(MeshState* state) {
        vector<Mesh*> isl = mislands(state->getMesh());
        parentNames.push_back(state->getName());

        for (int i = 0; i < isl.size(); i++) {
            char buf[4];
            sprintf(buf, "_%d", i);
            mnames.push_back(state->getName() + buf);
            mresult.push_back(isl[i]);
        }
    }

    void CmdIslands::execute(PointCloudState* state) {
        vector<PointCloud*> isl = islands(state->getPointCloud(), maxDistance);
        parentNames.push_back(state->getName());

        for (int i = 0; i < isl.size(); i++) {
            char buf[4];
            sprintf(buf, "_%d", i);
            names.push_back(state->getName() + buf);
            result.push_back(isl[i]);
        }
    }

    void CmdIslands::postProcess() {
        // Add all pc islands
        for (int i = 0; i < result.size(); i++) {
            mtstate->addObject(new PointCloudState(result[i], NULL, names[i]));
        }

        // Add all mesh islands
        for (int i = 0; i < mresult.size(); i++) {
            mtstate->addObject(new MeshState(mresult[i], mnames[i]));
        }

        // Remove parent clouds
        for (int i = 0; i < parentNames.size(); i++) {
            mtstate->deleteObject(parentNames[i]);
        }
    }
}
