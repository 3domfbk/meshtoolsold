#include "cmd_repair.h"
#include <filtering/repair.h>
#include <string.h>

namespace mt {

    bool CmdRepair::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;
        epsilon = (float) atof(argv[1]);

        return true;
    }

    void CmdRepair::execute(MeshState* state) {
        // Decimate the mesh
        repair(state->getMesh(), epsilon);
        state->invalidate();
    }

    void CmdRepair::execute(PointCloudState* state) {
        // Not supported
    }
}