#include "cmd_select.h"
#include <string.h>

namespace mt {

    bool CmdSelect::parseArgs(int argc, char** argv) {
        if (argc < 2) return false;
        for (int i = 1; i < argc; i++) {
            names.push_back(argv[i]);
        }

        return true;
    }

    void CmdSelect::execute(MtState* state) {
        // Select-all with "*"
        if (names[0] == "*") {
            state->fillSelection();
            return;
        }

        // Clear selection
        state->clearSelection();

        for (int i = 0; i < names.size(); i++) {
            // Select specified object if it exists
            ObjectState* obj = state->getObject(names[i]);
            if (obj != NULL) obj->setSelected(true);
        }
    }
}