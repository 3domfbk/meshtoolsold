#pragma once
#include "command.h"
#include <stdio.h>

namespace mt {

    class CmdSelect : public Command {
    public:

        // Override
        void execute(MtState* state);

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

    private:
        vector<string> names;
    };
}