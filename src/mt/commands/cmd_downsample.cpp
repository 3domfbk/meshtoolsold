#include "cmd_downsample.h"
#include <filtering/downsampling.h>
#include <string.h>

namespace mt {

    bool CmdDownsample::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        voxelSize = (float) atof(argv[1]);
        if (voxelSize == 0.0f) return false;
        return true;
    }

    void CmdDownsample::execute(MeshState* state) {
        // Unsupported
    }

    void CmdDownsample::execute(PointCloudState* state) {
        // Downsample the point cloud
        PointCloud* ds = downsample(state->getPointCloud(), voxelSize, true);
        // Overwrite
        mtstate->addObject(new PointCloudState(ds, NULL, state->getName()));
    }
}