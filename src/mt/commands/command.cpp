#include "command.h"
#include <utils/stringutils.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

namespace mt {

    bool Command::prepareArgs(char* commandLine) {
        bool res = true;

        int tokens;
        char** args = strSplit(commandLine, ' ', tokens);

        // Parse args
        if (!parseArgs(tokens, args)) {
            commandError("Bad arguments.\n");
            res = false;
        }

        // Release them
        for (int i = 0; i < tokens; i++) {
            free(args[i]);
        }

        free(args);

        return res;
    }

    void Command::commandError(const char* error, ...) {
        printf("E: ");
        va_list args;
        va_start(args, error);
        vprintf(error, args);
        va_end(args);

        printf("\n");
        fflush(stdout);
    }

    void Command::output(char* message, ...) {
        printf("M: ");
        va_list args;
        va_start(args, message);
        vprintf(message, args);
        va_end(args);

        printf("\n");
        fflush(stdout);
    }
}