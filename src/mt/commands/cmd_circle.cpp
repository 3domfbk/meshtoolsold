#include "cmd_circle.h"
#include <datatypes/circle.h>
#include <utils/types.h>
#include <string.h>

namespace mt {

    bool CmdCircle::parseArgs(int argc, char** argv) {
        if (argc != 9) return false;
        x = atof(argv[1]);
		y = atof(argv[2]);
		z = atof(argv[3]);
		radius = atof(argv[4]);
		r = atoi(argv[5]);
		g = atoi(argv[6]);
		b = atoi(argv[7]);
		name = strdup(argv[8]);

        return true;
    }

    void CmdCircle::execute(MtState* state) {
        
		Circle* circle = new Circle(x, y, z, radius, BGR(r, g, b));
		
        // Add point cloud to global state
        state->addObject(new CircleState(circle, name));

        // Autofocus
        state->focusToObject(name);
    }
}
