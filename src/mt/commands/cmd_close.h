#pragma once
#include "batchcommand.h"
#include <stdio.h>

namespace mt {

    class CmdClose : public BatchCommand {
    public:
        // Override
        void execute(MeshState* mesh);
        void execute(PointCloudState* pointCloud);
        void postProcess();

    protected:
        // Override
        bool parseArgs(int argc, char** argv);
    };
}