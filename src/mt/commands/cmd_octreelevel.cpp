#include "cmd_octreelevel.h"
#include <utils/stringutils.h>
#include <string.h>

namespace mt {

    bool CmdOctreeLevel::parseArgs(int argc, char** argv) {
        if (argc < 3) return false;
        objectName = strdup(argv[1]);
        octreeLevel = atoi(argv[2]);
       
        return true;
    }

    void CmdOctreeLevel::execute(MtState* state) {
        ObjectState* object = state->getObject<>(objectName);
        if (object == NULL) {
            commandError("Object not found");
            return;
        }

        object->getViewer()->setOctreeLevel(object->getName(), octreeLevel);
    }
}
