#include "cmd_close.h"
#include <string.h>

namespace mt {

    bool CmdClose::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdClose::execute(MeshState* state) {
        // Delete object
        mtstate->deleteObject(state->getName());
    }

    void CmdClose::execute(PointCloudState* state) {
        // Delete object
        mtstate->deleteObject(state->getName());
    }

    void CmdClose::postProcess() {
        mtstate->fillSelection();
    }
}