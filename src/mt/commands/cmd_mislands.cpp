#include "cmd_mislands.h"
#include <filtering/mislands.h>
#include <datatypes/meshisland.h>
#include <string.h>

namespace mt {

    bool CmdMislands::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdMislands::execute(MtState* state) {
        if (state->getMesh() == NULL) {
            commandError("No mesh loaded");
            return;
        }

        // Find islands
        vector<MeshIsland*> islands = mislands(state->getMesh());
        //state->setMeshIslands(islands);
    }
}