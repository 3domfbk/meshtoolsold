#include "cmd_makeok.h"
#include <filtering/farea.h>
#include <filtering/islands.h>
#include <filtering/mislands.h>
#include <filtering/downsampling.h>
#include <filtering/nonmanifolds.h>
#include <filtering/fedgelength.h>
#include <filtering/holefill.h>
#include <filtering/remesh.h>
#include <processing/transfercolor.h>
#include <string.h>

namespace mt {

    bool CmdMakeok::parseArgs(int argc, char** argv) {
        return true;
    }

    void CmdMakeok::execute(MeshState* state) {
        Mesh* orig = state->getMesh();

        // Remove bad shaped triangles
        float d = orig->getPointDensity();
        float fedgeLength = 3.0f / sqrtf(d);
        fedgelength(orig, fedgeLength);

        // Islands
        vector<Mesh*> isls = mislands(orig);
        Mesh* result = isls[0];

        // Remove all garbage
        for (int i = 1; i < isls.size(); i++) {
            delete isls[i];
        }

        // Remove non-manifolds
        nonmanifolds(result);

        // Hole filling
        //holefill(result);

        // Remesh
        remesh(result, fedgeLength / 3);
        nonmanifolds(result);

        // Transfercolor
        transferColor(orig, result);

        names.push_back(state->getName() + "_ok");
        meshResults.push_back(result);
    }

    void CmdMakeok::execute(PointCloudState* state) {

        // Iterate garbage removal!
        PointCloud* result = state->getPointCloud();
        while (true) {
            // Make octree with 8 points per leaf
            result->buildOctreeNP(8);

            // Compute islands with good size based on density estimate
            float maxD = result->getAvgPointDistance() * 3;
            vector<PointCloud*> isls = islands(result, maxD);

            // Delete intermediate
            if (result != state->getPointCloud()) delete result;
            result = isls[0];

            // Remove all garbage
            for (int i = 1; i < isls.size(); i++) {
                delete isls[i];
            }

            // Exit condition
            cout << "Islands=" << isls.size() << ", md=" << maxD << endl;
            if (isls.size() < 50) break;
        }

        // Downsample to remove doubles
        float pd = result->getAvgPointDistance();
        result->deleteOctree();
        PointCloud* ds = downsample(result, pd / 10);
        delete result;
        result = ds;

        // Save result
        results.push_back(result);
        names.push_back(state->getName() + "_ok");
    }

    void CmdMakeok::postProcess() {
        // PCs
        for (int i = 0; i < results.size(); i++) {
            mtstate->addObject(new PointCloudState(results[i], NULL, names[i]));
        }

        // Meshes
        for (int i = 0; i < meshResults.size(); i++) {
            mtstate->addObject(new MeshState(meshResults[i], names[i]));
        }
    }
}