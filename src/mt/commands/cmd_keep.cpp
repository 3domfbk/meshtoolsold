#include "cmd_keep.h"
#include <string.h>

namespace mt {

    bool CmdKeep::parseArgs(int argc, char** argv) {
        if (argc != 1) return false;
        return true;
    }

    void CmdKeep::execute(MtState* state) {
        // Deletes all non selected objects
        auto it = state->getObjects()->cbegin();
        for (; it != state->getObjects()->cend(); it++) {
            if (!it->second->isSelected()) {
                state->deleteObject(it->first);
            }
        }
    }
}