#include "cmd_subdivide.h"
#include <filtering/subdivide.h>
#include <string.h>

namespace mt {

    bool CmdSubdivide::parseArgs(int argc, char** argv) {
        if (argc != 2) return false;

        times = atoi(argv[1]);
        if (times == 0) return false;

        return true;
    }

    void CmdSubdivide::execute(MeshState* state) {
        // Decimate the mesh
        subdivide(state->getMesh(), times);
        state->invalidate();
    }

    void CmdSubdivide::execute(PointCloudState* state) {
        // Not supported
    }
}