/* 
 * File:   cmd_createlods.h
 * Author: lake
 *
 * Created on December 12, 2016, 5:31 PM
 */

#pragma once

#include "command.h"
#include <string>
using namespace std;

namespace mt {

    class CmdCreateLods : public Command {
    public:
        // Override
        void execute(MtState* state);

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

    private:
        string inputFilePath;
    };
}