#pragma once
#include "../state/mtstate.h"
#include "command.h"

namespace mt {

    /**
     * Command that applies to all active objects in state
     */
    class BatchCommand : public Command {
    public:
        /**
         * Defines the execution of the command
         */
        void execute(MtState* state);

    protected:
        // Interface
        virtual void execute(MeshState* mesh) = 0;
        virtual void execute(PointCloudState* pointCloud) = 0;

        virtual void preProcess() {
        };

        virtual void postProcess() {
        };

        MtState* mtstate;
    };
}