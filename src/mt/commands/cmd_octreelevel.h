#pragma once
#include "command.h"
#include <stdio.h>

namespace mt {

    class CmdOctreeLevel : public Command {
    public:
        // Override
        void execute(MtState* state);

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

    private:
        string objectName;
        int octreeLevel;
    };
}