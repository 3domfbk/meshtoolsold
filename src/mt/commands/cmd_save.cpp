#include "cmd_save.h"
#include <io/pointcloudwriter.h>
#include <io/meshwriter.h>
#include <utils/stringutils.h>
#include <string.h>

namespace mt {

    bool CmdSave::parseArgs(int argc, char** argv) {
        if (argc < 3) return false;
        objectName = strdup(argv[1]);
        inputFilePath = strdup(argv[2]);
        if (argc == 4) {
            format = strdup(argv[3]);
        }

        return true;
    }

    void CmdSave::execute(MtState* state) {
        ObjectState* object = state->getObject<>(objectName);
        if (object == NULL) {
            commandError("Object not found");
            return;
        }

        // Try pointcloud
        PointCloudState* pc = dynamic_cast<PointCloudState*> (object);
        if (pc) {
            PcFormat fmt = PF_LIL_ENDIAN;
            if ("ASCII" == format || "ascii" == format) fmt = PF_ASCII;
            else if ("BIG_ENDIAN" == format || "big_endian" == format) fmt = PF_BIG_ENDIAN;

            // Write point cloud
            if (!PointCloudWriter::write(inputFilePath, pc->getPointCloud(), fmt)) {
                commandError("Format not supported: %s", inputFilePath.c_str());
            }
            return;
        }

        // Try mesh
        MeshState* mesh = dynamic_cast<MeshState*> (object);
        if (mesh) {
            // Write point cloud
            bool binary = true;
            if ("ASCII" == format || "ascii" == format) binary = false;

            MeshWriter meshWriter(inputFilePath);
            meshWriter.writeMesh(mesh->getMesh(), binary);
            return;
        }
    }
}
