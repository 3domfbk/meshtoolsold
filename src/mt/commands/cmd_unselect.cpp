#include "cmd_unselect.h"
#include <string.h>

namespace mt {

    bool CmdUnselect::parseArgs(int argc, char** argv) {
        if (argc < 2) return false;
        for (int i = 1; i < argc; i++) {
            names.push_back(argv[i]);
        }

        return true;
    }

    void CmdUnselect::execute(MtState* state) {
        for (int i = 0; i < names.size(); i++) {
            // Select specified object
            ObjectState* obj = state->getObject(names[i]);
            if (obj != NULL) obj->setSelected(false);
        }
    }

}