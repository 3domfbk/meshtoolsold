#pragma once
#include "batchcommand.h"
#include <stdio.h>

namespace mt {

    class CmdTransferColor : public Command {
    public:
        // Override
        void execute(MtState* state);

    protected:
        // Override
        bool parseArgs(int argc, char** argv);

        vector<string> names;
        int neighs;
    };
}